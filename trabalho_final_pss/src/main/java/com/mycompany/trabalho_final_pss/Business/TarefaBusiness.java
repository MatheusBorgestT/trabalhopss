package com.mycompany.trabalho_final_pss.Business;

import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.Tarefa;
import com.mycompany.trabalho_final_pss.Repository.RepublicaRepository;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TarefaBusiness {
    
    public static ArrayList<String> saveTarefa(Map<String, Object> valores) throws SQLException {
        var erros = validarCampos(valores);

        if (erros.isEmpty()) {
            RepublicaRepository.saveTarefa(construirTarefa(valores));
        } 
        
        return erros;
    }
    
    public static ArrayList<String> updateTarefa(Tarefa t, Map<String, Object> valores) throws SQLException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        var erros = validarCampos(valores);
        
        if (erros.isEmpty()) {
            t.setDescricao((String) valores.get("descricao"));
            t.setDataLimiteTermino(LocalDate.parse(((String) valores.get("dataTermino")), dtf));
            
            RepublicaRepository.updateTarefa(t);
        } 
        
        return erros;
    }
    
    private static ArrayList<String> validarCampos(Map<String, Object> valores) throws SQLException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        ArrayList<String> erros = new ArrayList<>();
        
        if (((String) valores.get("descricao")).isBlank()) {
            erros.add("Preencha o campo descrição da tarefa");
        }

        var dataTermino = LocalDate.parse((String) valores.get("dataTermino"), dtf);
        if (dataTermino.isBefore(LocalDate.now())) {
            erros.add("A data de término da tarefa precisa ser posterior a data de agendamento.");
        }

        return erros;
    }
    
    private static Tarefa construirTarefa(Map<String, Object> valores) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        
        return new Tarefa(
                (LocalDate) valores.get("dataAgendamento"), 
                LocalDate.parse(((String) valores.get("dataTermino")), dtf),
                (String) valores.get("descricao"),
                Integer.parseInt(String.valueOf(valores.get("idAutor"))),
                Integer.parseInt(String.valueOf(valores.get("idRepublica"))),
                2,
                (List<Morador>) valores.get("responsaveis")
        );
    }
    
}
