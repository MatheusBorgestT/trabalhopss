package com.mycompany.trabalho_final_pss.Business;

import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.Pessoa;
import com.mycompany.trabalho_final_pss.Model.StatusMoradorEnum;
import com.mycompany.trabalho_final_pss.Model.Usuario;
import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import java.sql.SQLException;
import java.util.Map;

public class LoginBusiness {

    public void cadastrarUsuario(Map<String, String> valores) throws SQLException {
        Pessoa pessoa = new Pessoa(
                valores.get("nomeCompleto"),
                valores.get("apelido"),
                valores.get("telefone"),
                valores.get("cpf"),
                valores.get("telefoneExtra1"),
                valores.get("telefoneExtra2")
        );
        int idPessoa = UsuarioRepository.cadastrarPessoa(pessoa);
        pessoa.setIdPessoa(idPessoa);
        
        Morador morador = new Morador(
                idPessoa, 
                StatusMoradorEnum.SEM_TETO.getValorInt(), 
                -1
        );
        UsuarioRepository.cadastrarMorador(morador);

        Usuario usuario = new Usuario(
                valores.get("usuario"),
                valores.get("senha"),
                pessoa
        );
        UsuarioRepository.cadastrarUsuario(usuario);
    }

    public boolean usuarioJaCadastrado(String usuario) throws SQLException {
        return UsuarioRepository.usuarioJaCadastrado(usuario);
    }    
    
    public Usuario getUsuarioByNomeUsuario(String usuario) throws SQLException {
        // Aqui seria feito algum processo de filtragem, se necessário. Como não
        // é, simplesmente conecta a Service à Repository
        return UsuarioRepository.getUsuarioByNomeUsuario(usuario);
    }

}
