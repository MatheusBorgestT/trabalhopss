package com.mycompany.trabalho_final_pss.Business;

import com.mycompany.trabalho_final_pss.Model.Carteira;
import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.RegraNotificacao;
import com.mycompany.trabalho_final_pss.Model.Republica;
import com.mycompany.trabalho_final_pss.Model.StatusMoradorEnum;
import com.mycompany.trabalho_final_pss.Repository.RepublicaRepository;
import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Map;

public class RepublicaBusiness {

    private DateTimeFormatter dtf;

    public RepublicaBusiness() {
        dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    }

    public ArrayList<String> createRepublica(Map<String, String> valores) throws SQLException {
        var erros = validarCampos(valores);

        if (erros.isEmpty()) {
            var republica = construirRepublica(valores);
            
            int idRepublica = RepublicaRepository.cadastrarRepublica(republica);

            RepublicaRepository.cadastrarCarteira(new Carteira(idRepublica));
            RepublicaRepository.cadastrarRegraNotificacao(new RegraNotificacao(idRepublica));

            int idPessoa = UsuarioService.getIdPessoaLogada();

            UsuarioRepository.updateMoradorRepublicaOndeReside(idPessoa, idRepublica);
            UsuarioRepository.updateStatusMorador(idPessoa, StatusMoradorEnum.REPRESENTANTE.getValorInt());
        }

        return erros;
    }

    public int deleteRepublica(Republica republica) throws SQLException {
        ArrayList<Morador> moradores = UsuarioRepository.getMoradorListByIdRepublicaOndeReside(republica.getIdRepublica());
        for (Morador m : moradores) {
            UsuarioRepository.updateMoradorRepublicaOndeReside(m.getIdPessoa(), -1);
            UsuarioRepository.updateStatusMorador(m.getIdPessoa(), StatusMoradorEnum.SEM_TETO.getValorInt());
        }
        RepublicaRepository.excluirCarteira(republica.getCarteira());
        RepublicaRepository.excluirRegraNotificacao(republica.getRegraNotificacao());
        return RepublicaRepository.excluirRepublica(republica);
    }

    public ArrayList<String> updateRepublica(Republica republica, Map<String, String> valores) throws SQLException {
        var erros = validarCampos(valores);

        if (erros.isEmpty()) {
            int idRepublica = republica.getIdRepublica();
            republica = construirRepublica(valores);

            republica.setIdRepublica(idRepublica);
            RepublicaRepository.updateRepublica(republica);
        }

        return erros;
    }

    private ArrayList<String> validarCampos(Map<String, String> valores) {
        ArrayList<String> erros = new ArrayList<>();

        for (Map.Entry<String, String> par : valores.entrySet()) {
            String key = par.getKey();
            if (!key.equals("latitude")
                    && !key.equals("longitude")
                    && !key.equals("codigoEtica")
                    && par.getValue().isEmpty()) {
                erros.add("Somente os campos de latitude, longitude e código de ética podem ficar vazios;");
                break;
            }
        }

        if (valores.get("latitude").isEmpty() != valores.get("longitude").isEmpty()) {
            erros.add("As coordenadas precisam ser preenchidas totalmente ou não serem;");
        }

        if (!valores.get("latitude").isEmpty()) {
            try {
                Double.valueOf(valores.get("latitude"));
            } catch (NumberFormatException e) {
                erros.add("A latitude precisa ser um valor real;");
            }
        }

        if (!valores.get("longitude").isEmpty()) {
            try {
                Double.valueOf(valores.get("longitude"));
            } catch (NumberFormatException e) {
                erros.add("A longitude precisa ser um valor real;");
            }
        }

        boolean dataFlag = true;
        try {
            LocalDate.parse(valores.get("dataFundacao"), dtf);
        } catch (DateTimeParseException e) {
            erros.add("Erro ao converter a data informada;");
            dataFlag = false;
        }

        if (dataFlag) {
            var dataFundacao = LocalDate.parse(valores.get("dataFundacao"), dtf);
            if (dataFundacao.isAfter(LocalDate.now())) {
                erros.add("A data de fundação precisa ser anterior ou igual ao dia de hoje;");
            }
        }

        try {
            Double.valueOf(valores.get("despesasMediasMorador"));
        } catch (NumberFormatException e) {
            erros.add("A despesa média por morador precisa ser um valor real;");
        }

        boolean vagasFlag = true;

        try {
            Integer.parseInt(valores.get("totalVagas"));
        } catch (NumberFormatException e) {
            erros.add("O total de vagas precisa ser um valor inteiro;");
            vagasFlag = false;
        }

        try {
            Integer.parseInt(valores.get("vagasOcupadas"));
        } catch (NumberFormatException e) {
            erros.add("O número de vagas ocupadas precisa ser um valor inteiro;");
            vagasFlag = false;
        }

        if (vagasFlag) {
            int totalVagas = Integer.valueOf(valores.get("totalVagas"));
            int vagasOcupadas = Integer.valueOf(valores.get("vagasOcupadas"));

            if (totalVagas < 0) {
                erros.add("O número total de vagas precisa ser um valor positivo;");
            } else if (totalVagas < 2) {
                erros.add("É necessário que hajam pelo menos duas vagas na república;");
            }

            if (vagasOcupadas < 0) {
                erros.add("O número de vagas ocupadas precisa ser um valor positivo;");
            } else if (vagasOcupadas > totalVagas) {
                erros.add("É necessário que o número de vagas ocupadas seja menor ou igual ao de total de vagas");
            }
        }

        return erros;
    }

    private Republica construirRepublica(Map<String, String> valores) throws SQLException {
        Republica republica = new Republica(
                valores.get("nome"),
                LocalDate.parse(valores.get("dataFundacao"), dtf),
                valores.get("logradouro"),
                valores.get("cep"),
                valores.get("bairro"),
                valores.get("pontoReferencia"),
                valores.get("vantagens"),
                Double.valueOf(valores.get("despesasMediasMorador")),
                Integer.valueOf(valores.get("totalVagas")),
                Integer.valueOf(valores.get("vagasOcupadas")),
                UsuarioService.getUsuarioLogado().getPessoa().getMorador()
        );
        if (!valores.get("latitude").isEmpty()) {
            republica.setLatitude(Double.parseDouble(valores.get("latitude")));
        }
        if (!valores.get("longitude").isEmpty()) {
            republica.setLatitude(Double.parseDouble(valores.get("longitude")));
        }
        if (!valores.get("codigoEtica").isEmpty()) {
            republica.setEstatuto(valores.get("codigoEtica"));
        }
        
        return republica;
    }

}
