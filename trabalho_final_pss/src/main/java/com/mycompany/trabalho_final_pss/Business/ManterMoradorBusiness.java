/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.trabalho_final_pss.Business;

import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.Republica;
import com.mycompany.trabalho_final_pss.Model.StatusMoradorEnum;
import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Julia Dalcamini
 */
public class ManterMoradorBusiness {
    
    private DateTimeFormatter dtf;

    public ManterMoradorBusiness() {
        dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    }

    public ArrayList<String> createMorador(Map<String, String> valores) throws SQLException {
        var erros = validarCampos(valores);

        if (erros.isEmpty()) {
            var morador = construirMorador(valores);
            
            int idPessoa = UsuarioRepository.cadastrarMorador(morador);

            Republica republica = UsuarioService.getRepublicaOndeResideUsuarioLogado();

            UsuarioRepository.updateMoradorRepublicaOndeReside(idPessoa, republica.getIdRepublica());
            UsuarioRepository.updateStatusMorador(idPessoa, StatusMoradorEnum.MORADOR.getValorInt());
        }

        return erros;
    }

    public ArrayList<String> updateMorador(Morador morador, Map<String, String> valores) throws SQLException {
        var erros = validarCampos(valores);

        if (erros.isEmpty()) {
            int idPessoa = morador.getIdPessoa();
            morador = construirMorador(valores);

            morador.setIdPessoa(idPessoa);
            UsuarioRepository.updateMorador(morador);
        }

        return erros;
    }

    public void deleteMorador(Morador morador) throws SQLException {
        ArrayList<Morador> moradores = UsuarioRepository.getMoradorListByIdRepublicaOndeReside(UsuarioService.getRepublicaOndeResideUsuarioLogado().getIdRepublica());
        for (Morador m : moradores) {
            if (m.getIdPessoa() == morador.getIdPessoa())
                UsuarioRepository.updateStatusMorador(m.getIdPessoa(), StatusMoradorEnum.SEM_TETO.getValorInt());
        }
    }
    
private ArrayList<String> validarCampos(Map<String, String> valores) {
        ArrayList<String> erros = new ArrayList<>();

        if (!valores.get("valorAluguel").isEmpty()) {
            try {
                Double.valueOf(valores.get("valorAluguel"));
            } catch (NumberFormatException e) {
                erros.add("O valor do aluguel precisa ser um valor real;");
            }
        }

        boolean dataFlag = true;
        try {
            LocalDate.parse(valores.get("dataEntrada"), dtf);
        } catch (DateTimeParseException e) {
            erros.add("Erro ao converter a data informada;");
            dataFlag = false;
        }

        if (dataFlag) {
            var dataFundacao = LocalDate.parse(valores.get("dataEntrada"), dtf);
            if (dataFundacao.isAfter(LocalDate.now())) {
                erros.add("A data de entrada precisa ser anterior ou igual ao dia de hoje;");
            }
        }

        return erros;
    }

    private Morador construirMorador(Map<String, String> valores) throws SQLException {
        Morador morador = new Morador(
                Integer.parseInt(valores.get("idMorador")),
                Integer.parseInt(valores.get("statusMorador")),
                Integer.parseInt(valores.get("idRepublicaOndeReside"))
        );
        if (!valores.get("idMorador").isEmpty()) {
            morador.setIdPessoa(Integer.parseInt(valores.get("latitude")));
        }
        if (!valores.get("statusMorador").isEmpty()) {
            morador.setStatusMorador(StatusMoradorEnum.valueOf(valores.get("statusMorador")));
        }
        if (!valores.get("idRepublicaOndeReside").isEmpty()) {
            morador.setIdRepublicaOndeReside(Integer.parseInt(valores.get("idRepublicaOndeReside")));
        }
        
        return morador;
    }

}
