package com.mycompany.trabalho_final_pss.Business;

import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.ReclamacaoSugestao;
import com.mycompany.trabalho_final_pss.Model.TipoReclamacaoSugestaoEnum;
import com.mycompany.trabalho_final_pss.Repository.RepublicaRepository;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ReclamacaoSugestaoBusiness {

    public static ArrayList<String> saveReclamacaoSugestao(Map<String, Object> valores) throws SQLException {
        var erros = validarCampos(valores);

        if (erros.isEmpty()) {
            RepublicaRepository.saveReclamacaoSugestao(construirReclamacaoSugestao(valores));
        } 
        
        return erros;
    }

    public static ArrayList<String> updateReclamacaoSugestao(ReclamacaoSugestao rs, Map<String, Object> valores) throws SQLException {
        var erros = validarCampos(valores);
        
        if (erros.isEmpty()) {
            rs.setDescricao((String) valores.get("descricao"));
            rs.setTipo(TipoReclamacaoSugestaoEnum.getTipo(Integer.parseInt((String) valores.get("tipo"))));
            
            RepublicaRepository.updateReclamacaoSugestao(rs);
        } 
        
        return erros;
    }

    private static ArrayList<String> validarCampos(Map<String, Object> valores) {
        ArrayList<String> erros = new ArrayList<>();

        if (Integer.parseInt((String) valores.get("tipo")) == -1) {
            erros.add("É necessário escolher um tipo;");
        }

        if (((String) valores.get("descricao")).isBlank()) {
            erros.add("O campo descrição não pode estar vazio;");
        }

        return erros;
    }

    private static ReclamacaoSugestao construirReclamacaoSugestao(Map<String, Object> valores) {
        return new ReclamacaoSugestao(
                (LocalDate) valores.get("dataCriacao"),
                (String) valores.get("descricao"),
                Integer.parseInt(String.valueOf(valores.get("idAutor"))),
                Integer.parseInt(String.valueOf(valores.get("idRepublica"))),
                Integer.parseInt((String) valores.get("tipo")), 
                (List<Morador>) valores.get("envolvidos")
        );
    }

}
