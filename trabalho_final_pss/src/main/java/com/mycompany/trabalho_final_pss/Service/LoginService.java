
package com.mycompany.trabalho_final_pss.Service;

import com.mycompany.trabalho_final_pss.Business.LoginBusiness;
import com.mycompany.trabalho_final_pss.Model.Usuario;
import com.pss.senha.validacao.ValidadorSenha;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class LoginService {
    
    private LoginBusiness business;
    
    public LoginService() {
        business = new LoginBusiness();
    }
    
    public List<String> cadastrarUsuario(Map<String, String> valores) throws SQLException {
        List<String> erros = new ValidadorSenha().validar(valores.get("senha"));
        if (business.usuarioJaCadastrado(valores.get("usuario")))
            erros.add("Já existe um usuário com esse nome cadastrado");
        if (!valores.get("senha").equals(valores.get("confirmarSenha")))
            erros.add("As senhas não coincidem");
        
        if (erros.isEmpty()) {
            business.cadastrarUsuario(valores);
        }
        
        return erros;
    }
    
    public String validarLogin(Map<String, String> valores) throws SQLException {
        // Serive trabalhando com a lógica de detectar erros no login
        Usuario cadastrado = business.getUsuarioByNomeUsuario(valores.get("usuario"));
        if (cadastrado == null) {
            return "Este nome de usuário não está cadastrado";
        } else if (!cadastrado.getSenha().equals(valores.get("senha"))) {
            return "Senha incorreta";
        }
        return null;
    }
    
    public Usuario getUsuarioLogado(Map<String, String> valores) throws SQLException {
        return business.getUsuarioByNomeUsuario(valores.get("usuario"));
    }
    
}
