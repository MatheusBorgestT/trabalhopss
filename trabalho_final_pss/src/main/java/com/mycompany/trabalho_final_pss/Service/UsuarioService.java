package com.mycompany.trabalho_final_pss.Service;

import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.Republica;
import com.mycompany.trabalho_final_pss.Model.StatusMoradorEnum;
import com.mycompany.trabalho_final_pss.Model.Usuario;
import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import java.sql.SQLException;

public class UsuarioService {
    
    private static Usuario usuarioLogado;
    
    public static void setUsuarioLogado(Usuario usuario) {
        usuarioLogado = usuario;
    }
    
    public static Usuario getUsuarioLogado() {
        return usuarioLogado;
    }
    
    public static Morador getMoradorLogado() throws SQLException {
        return usuarioLogado.getPessoa().getMorador();
    }
    
    public static int getIdPessoaLogada() {
        return usuarioLogado.getIdPessoa();
    }
    
    public static Republica getRepublicaOndeResideUsuarioLogado() throws SQLException {
        return usuarioLogado.getPessoa().getMorador().getRepublicaOndeReside();
    }
    
    public static boolean isUsuarioRepresentante() throws SQLException {
        return usuarioLogado.getPessoa().getMorador().getStatusMorador() == StatusMoradorEnum.REPRESENTANTE;
    }
    
    public static void atualizarUsuarioInfo() throws SQLException {
        usuarioLogado = UsuarioRepository.getUsuarioByNomeUsuario(usuarioLogado.getNomeUsuario());
    }
    
}
