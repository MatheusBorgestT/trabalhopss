package com.mycompany.trabalho_final_pss.Service;

import com.mycompany.trabalho_final_pss.Business.TarefaBusiness;
import com.mycompany.trabalho_final_pss.Model.Tarefa;
import com.mycompany.trabalho_final_pss.Repository.RepublicaRepository;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class TarefaService {
    
    public static ArrayList<Tarefa> getTarefasByIdRepublica(int idRepublica) throws SQLException {
        return RepublicaRepository.getTarefasByIdRepublica(idRepublica);
    }

    public static ArrayList<Tarefa> getTarefasByDescricao(int idRepublica, String descricao) throws SQLException {
        return RepublicaRepository.getTarefasByDescricao(idRepublica, descricao);
    }

    public static ArrayList<Tarefa> getTarefasByPessoa(int idRepublica, String pessoa) throws SQLException {
        return RepublicaRepository.getTarefasByPessoa(idRepublica, pessoa);
    }

    public static ArrayList<String> cadastrarTarefa(Map<String, Object> valores) throws SQLException {
        return TarefaBusiness.saveTarefa(valores);
    }

    public static ArrayList<String> atualizarTarefa(Tarefa t, Map<String, Object> valores) throws SQLException {
        return TarefaBusiness.updateTarefa(t, valores);
    }

    public static void excluirTarefa(Tarefa t) throws SQLException {
        RepublicaRepository.excluirTarefa(t);
    }
}