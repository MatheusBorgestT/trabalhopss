/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.trabalho_final_pss.Service;

import com.mycompany.trabalho_final_pss.Business.ManterMoradorBusiness;
import com.mycompany.trabalho_final_pss.Model.Morador;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Julia Dalcamini
 */
public class ManterMoradorService {
    
    private ManterMoradorBusiness business;

    public ManterMoradorService() {
        business = new ManterMoradorBusiness();
    }

    public ArrayList<String> createMorador(Map<String, String> valores) throws SQLException {
        return business.createMorador(valores);
    }

    public void deleteMorador(Morador morador) throws SQLException {
        business.deleteMorador(morador);
    }
    
    public ArrayList<String> updateMorador(Morador morador, Map<String, String> valores) throws SQLException {
        return business.updateMorador(morador, valores);
    }
}
