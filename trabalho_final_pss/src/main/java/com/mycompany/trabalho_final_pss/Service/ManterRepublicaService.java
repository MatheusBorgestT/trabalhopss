package com.mycompany.trabalho_final_pss.Service;

import com.mycompany.trabalho_final_pss.Business.ManterRepublicaBusiness;
import com.mycompany.trabalho_final_pss.Model.Republica;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class ManterRepublicaService {

    private ManterRepublicaBusiness business;

    public ManterRepublicaService() {
        business = new ManterRepublicaBusiness();
    }

    public ArrayList<String> createRepublica(Map<String, String> valores) throws SQLException {
        return business.createRepublica(valores);
    }

    public void deleteRepublica(Republica republica) throws SQLException {
        business.deleteRepublica(republica);
    }
    
    public ArrayList<String> updateRepublica(Republica republica, Map<String, String> valores) throws SQLException {
        return business.updateRepublica(republica, valores);
    }
}
