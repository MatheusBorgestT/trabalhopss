package com.mycompany.trabalho_final_pss.Service;

import com.mycompany.trabalho_final_pss.Business.ReclamacaoSugestaoBusiness;
import com.mycompany.trabalho_final_pss.Model.ReclamacaoSugestao;
import com.mycompany.trabalho_final_pss.Repository.RepublicaRepository;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class ReclamacaoSugestaoService {

    public static ArrayList<ReclamacaoSugestao> getReclamacoesSugestoesByIdRepublica(int idRepublica) throws SQLException {
        return RepublicaRepository.getReclamacoesSugestoesByIdRepublica(idRepublica);
    }

    public static ArrayList<ReclamacaoSugestao> getReclamacoesSugestoesByDescricao(int idRepublica, String descricao) throws SQLException {
        return RepublicaRepository.getReclamacoesSugestoesByDescricao(idRepublica, descricao);
    }

    public static ArrayList<ReclamacaoSugestao> getReclamacoesSugestoesByPessoa(int idRepublica, String pessoa) throws SQLException {
        return RepublicaRepository.getReclamacoesSugestoesByPessoa(idRepublica, pessoa);
    }

    public static ArrayList<String> cadastrarReclamacaoSugestao(Map<String, Object> valores) throws SQLException {
        return ReclamacaoSugestaoBusiness.saveReclamacaoSugestao(valores);
    }

    public static ArrayList<String> atualizarReclamacaoSugestao(ReclamacaoSugestao rs, Map<String, Object> valores) throws SQLException {
        return ReclamacaoSugestaoBusiness.updateReclamacaoSugestao(rs, valores);
    }

    public static void excluirReclamacaoSugestao(ReclamacaoSugestao rs) throws SQLException {
        RepublicaRepository.excluirReclamacaoSugestao(rs);
    }

}
