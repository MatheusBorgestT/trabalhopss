
package com.mycompany.trabalho_final_pss.Model;

import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import com.mycompany.trabalho_final_pss.Repository.RepublicaRepository;
import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class Morador {
    
    private int idPessoa;
    
    private StatusMoradorEnum statusMorador;
    
    private int idRepublicaOndeReside;
    
    private Republica republicaOndeReside;

    public Morador(int idMorador, int statusMorador, int idRepublicaOndeReside) {
        this.idPessoa = idMorador;
        this.statusMorador = StatusMoradorEnum.getStatus(statusMorador);
        this.idRepublicaOndeReside = idRepublicaOndeReside;
    }
    
    public Object[] getInfoTabelaConvidar() throws SQLException {
        var pessoa = UsuarioRepository.getPessoaByIdPessoa(idPessoa);
        return new Object[] {
            pessoa.getNome()
        };
    }
    
    public int getIdPessoa() {
        return idPessoa;
    }
    
    public Pessoa getPessoa() throws SQLException {
        return UsuarioRepository.getPessoaByIdPessoa(idPessoa);
    }

    public StatusMoradorEnum getStatusMorador() {
        return statusMorador;
    }

    public int getIdRepublicaOndeReside() {
        return idRepublicaOndeReside;
    }

    public Republica getRepublicaOndeReside() throws SQLException {
        if (republicaOndeReside == null) {
            republicaOndeReside = RepublicaRepository.getRepublicaById(idRepublicaOndeReside);
        }
        return republicaOndeReside;
    }

    public void setIdPessoa(int idMorador) {
        this.idPessoa = idMorador;
    }

    public void setStatusMorador(StatusMoradorEnum statusMorador) {
        this.statusMorador = statusMorador;
    }

    public void setRepublicaOndeReside(Republica republicaOndeReside) {
        this.republicaOndeReside = republicaOndeReside;
    }

    public void setIdRepublicaOndeReside(int idRepublicaOndeReside) {
        this.idRepublicaOndeReside = idRepublicaOndeReside;
    }    
}
