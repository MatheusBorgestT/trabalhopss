package com.mycompany.trabalho_final_pss.Model;

import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;

public class ReclamacaoSugestao {

    private int idReclamacaoSugestao;

    private LocalDate dataCriacao;

    private LocalDate dataSolucao;

    private String descricao;

    private boolean excluida;

    private int idAutor;

    private Pessoa autor;

    private int idRepublica;

    private Republica republica;

    private TipoReclamacaoSugestaoEnum tipo;

    private List<Morador> moradoresEnvolvidos;

    public ReclamacaoSugestao(int idReclamacaoSugestao) {
        this.idReclamacaoSugestao = idReclamacaoSugestao;
    }

    public ReclamacaoSugestao(LocalDate dataCriacao, String descricao, int idAutor, int idRepublica, int tipo, List<Morador> moradoresEnvolvidos) {
        this.dataCriacao = dataCriacao;
        this.descricao = descricao;
        this.idAutor = idAutor;
        this.idRepublica = idRepublica;
        this.tipo = TipoReclamacaoSugestaoEnum.getTipo(tipo);
        this.moradoresEnvolvidos = moradoresEnvolvidos;
    }

    public ReclamacaoSugestao(int idReclamacaoSugestao, LocalDate dataCriacao, LocalDate dataSolucao, String descricao, boolean excluida, int idAutor, int idRepublica, int tipo) {
        this.idReclamacaoSugestao = idReclamacaoSugestao;
        this.dataCriacao = dataCriacao;
        this.dataSolucao = dataSolucao;
        this.descricao = descricao;
        this.excluida = excluida;
        this.idAutor = idAutor;
        this.idRepublica = idRepublica;
        this.tipo = TipoReclamacaoSugestaoEnum.getTipo(tipo);
    }

    public Object[] toObjectArray() throws SQLException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        var moradoresEnvolvidos = getMoradoresEnvolvidos();
        StringBuilder sb = new StringBuilder();
        String nome;
        for (Morador m : moradoresEnvolvidos) {
            nome = UsuarioRepository.getPessoaByIdPessoa(m.getIdPessoa()).getNome();
            sb.append(nome).append(", ");
        }
        if (sb.length() > 1) {
            sb.setLength(sb.length() - 2);
        }

        return new Object[]{
            dtf.format(dataCriacao),
            tipo.getValorString(),
            getAutor().getNome(),
            descricao,
            sb.toString(),
            dataSolucao != null ? "SIM" : "NÃO"
        };
    }

    public int getIdReclamacaoSugestao() {
        return idReclamacaoSugestao;
    }

    public LocalDate getDataCriacao() {
        return dataCriacao;
    }

    public LocalDate getDataSolucao() {
        return dataSolucao;
    }

    public String getDescricao() {
        return descricao;
    }

    public boolean isExcluida() {
        return excluida;
    }

    public int getIdAutor() {
        return idAutor;
    }

    public Pessoa getAutor() throws SQLException {
        if (autor == null) {
            autor = UsuarioRepository.getPessoaByIdPessoa(idAutor);
        }
        return autor;
    }

    public int getIdRepublica() {
        return idRepublica;
    }

    public Republica getRepublica() {
        return republica;
    }

    public TipoReclamacaoSugestaoEnum getTipo() {
        return tipo;
    }

    public List<Morador> getMoradoresEnvolvidos() throws SQLException {
        if (moradoresEnvolvidos == null) {
            moradoresEnvolvidos = UsuarioRepository.getMoradorListByEnvolvidoReclamacaoSugestao(idReclamacaoSugestao);
        }
        return moradoresEnvolvidos;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setTipo(TipoReclamacaoSugestaoEnum tipo) {
        this.tipo = tipo;
    }
}
