package com.mycompany.trabalho_final_pss.Model;

import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Tarefa {
    
    private int idTarefa;
    
    private LocalDate dataAgendamento;

    private LocalDate dataLimiteTermino;

    private String descricao;

    private boolean finalizada;
    
    private int idAutor;

    private Pessoa autor;

    private int idRepublica;

    private Republica republica;

    private List<Morador> moradoresResponsaveis;

    private PeriodicidadeEnum periodicidade;

    public Tarefa(int idTarefa) {
        this.idTarefa = idTarefa;
    }
    
    public Tarefa(LocalDate dataAgendamento, LocalDate dataLimiteTermino, String descricao, int idAutor, int idRepublica, int periodicidade, List<Morador> moradoresResponsaveis) {
        this.dataAgendamento = dataAgendamento;
        this.dataLimiteTermino = dataLimiteTermino;
        this.descricao = descricao;
        this.idAutor = idAutor;
        this.idRepublica = idRepublica;
        this.periodicidade = PeriodicidadeEnum.getPeriodicidadeEnum(periodicidade);
        this.moradoresResponsaveis = moradoresResponsaveis;
    }
    
    public Tarefa(int idTarefa, LocalDate dataAgendamento, LocalDate dataLimiteTermino, String descricao, boolean finalizada, int idAutor, int idRepublica, int periodicidade) {
        this.idTarefa = idTarefa;
        this.dataAgendamento = dataAgendamento;
        this.dataLimiteTermino = dataLimiteTermino;
        this.descricao = descricao;
        this.finalizada = finalizada;
        this.idAutor = idAutor;
        this.idRepublica = idRepublica;
        this.periodicidade = PeriodicidadeEnum.getPeriodicidadeEnum(periodicidade);
    }
    
    public Object[] toObjectArray() throws SQLException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        var moradoresReponsaveis = getMoradoresResponsaveis();
        StringBuilder sb = new StringBuilder();
        String nome;
        for (Morador m : moradoresReponsaveis) {
            nome = UsuarioRepository.getPessoaByIdPessoa(m.getIdPessoa()).getNome();
            sb.append(nome).append(", ");
        }
        if (sb.length() > 1) {
            sb.setLength(sb.length() - 2);
        }

        return new Object[]{
            descricao,
            sb.toString(),
            dtf.format(dataAgendamento),
            dtf.format(dataLimiteTermino),
            finalizada != false ? "SIM" : "NÃO"
        };
    }

    public int getIdTarefa() {
        return idTarefa;
    }

    public void setIdTarefa(int idTarefa) {
        this.idTarefa = idTarefa;
    }

    public LocalDate getDataAgendamento() {
        return dataAgendamento;
    }

    public void setDataAgendamento(LocalDate dataAgendamento) {
        this.dataAgendamento = dataAgendamento;
    }

    public LocalDate getDataLimiteTermino() {
        return dataLimiteTermino;
    }

    public void setDataLimiteTermino(LocalDate dataLimiteTermino) {
        this.dataLimiteTermino = dataLimiteTermino;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isFinalizada() {
        return finalizada;
    }

    public void setFinalizada(boolean finalizada) {
        this.finalizada = finalizada;
    }
    
    public int getIdAutor() {
        return idAutor;
    }

    public Pessoa getAutor() throws SQLException {
        if (autor == null) {
            autor = UsuarioRepository.getPessoaByIdPessoa(idAutor);
        }
        return autor;
    }

    public int getIdRepublica() {
        return idRepublica;
    }

    public Republica getRepublica() {
        return republica;
    }

    public PeriodicidadeEnum getPeriodicidade() {
        return periodicidade;
    }

    public void setPeriodicidade(PeriodicidadeEnum periodicidade) {
        this.periodicidade = periodicidade;
    }
    
    public List<Morador> getMoradoresResponsaveis() throws SQLException {
        if (moradoresResponsaveis == null) {
            moradoresResponsaveis = UsuarioRepository.getMoradorListByResponsavelTarefa(idTarefa);
        }
        return moradoresResponsaveis;
    }

}
