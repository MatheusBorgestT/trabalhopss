package com.mycompany.trabalho_final_pss.Model;

public enum TipoEntradaEnum {

    DOACAO(1), RENDA_EVENTO(2), OUTRO(3);
    
    private int tipoEntrada;
    
    private TipoEntradaEnum(int tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }

}
