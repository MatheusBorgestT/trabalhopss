package com.mycompany.trabalho_final_pss.Model;

public enum TipoReclamacaoSugestaoEnum {

    RECLAMACAO(1), SUGESTAO(2);

    private final int tipo;

    private TipoReclamacaoSugestaoEnum(int tipo) {
        this.tipo = tipo;
    }

    public static TipoReclamacaoSugestaoEnum getTipo(int id) {
        for (TipoReclamacaoSugestaoEnum t : TipoReclamacaoSugestaoEnum.values()) {
            if (t.tipo == id) {
                return t;
            }
        }
        throw new IllegalArgumentException("Status não encontrado");
    }
    
    public int getValorInt() {
        return tipo;
    }
    
    public String getValorString() {
        switch (tipo) {
            case 1:
                return "Reclamação";
            case 2:
                return "Sugestão";
            default:
                return "Erro";
        }
    }

}
