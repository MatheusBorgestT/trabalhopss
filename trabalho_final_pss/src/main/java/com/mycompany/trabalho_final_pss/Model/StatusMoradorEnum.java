package com.mycompany.trabalho_final_pss.Model;

public enum StatusMoradorEnum {

    REPRESENTANTE(1), MORADOR(2), SEM_TETO(3);
    
    private final int statusMorador;
    
    private StatusMoradorEnum(int statusMorador) {
        this.statusMorador = statusMorador;
    }
    
    public int getValorInt() {
        return statusMorador;
    }
    
    public String getValorString() {
        switch (statusMorador) {
            case 1:
                return "Representante";
            case 2:
                return "Morador";
            case 3:
                return "Sem Teto";
            default:
                return "Erro";
        }
    }
    
    public static StatusMoradorEnum getStatus(int id) {
        for (StatusMoradorEnum s: StatusMoradorEnum.values()) {
            if (s.statusMorador == id) {
                return s;
            }
        }
        throw new IllegalArgumentException("Status não encontrado");
    }

}
