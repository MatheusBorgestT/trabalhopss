package com.mycompany.trabalho_final_pss.Model;

import java.util.ArrayList;
import java.util.Calendar;

public class LancamentoSaida extends Lancamento {

    private double valorPagoPelaReceitaColetiva;

    private Calendar vencimento;

    private PeriodicidadeEnum periodicidade;

    private ArrayList<ParcelaLancamentoMorador> moradorPagante;

}
