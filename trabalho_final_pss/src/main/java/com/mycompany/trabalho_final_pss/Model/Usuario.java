package com.mycompany.trabalho_final_pss.Model;

import com.mycompany.trabalho_final_pss.DAO.PessoaDAO;
import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Usuario {

    private int idUsuario;

    private String nomeUsuario;

    private String senha;

    private int idPessoa;

    private Pessoa pessoa;

    public Usuario(int idUsuario, String nomeUsuario, String senha, int idPessoa) {
        this.idUsuario = idUsuario;
        this.nomeUsuario = nomeUsuario;
        this.senha = senha;
        this.idPessoa = idPessoa;
    }

    public Usuario(String usuario, String senha, Pessoa pessoa) {
        this.nomeUsuario = usuario;
        this.senha = senha;
        this.pessoa = pessoa;
    }

    public Pessoa getPessoa() throws SQLException {
        if (pessoa == null) {
            pessoa = UsuarioRepository.getPessoaByIdPessoa(idPessoa);
        }
        return pessoa;
    }

    public int getIdPessoa() {
        return pessoa == null ? -1 : pessoa.getIdPessoa();
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public String getSenha() {
        return senha;
    }

}
