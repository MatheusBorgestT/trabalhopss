package com.mycompany.trabalho_final_pss.Model;


import com.mycompany.trabalho_final_pss.Repository.RepublicaRepository;
import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class Republica {
    
    private int idRepublica;

    private String nome;

    private LocalDate dataFundacao;

    private LocalDate dataExtincao;

    private String endereco;

    private String cep;

    private String bairro;

    private String pontoReferencia;

    private double latitude;

    private double longitude;

    private String vantagens;

    private double despesaMedia;

    private int numeroTotalVagas;

    private int numeroVagasOcupadas;

    private String estatuto;

    private RegraNotificacao regraNotificacao;
    
    private int idRepresentante;

    private Morador representante;

    private ArrayList<Morador> moradores;

    private Carteira carteira;

    private ArrayList<ReclamacaoSugestao> reclamacoesSugestoes;

    public Republica(String nome, LocalDate dataFundacao, String endereco, String cep, String bairro, String pontoReferencia, String vantagens, double despesaMedia, int numeroTotalVagas, int numeroVagasOcupadas, Morador representante) {
        this.nome = nome;
        this.dataFundacao = dataFundacao;
        this.endereco = endereco;
        this.cep = cep;
        this.bairro = bairro;
        this.pontoReferencia = pontoReferencia;
        this.vantagens = vantagens;
        this.despesaMedia = despesaMedia;
        this.numeroTotalVagas = numeroTotalVagas;
        this.numeroVagasOcupadas = numeroVagasOcupadas;
        this.representante = representante;
    }

    public Republica(int idRepublica, String nome, LocalDate dataFundacao, LocalDate dataExtincao, String endereco, String cep, String bairro, String pontoReferencia, double latitude, double longitude, String vantagens, double despesaMedia, int numeroTotalVagas, int numeroVagasOcupadas, String estatuto, int idRepresentante) {
        this.idRepublica = idRepublica;
        this.nome = nome;
        this.dataFundacao = dataFundacao;
        this.dataExtincao = dataExtincao;
        this.endereco = endereco;
        this.cep = cep;
        this.bairro = bairro;
        this.pontoReferencia = pontoReferencia;
        this.latitude = latitude;
        this.longitude = longitude;
        this.vantagens = vantagens;
        this.despesaMedia = despesaMedia;
        this.numeroTotalVagas = numeroTotalVagas;
        this.numeroVagasOcupadas = numeroVagasOcupadas;
        this.estatuto = estatuto;
        this.idRepresentante = idRepresentante;
    }
    
    public ArrayList<Object[]> getInfoTabelaMoradores() throws SQLException {
        var historico = UsuarioRepository.getListHistoricoByIdRepublica(idRepublica);
        ArrayList<Object[]> lista = new ArrayList();
        for(InstanciaHistoricoRepublicas i : historico) {
            lista.add(i.getObjectHistorico());
        }
        return lista;
    }

    public int getNumeroVagasDisponiveis() {
        return numeroTotalVagas - numeroVagasOcupadas;
    }
    
    public int getIdRepresentante() {
        return representante == null ? -1 : representante.getIdPessoa();
    }

    public int getIdRepublica() {
        return idRepublica;
    }

    public String getNome() {
        return nome;
    }

    public LocalDate getDataFundacao() {
        return dataFundacao;
    }

    public LocalDate getDataExtincao() {
        return dataExtincao;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getCep() {
        return cep;
    }

    public String getBairro() {
        return bairro;
    }

    public String getPontoReferencia() {
        return pontoReferencia;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getVantagens() {
        return vantagens;
    }

    public double getDespesaMedia() {
        return despesaMedia;
    }

    public int getNumeroTotalVagas() {
        return numeroTotalVagas;
    }

    public int getNumeroVagasOcupadas() {
        return numeroVagasOcupadas;
    }

    public String getEstatuto() {
        return estatuto;
    }

    public RegraNotificacao getRegraNotificacao() throws SQLException {
        if (regraNotificacao == null) {
            regraNotificacao = RepublicaRepository.getRegraNotificacaoById(idRepublica);
        }
        return regraNotificacao;
    }

    public Morador getRepresentante() {
        return representante;
    }

    public ArrayList<Morador> getMoradores() throws SQLException {
        if (moradores == null) {
            moradores = UsuarioRepository.getMoradorListByIdRepublicaOndeReside(idRepublica);
        }
        return moradores;
    }

    public Carteira getCarteira() throws SQLException {
        if (carteira == null) {
            carteira = RepublicaRepository.getCarteiraById(idRepublica);
        }
        return carteira;
    }

    public ArrayList<ReclamacaoSugestao> getReclamacoesSugestoes() {
        return reclamacoesSugestoes;
    }

    public void setIdRepublica(int idRepublica) {
        this.idRepublica = idRepublica;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDataFundacao(LocalDate dataFundacao) {
        this.dataFundacao = dataFundacao;
    }

    public void setDataExtincao(LocalDate dataExtincao) {
        this.dataExtincao = dataExtincao;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setPontoReferencia(String pontoReferencia) {
        this.pontoReferencia = pontoReferencia;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setVantagens(String vantagens) {
        this.vantagens = vantagens;
    }

    public void setDespesaMedia(double despesaMedia) {
        this.despesaMedia = despesaMedia;
    }

    public void setNumeroTotalVagas(int numeroTotalVagas) {
        this.numeroTotalVagas = numeroTotalVagas;
    }

    public void setNumeroVagasOcupadas(int numeroVagasOcupadas) {
        this.numeroVagasOcupadas = numeroVagasOcupadas;
    }

    public void setEstatuto(String estatuto) {
        this.estatuto = estatuto;
    }

    public void setRegraNotificacao(RegraNotificacao regraNotificacao) {
        this.regraNotificacao = regraNotificacao;
    }

    public void setRepresentante(Morador representante) {
        this.representante = representante;
    }

    public void setMoradores(ArrayList<Morador> moradores) {
        this.moradores = moradores;
    }

    public void setCarteira(Carteira carteira) {
        this.carteira = carteira;
    }

    public void setReclamacoesSugestoes(ArrayList<ReclamacaoSugestao> reclamacoesSugestoes) {
        this.reclamacoesSugestoes = reclamacoesSugestoes;
    }

}
