package com.mycompany.trabalho_final_pss.Model;

public enum PeriodicidadeEnum {

    MENSAL(1), SEMANAL(2), DESPESA_UNICA(3);

    private final int periodicidade;

    private PeriodicidadeEnum(int periodicidade) {
        this.periodicidade = periodicidade;
    }
    
    public static PeriodicidadeEnum getPeriodicidadeEnum(int id) {
        for (PeriodicidadeEnum p : PeriodicidadeEnum.values()) {
            if (p.periodicidade == id) {
                return p;
            }
        }
        throw new IllegalArgumentException("Status não encontrado");
    }

    public int getValorInt() {
        return periodicidade;
    }
    
    public String getValorString() {
        switch (periodicidade) {
            case 1:
                return "Mensal";
            case 2:
                return "Semanal";
            case 3:
                return "Despesa única";
            default:
                return "Erro";
        }
    }

}
