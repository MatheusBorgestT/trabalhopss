package com.mycompany.trabalho_final_pss.Model;

import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class InstanciaHistoricoRepublicas {

    private String nomeRepublicaNaEpoca;

    private double mediaReputacao;
    
    private Republica republicaNaEpoca;
    
    private int idRepresentanteNaEpoca;
    
    private LocalDate dataEntrada;
    
    private double valorAluguel;
    
    private LocalDate dataSaida;
    
    private int idPessoa;
    
    private Pessoa pessoa;

    public InstanciaHistoricoRepublicas(String nomeRepublicaNaEpoca, double mediaReputacao, int idRepublicaNaEpoca, int idRepresentanteNaEpoca, LocalDate dataEntrada, double valorAluguel, LocalDate dataSaida, int idPessoa) {
        this.nomeRepublicaNaEpoca = nomeRepublicaNaEpoca;
        this.mediaReputacao = mediaReputacao;
        this.republicaNaEpoca.setIdRepublica(idRepublicaNaEpoca);
        this.idRepresentanteNaEpoca = idRepresentanteNaEpoca;
        this.dataEntrada = dataEntrada;
        this.valorAluguel = valorAluguel;
        this.dataSaida = dataSaida;
        this.idPessoa = idPessoa;
    }
    
    public Object[] getObjectHistorico() throws SQLException {
        var dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return new Object[] {
            getPessoa().getNome(),
            dtf.format(dataEntrada),
            this.getValorAluguel()
        };
    }

    public Pessoa getPessoa() throws SQLException {
        if (pessoa == null) {
            pessoa = UsuarioRepository.getPessoaByIdPessoa(idPessoa);
        }
        return pessoa;
    }
    

    public int getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }
    
    public String getNomeAtualRepublica() {
        return null;
    }

    public String getNomeRepresentanteAtual() {
        return null;
    }

    public String getContatoRepresentanteAtual() {
        return null;
    }

    public String getNomeRepresentanteNaEpoca() {
        return null;
    }

    public String getNomeRepublicaNaEpoca() {
        return nomeRepublicaNaEpoca;
    }

    public void setNomeRepublicaNaEpoca(String nomeRepublicaNaEpoca) {
        this.nomeRepublicaNaEpoca = nomeRepublicaNaEpoca;
    }

    public double getMediaReputacao() {
        return mediaReputacao;
    }

    public void setMediaReputacao(double mediaReputacao) {
        this.mediaReputacao = mediaReputacao;
    }

    public Republica getRepublicaNaEpoca() {
        return republicaNaEpoca;
    }

    public void setRepublicaNaEpoca(Republica republicaNaEpoca) {
        this.republicaNaEpoca = republicaNaEpoca;
    }

    public LocalDate getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(LocalDate dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public double getValorAluguel() {
        return valorAluguel;
    }

    public void setValorAluguel(double valorAluguel) {
        this.valorAluguel = valorAluguel;
    }

    public LocalDate getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(LocalDate dataSaida) {
        this.dataSaida = dataSaida;
    }

    public int getIdRepresentanteNaEpoca() {
        return idRepresentanteNaEpoca;
    }

    public void setIdRepresentanteNaEpoca(int idRepresentanteNaEpoca) {
        this.idRepresentanteNaEpoca = idRepresentanteNaEpoca;
    }

}
