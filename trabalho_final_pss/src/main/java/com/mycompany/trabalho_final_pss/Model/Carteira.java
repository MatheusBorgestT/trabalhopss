package com.mycompany.trabalho_final_pss.Model;

public class Carteira {
    
    private int idRepublica;

    private double saldo;

    public Carteira(int idRepublica) {
        this.idRepublica = idRepublica;
    }

    public Carteira(int idRepublica, double saldo) {
        this.idRepublica = idRepublica;
        this.saldo = saldo;
    }

    public int getIdRepublica() {
        return idRepublica;
    }

    public double getSaldo() {
        return saldo;
    }

}
