package com.mycompany.trabalho_final_pss.Model;


public class RegraNotificacao {

    private int idRepublica;
    
    private int diasParaAvisoVencimento;

    public RegraNotificacao(int idRepublica) {
        this.idRepublica = idRepublica;
    }

    public RegraNotificacao(int idRepublica, int diasParaAvisoVencimento) {
        this.idRepublica = idRepublica;
        this.diasParaAvisoVencimento = diasParaAvisoVencimento;
    }
    
    public int getIdRepublica() {
        return idRepublica;
    }

    public int getDiasParaAvisoVencimento() {
        return diasParaAvisoVencimento;
    }

}
