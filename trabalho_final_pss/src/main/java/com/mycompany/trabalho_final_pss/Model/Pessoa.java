package com.mycompany.trabalho_final_pss.Model;

import com.mycompany.trabalho_final_pss.DAO.MoradorDAO;
import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Pessoa {
    
    private int idPessoa;

    private String nome;

    private String apelido;

    private String telefone;

    private String cpf;

    private String telefoneExtra1;

    private String telefoneExtra2;
    
    private Morador morador;

    public Pessoa(int idPessoa, String nome, String apelido, String telefone, String cpf, String telefoneExtra1, String telefoneExtra2, Morador morador) {
        this.idPessoa = idPessoa;
        this.nome = nome;
        this.apelido = apelido;
        this.telefone = telefone;
        this.cpf = cpf;
        this.telefoneExtra1 = telefoneExtra1;
        this.telefoneExtra2 = telefoneExtra2;
        this.morador = morador;
    }

    public Pessoa(int idPessoa, String nome, String apelido, String telefone, String cpf, String telefoneExtra1, String telefoneExtra2) {
        this.idPessoa = idPessoa;
        this.nome = nome;
        this.apelido = apelido;
        this.telefone = telefone;
        this.cpf = cpf;
        this.telefoneExtra1 = telefoneExtra1;
        this.telefoneExtra2 = telefoneExtra2;
    }

    public Pessoa(String nome, String apelido, String telefone, String cpf, String telefoneExtra1, String telefoneExtra2) {
        this.nome = nome;
        this.apelido = apelido;
        this.telefone = telefone;
        this.cpf = cpf;
        this.telefoneExtra1 = telefoneExtra1;
        this.telefoneExtra2 = telefoneExtra2;
    }
    
    public Morador getMorador() throws SQLException {
        if (morador == null) {
            morador = UsuarioRepository.getMoradorByIdPessoa(idPessoa);
        }
        return morador;
    }

    public int getIdPessoa() {
        return idPessoa;
    }

    public String getNome() {
        return nome;
    }

    public String getApelido() {
        return apelido;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public String getTelefoneExtra1() {
        return telefoneExtra1;
    }

    public String getTelefoneExtra2() {
        return telefoneExtra2;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setTelefoneExtra1(String telefoneExtra1) {
        this.telefoneExtra1 = telefoneExtra1;
    }

    public void setTelefoneExtra2(String telefoneExtra2) {
        this.telefoneExtra2 = telefoneExtra2;
    }

    public void setMorador(Morador morador) {
        this.morador = morador;
    }

}
