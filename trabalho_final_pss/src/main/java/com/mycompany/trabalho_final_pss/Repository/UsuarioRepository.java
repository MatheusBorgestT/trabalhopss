
package com.mycompany.trabalho_final_pss.Repository;

import com.mycompany.trabalho_final_pss.DAO.InstanciaHistoricoRepublicasDAO;
import com.mycompany.trabalho_final_pss.DAO.MoradorDAO;
import com.mycompany.trabalho_final_pss.DAO.PessoaDAO;
import com.mycompany.trabalho_final_pss.DAO.UsuarioDAO;
import com.mycompany.trabalho_final_pss.Model.InstanciaHistoricoRepublicas;
import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.Pessoa;
import com.mycompany.trabalho_final_pss.Model.Usuario;
import java.sql.SQLException;
import java.util.ArrayList;

public class UsuarioRepository {
    
    private static UsuarioDAO usuarioDAO = new UsuarioDAO();
    
    private static PessoaDAO pessoaDAO = new PessoaDAO();
    
    private static MoradorDAO moradorDAO = new MoradorDAO();
    
    // -------------------------------------------------------------------------
    // Usuário
    // -------------------------------------------------------------------------
    public static int cadastrarUsuario(Usuario usuario) throws SQLException {
        return usuarioDAO.save(usuario);
    }
    
    public static Usuario getUsuarioByNomeUsuario(String usuario) throws SQLException {
        return usuarioDAO.getLoginByUsuario(usuario);
    }
    
    public static boolean usuarioJaCadastrado(String usuario) throws SQLException {
        return usuarioDAO.usuarioJaCadastrado(usuario);
    }
    
    // -------------------------------------------------------------------------
    // Pessoa
    // -------------------------------------------------------------------------
    public static int cadastrarPessoa(Pessoa pessoa) throws SQLException {
        return pessoaDAO.save(pessoa);
    }
    
    public static Pessoa getPessoaByIdPessoa(int idPessoa) throws SQLException {
        return pessoaDAO.get(idPessoa);
    }
    
    // -------------------------------------------------------------------------
    // Morador
    // -------------------------------------------------------------------------
    public static int cadastrarMorador(Morador morador) throws SQLException {
        return moradorDAO.save(morador);
    }
    
    public static Morador getMoradorByIdPessoa(int idPessoa) throws SQLException {
        return moradorDAO.get(idPessoa);
    }
    
    public static void updateMoradorRepublicaOndeReside(int idPessoa, int idRepublica) throws SQLException {
        moradorDAO.updateIdRepublicaOndeReside(idPessoa, idRepublica);
    }
    
    public static void updateMorador(Morador morador) throws SQLException {
        new MoradorDAO().update(morador);
    }
    
    public static void updateStatusMorador(int idPessoa, int status) throws SQLException{
        moradorDAO.updateStatus(idPessoa, status);
    }
    
    public static ArrayList<Morador> getMoradorListByIdRepublicaOndeReside(int idRepublica) throws SQLException {
        return moradorDAO.getListByIdRepublicaOndeReside(idRepublica);
    }
    
    public static ArrayList<Morador> getMoradorListByEnvolvidoReclamacaoSugestao(int idReclamacaoSugestao) throws SQLException {
        return moradorDAO.getMoradorListByEnvolvidoReclamacaoSugestao(idReclamacaoSugestao);
    }
    
    public static ArrayList<Morador> getMoradorListByResponsavelTarefa(int idTarefa) throws SQLException {
        return moradorDAO.getMoradorListByResponsavelTarefa(idTarefa);
    }
    
    // -------------------------------------------------------------------------
    // Historico
    // -------------------------------------------------------------------------
    public static int registrarHistorico(InstanciaHistoricoRepublicas historico) throws SQLException {
        return new InstanciaHistoricoRepublicasDAO().save(historico);
    }
    
    public static ArrayList<InstanciaHistoricoRepublicas> getListHistoricoByIdRepublica(int idRepublica) throws SQLException {
        return new InstanciaHistoricoRepublicasDAO().getListByIdRepublica(idRepublica);
    }
}
