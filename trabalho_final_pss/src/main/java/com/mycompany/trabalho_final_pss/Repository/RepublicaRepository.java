package com.mycompany.trabalho_final_pss.Repository;

import com.mycompany.trabalho_final_pss.DAO.CarteiraDAO;
import com.mycompany.trabalho_final_pss.DAO.ReclamacaoSugestaoDAO;
import com.mycompany.trabalho_final_pss.DAO.RegraNotificacaoDAO;
import com.mycompany.trabalho_final_pss.DAO.RepublicaDAO;
import com.mycompany.trabalho_final_pss.DAO.TarefaDAO;
import com.mycompany.trabalho_final_pss.Model.Carteira;
import com.mycompany.trabalho_final_pss.Model.ReclamacaoSugestao;
import com.mycompany.trabalho_final_pss.Model.RegraNotificacao;
import com.mycompany.trabalho_final_pss.Model.Republica;
import com.mycompany.trabalho_final_pss.Model.Tarefa;
import java.sql.SQLException;
import java.util.ArrayList;

public class RepublicaRepository {
    
    private static RepublicaDAO republicaDAO = new RepublicaDAO();
    
    private static CarteiraDAO carteiraDAO = new CarteiraDAO();
    
    private static RegraNotificacaoDAO regraNotificacaoDAO = new RegraNotificacaoDAO();
    
    private static ReclamacaoSugestaoDAO reclamacaoSugestaoDAO = new ReclamacaoSugestaoDAO();
    
    private static TarefaDAO tarefaDAO = new TarefaDAO();

    // -------------------------------------------------------------------------
    // República
    // -------------------------------------------------------------------------
    public static Republica getRepublicaById(int id) throws SQLException {
        return republicaDAO.get(id);
    }

    public static int cadastrarRepublica(Republica republica) throws SQLException {
        return republicaDAO.save(republica);
    }

    public static Republica updateRepublica(Republica republica) throws SQLException {
        return republicaDAO.update(republica);
    }

    public static int excluirRepublica(Republica republica) throws SQLException {
        return republicaDAO.delete(republica);
    }

    // -------------------------------------------------------------------------
    // Carteira
    // -------------------------------------------------------------------------
    public static Carteira getCarteiraById(int id) throws SQLException {
        return carteiraDAO.get(id);
    }

    public static int cadastrarCarteira(Carteira carteira) throws SQLException {
        return carteiraDAO.save(carteira);
    }

    public static Carteira updateCarteira(Carteira carteira) throws SQLException {
        return carteiraDAO.update(carteira);
    }

    public static int excluirCarteira(Carteira carteira) throws SQLException {
        return carteiraDAO.delete(carteira);
    }

    // -------------------------------------------------------------------------
    // Regra Notificação
    // -------------------------------------------------------------------------
    public static RegraNotificacao getRegraNotificacaoById(int id) throws SQLException {
        return regraNotificacaoDAO.get(id);
    }

    public static int cadastrarRegraNotificacao(RegraNotificacao regraNotificacao) throws SQLException {
        return regraNotificacaoDAO.save(regraNotificacao);
    }

    public static RegraNotificacao updateRegraNotificacao(RegraNotificacao regraNotificacao) throws SQLException {
        return regraNotificacaoDAO.update(regraNotificacao);
    }

    public static int excluirRegraNotificacao(RegraNotificacao regraNotificacao) throws SQLException {
        return regraNotificacaoDAO.delete(regraNotificacao);
    }

    // -------------------------------------------------------------------------
    // Reclamações e Sugestões
    // -------------------------------------------------------------------------
    public static int saveReclamacaoSugestao(ReclamacaoSugestao rs) throws SQLException {
        return reclamacaoSugestaoDAO.save(rs);
    }
    
    public static ReclamacaoSugestao updateReclamacaoSugestao(ReclamacaoSugestao rs) throws SQLException {
        return reclamacaoSugestaoDAO.update(rs);
    }
    
    public static void excluirReclamacaoSugestao(ReclamacaoSugestao rs) throws SQLException {
        reclamacaoSugestaoDAO.delete(rs);
    }
    
    public static ArrayList<ReclamacaoSugestao> getReclamacoesSugestoesByIdRepublica(int id) throws SQLException {
        return reclamacaoSugestaoDAO.getByIdRepublica(id);
    }

    public static ArrayList<ReclamacaoSugestao> getReclamacoesSugestoesByDescricao(int idRepublica, String descricao) throws SQLException {
        return reclamacaoSugestaoDAO.getByDescricao(idRepublica, descricao);
    }

    public static ArrayList<ReclamacaoSugestao> getReclamacoesSugestoesByPessoa(int idRepublica, String pessoa) throws SQLException {
        return reclamacaoSugestaoDAO.getByPessoa(idRepublica, pessoa);
    }
    
    // -------------------------------------------------------------------------
    // Tarefas
    // -------------------------------------------------------------------------
    public static int saveTarefa(Tarefa t) throws SQLException {
        return tarefaDAO.save(t);
    }
    
    public static Tarefa updateTarefa(Tarefa t) throws SQLException {
        return tarefaDAO.update(t);
    }
    
    public static void excluirTarefa(Tarefa t) throws SQLException {
        tarefaDAO.delete(t);
    }
    
    public static ArrayList<Tarefa> getTarefasByIdRepublica(int id) throws SQLException {
        return tarefaDAO.getByIdRepublica(id);
    }

    public static ArrayList<Tarefa> getTarefasByDescricao(int idRepublica, String descricao) throws SQLException {
        return tarefaDAO.getByDescricao(idRepublica, descricao);
    }

    public static ArrayList<Tarefa> getTarefasByPessoa(int idRepublica, String pessoa) throws SQLException {
        return tarefaDAO.getByPessoa(idRepublica, pessoa);
    }

}
