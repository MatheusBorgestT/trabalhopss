
package com.mycompany.trabalho_final_pss.DAO;

import com.mycompany.trabalho_final_pss.Utils.DAOUtil;
import com.mycompany.trabalho_final_pss.Model.RegraNotificacao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class RegraNotificacaoDAO implements IDAO<RegraNotificacao>{

    @Override
    public RegraNotificacao get(int id) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM regra_notificacao WHERE id_republica = ?");
        
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        RegraNotificacao r = null;
        if (rs.next()) {
            r = new RegraNotificacao(
                    rs.getInt("id_republica"),
                    rs.getInt("dias_para_aviso_vencimento")
            );
        }

        conn.close();
        return r;
    }

    @Override
    public ArrayList<RegraNotificacao> getList() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int save(RegraNotificacao objeto) throws SQLException {
        var conn = DAOUtil.getConnection();
        var ps = conn.prepareStatement(
                " INSERT INTO regra_notificacao(id_republica, dias_para_aviso_vencimento) "
                + " VALUES(?, ?) ",
                Statement.RETURN_GENERATED_KEYS
        );
        ps.setInt(1, objeto.getIdRepublica());
        ps.setInt(2, 5);
        int id = ps.executeUpdate();
        
        conn.close();
        return id;
    }

    @Override
    public int delete(RegraNotificacao objeto) throws SQLException {
        var conn = DAOUtil.getConnection();
        var ps = conn.prepareStatement(
                "DELETE FROM regra_notificacao WHERE id_republica = ?",
                Statement.RETURN_GENERATED_KEYS
        );
        ps.setInt(1, objeto.getIdRepublica());
        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);
        
        conn.close();
        return id;
    }

    @Override
    public RegraNotificacao update(RegraNotificacao objeto) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
