package com.mycompany.trabalho_final_pss.DAO;

import com.mycompany.trabalho_final_pss.Utils.DAOUtil;
import com.mycompany.trabalho_final_pss.Model.Pessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PessoaDAO implements IDAO<Pessoa> {

    @Override
    public Pessoa get(int id) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM pessoa WHERE id_pessoa = ?");
        
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Pessoa p = null;
        if (rs.next()) {
            p = new Pessoa(
                    rs.getInt("id_pessoa"),
                    rs.getString("nome"),
                    rs.getString("apelido"),
                    rs.getString("telefone"),
                    rs.getString("cpf"),
                    rs.getString("telefone_extra_1"),
                    rs.getString("telefone_extra_2")
            );
        }

        conn.close();
        return p;
    }

    @Override
    public ArrayList<Pessoa> getList() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int save(Pessoa objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " insert into pessoa (nome, apelido, telefone, cpf, telefone_extra_1, telefone_extra_2) "
                + " values (?, ?, ?, ?, ?, ?); ",
                Statement.RETURN_GENERATED_KEYS
        );

        ps.setString(1, objeto.getNome());
        ps.setString(2, objeto.getApelido());
        ps.setString(3, objeto.getTelefone());
        ps.setString(4, objeto.getCpf());
        ps.setString(5, objeto.getTelefoneExtra1());
        ps.setString(6, objeto.getTelefoneExtra2());

        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);

        conn.close();
        return id;
    }

    @Override
    public int delete(Pessoa objeto) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pessoa update(Pessoa objeto) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
