package com.mycompany.trabalho_final_pss.DAO;

import com.mycompany.trabalho_final_pss.Utils.DAOUtil;
import com.mycompany.trabalho_final_pss.Model.Morador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MoradorDAO implements IDAO<Morador> {

    @Override
    public Morador get(int id) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM morador WHERE id_pessoa = ?");

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Morador m = null;
        if (rs.next()) {
            m = new Morador(
                    rs.getInt("id_pessoa"),
                    rs.getInt("status"),
                    rs.getInt("id_republica_onde_reside")
            );
        }

        conn.close();
        return m;
    }

    @Override
    public ArrayList<Morador> getList() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int save(Morador objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " INSERT INTO morador(id_pessoa, status, id_republica_onde_reside) "
                + " VALUES(?, ?, ?) ",
                Statement.RETURN_GENERATED_KEYS
        );

        ps.setInt(1, objeto.getIdPessoa());
        ps.setInt(2, objeto.getStatusMorador().getValorInt());
        ps.setInt(3, objeto.getIdRepublicaOndeReside());

        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);

        conn.close();
        return id;
    }

    @Override
    public int delete(Morador objeto) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Morador update(Morador objeto) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<Morador> getListByIdRepublicaOndeReside(int idRepublica) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                "SELECT * FROM morador WHERE id_republica_onde_reside = ?"
        );

        ps.setInt(1, idRepublica);
        ResultSet rs = ps.executeQuery();

        ArrayList<Morador> listaMoradores = new ArrayList<>();
        while (rs.next()) {
            listaMoradores.add(new Morador(
                    rs.getInt("id_pessoa"),
                    rs.getInt("status"),
                    rs.getInt("id_republica_onde_reside")
            ));
        }

        conn.close();
        return listaMoradores;
    }

    public void updateIdRepublicaOndeReside(int idPessoa, int idRepublica) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                "UPDATE morador SET id_republica_onde_reside = ? WHERE id_pessoa = ?"
        );

        ps.setInt(1, idRepublica);
        ps.setInt(2, idPessoa);
        ps.executeUpdate();

        conn.close();
    }

    public void updateStatus(int idPessoa, int status) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                "UPDATE morador SET status = ? WHERE id_pessoa = ?"
        );

        ps.setInt(1, status);
        ps.setInt(2, idPessoa);
        ps.executeUpdate();

        conn.close();
    }
    
    public ArrayList<Morador> getMoradorListByEnvolvidoReclamacaoSugestao(int idReclamacaoSugestao) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " SELECT * FROM morador m "
                        + " INNER JOIN reclamacao_sugestao_envolvido rse ON m.id_pessoa = rse.id_envolvido "
                        + " WHERE rse.id_reclamacao_sugestao = ? "
        );

        ps.setInt(1, idReclamacaoSugestao);
        ResultSet rs = ps.executeQuery();

        ArrayList<Morador> listaMoradores = new ArrayList<>();
        while (rs.next()) {
            listaMoradores.add(new Morador(
                    rs.getInt("id_pessoa"),
                    rs.getInt("status"),
                    rs.getInt("id_republica_onde_reside")
            ));
        }

        conn.close();
        return listaMoradores;
    }
    
    public ArrayList<Morador> getMoradorListByResponsavelTarefa(int idTarefa) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " SELECT * FROM morador m "
                        + " INNER JOIN tarefa_morador tm ON m.id_pessoa = tm.id_morador "
                        + " WHERE tm.id_tarefa = ? "
        );

        ps.setInt(1, idTarefa);
        ResultSet rs = ps.executeQuery();

        ArrayList<Morador> listaMoradores = new ArrayList<>();
        while (rs.next()) {
            listaMoradores.add(new Morador(
                    rs.getInt("id_pessoa"),
                    rs.getInt("status"),
                    rs.getInt("id_republica_onde_reside")
            ));
        }

        conn.close();
        return listaMoradores;
    }

}
