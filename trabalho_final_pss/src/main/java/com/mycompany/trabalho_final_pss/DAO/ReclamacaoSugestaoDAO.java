package com.mycompany.trabalho_final_pss.DAO;

import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.ReclamacaoSugestao;
import com.mycompany.trabalho_final_pss.Utils.DAOUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;

public class ReclamacaoSugestaoDAO implements IDAO<ReclamacaoSugestao> {

    @Override
    public ReclamacaoSugestao get(int id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<ReclamacaoSugestao> getList() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int save(ReclamacaoSugestao objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " INSERT INTO reclamacao_sugestao(data_criacao, descricao, id_autor, id_republica, tipo) VALUES (?, ?, ?, ?, ?) ",
                Statement.RETURN_GENERATED_KEYS
        );

        ps.setDate(1, java.sql.Date.valueOf(objeto.getDataCriacao()));
        ps.setString(2, objeto.getDescricao());
        ps.setInt(3, objeto.getIdAutor());
        ps.setInt(4, objeto.getIdRepublica());
        ps.setInt(5, objeto.getTipo().getValorInt());

        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);

        // Pessoas envolvidas
        ps = conn.prepareStatement(" INSERT INTO reclamacao_sugestao_envolvido(id_envolvido, id_reclamacao_sugestao) VALUES (?, ?) ");
        for (Morador m : objeto.getMoradoresEnvolvidos()) {
            ps.setInt(1, m.getIdPessoa());
            ps.setInt(2, id);
            ps.executeUpdate();
        }

        conn.close();
        return id;
    }

    @Override
    public int delete(ReclamacaoSugestao objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();

        // Pessoas envolvidas
        PreparedStatement ps = conn.prepareStatement(
                " DELETE FROM reclamacao_sugestao_envolvido WHERE id_reclamacao_sugestao = ?"
        );
        ps.setInt(1, objeto.getIdReclamacaoSugestao());
        ps.executeUpdate();

        ps = conn.prepareStatement(
                " DELETE FROM reclamacao_sugestao WHERE id_reclamacao_sugestao = ?",
                Statement.RETURN_GENERATED_KEYS
        );
        ps.setInt(1, objeto.getIdReclamacaoSugestao());
        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);

        conn.close();
        return id;
    }

    @Override
    public ReclamacaoSugestao update(ReclamacaoSugestao objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " UPDATE reclamacao_sugestao SET descricao = ?, tipo = ? WHERE id_reclamacao_sugestao = ? "
        );

        ps.setString(1, objeto.getDescricao());
        ps.setInt(2, objeto.getTipo().getValorInt());
        ps.setInt(3, objeto.getIdReclamacaoSugestao());

        ps.executeUpdate();

        conn.close();

        return objeto;
    }

    public ArrayList<ReclamacaoSugestao> getByIdRepublica(int id) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM reclamacao_sugestao WHERE id_republica = ? AND NOT excluida ");

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        ArrayList<ReclamacaoSugestao> lista = new ArrayList<>();
        while (rs.next()) {
            lista.add(new ReclamacaoSugestao(
                    rs.getInt("id_reclamacao_sugestao"),
                    rs.getDate("data_criacao").toLocalDate(),
                    Objects.isNull(rs.getObject("data_solucao")) ? null : rs.getDate("data_solucao").toLocalDate(),
                    rs.getString("descricao"),
                    rs.getBoolean("excluida"),
                    rs.getInt("id_autor"),
                    rs.getInt("id_republica"),
                    rs.getInt("tipo")
            ));
        }

        conn.close();
        return lista;
    }

    public ArrayList<ReclamacaoSugestao> getByDescricao(int idRepublica, String descricao) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM reclamacao_sugestao WHERE LOWER(descricao) LIKE LOWER(?) AND NOT excluida AND id_republica = ?");

        ps.setString(1, "%" + descricao + "%");
        ps.setInt(2, idRepublica);
        ResultSet rs = ps.executeQuery();
        ArrayList<ReclamacaoSugestao> lista = new ArrayList<>();
        while (rs.next()) {
            lista.add(new ReclamacaoSugestao(
                    rs.getInt("id_reclamacao_sugestao"),
                    rs.getDate("data_criacao").toLocalDate(),
                    Objects.isNull(rs.getObject("data_solucao")) ? null : rs.getDate("data_solucao").toLocalDate(),
                    rs.getString("descricao"),
                    rs.getBoolean("excluida"),
                    rs.getInt("id_autor"),
                    rs.getInt("id_republica"),
                    rs.getInt("tipo")
            ));
        }

        conn.close();
        return lista;
    }

    public ArrayList<ReclamacaoSugestao> getByPessoa(int idRepublica, String pessoa) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " SELECT * FROM reclamacao_sugestao rs"
                + " INNER JOIN morador m ON rs.id_autor = m.id_pessoa "
                + " INNER JOIN pessoa p ON m.id_pessoa = p.id_pessoa "
                + " WHERE LOWER(p.nome) LIKE LOWER(?) "
                + " AND NOT rs.excluida "
                + " AND rs.id_republica = ? "
        );

        ps.setString(1, "%" + pessoa + "%");
        ps.setInt(2, idRepublica);
        ResultSet rs = ps.executeQuery();
        ArrayList<ReclamacaoSugestao> lista = new ArrayList<>();
        while (rs.next()) {
            lista.add(new ReclamacaoSugestao(
                    rs.getInt("id_reclamacao_sugestao"),
                    rs.getDate("data_criacao").toLocalDate(),
                    Objects.isNull(rs.getObject("data_solucao")) ? null : rs.getDate("data_solucao").toLocalDate(),
                    rs.getString("descricao"),
                    rs.getBoolean("excluida"),
                    rs.getInt("id_autor"),
                    rs.getInt("id_republica"),
                    rs.getInt("tipo")
            ));
        }

        conn.close();
        return lista;
    }

}
