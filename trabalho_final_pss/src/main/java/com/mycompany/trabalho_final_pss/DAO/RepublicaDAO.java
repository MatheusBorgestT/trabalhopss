
package com.mycompany.trabalho_final_pss.DAO;

import com.mycompany.trabalho_final_pss.Utils.DAOUtil;
import com.mycompany.trabalho_final_pss.Model.Republica;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;

public class RepublicaDAO implements IDAO<Republica>{

    @Override
    public Republica get(int id) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM republica WHERE id_republica = ?");

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Republica r = null;
        if (rs.next()) {
            r = new Republica(
                    rs.getInt("id_republica"),
                    rs.getString("nome"),
                    rs.getDate("data_fundacao").toLocalDate(),
                    Objects.isNull(rs.getObject("data_extincao")) ? null : rs.getDate("data_extincao").toLocalDate(),
                    rs.getString("endereco"),
                    rs.getString("cep"),
                    rs.getString("bairro"),
                    rs.getString("ponto_referencia"),
                    Objects.isNull(rs.getObject("latitude")) ? null : rs.getDouble("latitude"),
                    Objects.isNull(rs.getObject("longitude")) ? null : rs.getDouble("longitude"),
                    rs.getString("vantagens"),
                    rs.getDouble("despesa_media"),
                    rs.getInt("numero_total_vagas"),
                    rs.getInt("numero_vagas_ocupadas"),
                    Objects.isNull(rs.getObject("estatuto")) ? null : rs.getString("estatuto"),
                    rs.getInt("id_representante")
            );
        }

        conn.close();
        return r;
    }

    @Override
    public ArrayList<Republica> getList() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int save(Republica objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();

        // Republica
        PreparedStatement ps = conn.prepareStatement(
                "insert into republica (nome, data_fundacao, endereco, cep, bairro, ponto_referencia," +
                "                       latitude, longitude, vantagens, despesa_media, numero_total_vagas, numero_vagas_ocupadas," +
                "                       id_representante)" +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);",
                Statement.RETURN_GENERATED_KEYS
        );
        ps.setString(1, objeto.getNome());
        ps.setDate(2, java.sql.Date.valueOf(objeto.getDataFundacao()));
        ps.setString(3, objeto.getEndereco());
        ps.setString(4, objeto.getCep());
        ps.setString(5, objeto.getBairro());
        ps.setString(6, objeto.getPontoReferencia());
        ps.setDouble(7, objeto.getLatitude());
        ps.setDouble(8, objeto.getLongitude());
        ps.setString(9, objeto.getVantagens());
        ps.setDouble(10, objeto.getDespesaMedia());
        ps.setInt(11, objeto.getNumeroTotalVagas());
        ps.setInt(12, objeto.getNumeroVagasOcupadas());
        ps.setInt(13, objeto.getIdRepresentante());
        
        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);

        conn.close();
        return id;
    }

    @Override
    public int delete(Republica objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        
        // Republica
        PreparedStatement ps = conn.prepareStatement(
                "DELETE FROM republica WHERE id_republica = ?",
                Statement.RETURN_GENERATED_KEYS
        );
        ps.setInt(1, objeto.getIdRepublica());
        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);
        
        conn.close();
        return id;
    }

    @Override
    public Republica update(Republica objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        
        PreparedStatement ps = conn.prepareStatement(
                " UPDATE republica SET " +
                " nome = ?, " +
                " data_fundacao = ?, " +
                " data_extincao = ?, " +
                " endereco = ?, " +
                " cep = ?, " +
                " bairro = ?, " +
                " ponto_referencia = ?, " +
                " latitude = ?, " +
                " longitude = ?, " +
                " vantagens = ?, " +
                " despesa_media = ?, " +
                " numero_total_vagas = ?, " +
                " numero_vagas_ocupadas = ?, " +
                " estatuto = ?, " +
                " id_representante = ?" +
                " WHERE id_republica = ? "
        );
        
        ps.setString(1, objeto.getNome());
        ps.setDate(2, java.sql.Date.valueOf(objeto.getDataFundacao()));
        ps.setDate(3, Objects.isNull(objeto.getDataExtincao()) ? null : java.sql.Date.valueOf(objeto.getDataExtincao()));
        ps.setString(4, objeto.getEndereco());
        ps.setString(5, objeto.getCep());
        ps.setString(6, objeto.getBairro());
        ps.setString(7, objeto.getPontoReferencia());
        ps.setDouble(8, objeto.getLatitude());
        ps.setDouble(9, objeto.getLongitude());
        ps.setString(10, objeto.getVantagens());
        ps.setDouble(11, objeto.getDespesaMedia());
        ps.setInt(12, objeto.getNumeroTotalVagas());
        ps.setInt(13, objeto.getNumeroVagasOcupadas());
        ps.setString(14, objeto.getEstatuto());
        ps.setInt(15, objeto.getIdRepresentante());
        ps.setInt(16, objeto.getIdRepublica());
        
        ps.executeUpdate();
        
        conn.close();
        return objeto;
    }
    
}
