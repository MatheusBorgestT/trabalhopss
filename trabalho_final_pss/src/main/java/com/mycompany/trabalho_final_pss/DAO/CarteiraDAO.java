package com.mycompany.trabalho_final_pss.DAO;

import com.mycompany.trabalho_final_pss.Utils.DAOUtil;
import com.mycompany.trabalho_final_pss.Model.Carteira;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CarteiraDAO implements IDAO<Carteira> {

    @Override
    public Carteira get(int id) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM carteira WHERE id_republica = ?");
        
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Carteira c = null;
        if (rs.next()) {
            c = new Carteira(
                    rs.getInt("id_republica"),
                    rs.getDouble("saldo")
            );
        }

        conn.close();
        return c;
    }

    @Override
    public ArrayList<Carteira> getList() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int save(Carteira objeto) throws SQLException {
        var conn = DAOUtil.getConnection();
        var ps = conn.prepareStatement(
                " INSERT INTO carteira(id_republica, saldo) VALUES (?, ?) ",
                Statement.RETURN_GENERATED_KEYS
        );
        ps.setInt(1, objeto.getIdRepublica());
        ps.setDouble(2, 0.0);
        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);

        conn.close();
        return id;
    }

    @Override
    public int delete(Carteira objeto) throws SQLException {
        var conn = DAOUtil.getConnection();
        var ps = conn.prepareStatement(
                "DELETE FROM carteira WHERE id_republica = ?",
                Statement.RETURN_GENERATED_KEYS
        );
        ps.setInt(1, objeto.getIdRepublica());
        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);
        
        conn.close();
        return id;
    }

    @Override
    public Carteira update(Carteira objeto) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
