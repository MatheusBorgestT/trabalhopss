/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.trabalho_final_pss.DAO;

import com.mycompany.trabalho_final_pss.Model.InstanciaHistoricoRepublicas;
import com.mycompany.trabalho_final_pss.Utils.DAOUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Julia Dalcamini
 */
public class InstanciaHistoricoRepublicasDAO implements IDAO<InstanciaHistoricoRepublicas> {

    @Override
    public InstanciaHistoricoRepublicas get(int id) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM instancia_historico_republicas WHERE id_pessoa = ?");

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        InstanciaHistoricoRepublicas h = null;
        if (rs.next()) {
            h = new InstanciaHistoricoRepublicas(
                    rs.getString("nome_republica_na_epoca"),
                    rs.getDouble("media_reputacao"),
                    rs.getInt("id_republica_na_epoca"),
                    rs.getInt("id_representante_na_epoca"),
                    rs.getDate("data_entrada").toLocalDate(),
                    rs.getDouble("valor_aluguel"),
                    rs.getDate("data_saida").toLocalDate(),
                    rs.getInt("id_pessoa")
            );
        }

        conn.close();
        return h;
    }

    @Override
    public ArrayList<InstanciaHistoricoRepublicas> getList() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int save(InstanciaHistoricoRepublicas objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " INSERT INTO instancia_historico_republicas(nome_republica_na_epoca, media_reputacao,"
                        + "id_republica_na_epoca, id_representante_na_epoca, data_entrada, valor_aluguel,"
                        + "id_pessoa) "
                + " VALUES(?, ?, ?, ?, ?, ?, ?) "
        );

        ps.setString(1, objeto.getNomeRepublicaNaEpoca());
        ps.setDouble(2, objeto.getMediaReputacao());
        ps.setInt(3, objeto.getRepublicaNaEpoca().getIdRepublica());
        ps.setInt(4, objeto.getIdRepresentanteNaEpoca());
        ps.setDate(5, java.sql.Date.valueOf(objeto.getDataEntrada()));
        ps.setDouble(6, objeto.getValorAluguel());
        ps.setInt(7, objeto.getIdPessoa());

        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);

        conn.close();
        return id;
    }

    @Override
    public int delete(InstanciaHistoricoRepublicas objeto) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public InstanciaHistoricoRepublicas update(InstanciaHistoricoRepublicas objeto) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ArrayList<InstanciaHistoricoRepublicas> getListByIdRepublica(int idRepublica) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                "SELECT * FROM instancia_historico_republicas WHERE id_republica_na_epoca = ? AND data_saida is NULL"
        );

        ps.setInt(1, idRepublica);
        ResultSet rs = ps.executeQuery();

        ArrayList<InstanciaHistoricoRepublicas> listaHistorico = new ArrayList<>();
        while (rs.next()) {
            listaHistorico.add(new InstanciaHistoricoRepublicas(
                    rs.getString("nome_republica_na_epoca"),
                    rs.getDouble("media_reputacao"),
                    rs.getInt("id_republica_na_epoca"),
                    rs.getInt("id_representante_na_epoca"),
                    rs.getDate("data_entrada").toLocalDate(),
                    rs.getDouble("valor_aluguel"),
                    rs.getDate("data_saida").toLocalDate(),
                    rs.getInt("id_pessoa")
            ));
        }

        conn.close();
        return listaHistorico;
    }
    
}
