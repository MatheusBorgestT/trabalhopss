package com.mycompany.trabalho_final_pss.DAO;

import com.mycompany.trabalho_final_pss.Utils.DAOUtil;
import com.mycompany.trabalho_final_pss.Model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UsuarioDAO implements IDAO<Usuario> {

    public boolean usuarioJaCadastrado(String usuario) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM usuario WHERE nome_usuario = ?");

        ps.setString(1, usuario);
        ResultSet rs = ps.executeQuery();
        boolean result = rs.next();

        conn.close();
        return result;
    }

    @Override
    public Usuario get(int id) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM pessoa WHERE id_pessoa = ?");
        
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Usuario u = null;
        if (rs.next()) {
            u = new Usuario(
                    rs.getInt("id_usuario"),
                    rs.getString("nome_usuaro"),
                    rs.getString("senha"),
                    rs.getInt("id_pessoa")
            );
        }

        conn.close();
        return u;
    }

    @Override
    public ArrayList<Usuario> getList() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int save(Usuario objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                "INSERT INTO usuario(nome_usuario, senha, id_pessoa) VALUES(?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS
        );

        ps.setString(1, objeto.getNomeUsuario());
        ps.setString(2, objeto.getSenha());
        ps.setInt(3, objeto.getIdPessoa());

        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);

        conn.close();
        return id;
    }

    @Override
    public int delete(Usuario objeto) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario update(Usuario objeto) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Usuario getLoginByUsuario(String nomeUsuario) throws SQLException {
        // Essa parte é um pouco complicada de explicar por alto. Sugiro um
        // estudo breve de jdbc (java database connector)
        
        // Na linha abaixo é utilizado da classe Connector para criar uma
        // conexão
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM usuario WHERE nome_usuario = ?");

        ps.setString(1, nomeUsuario);
        ResultSet rs = ps.executeQuery();
        Usuario usuario = null;
        if (rs.next()) {
            usuario = new Usuario(
                    rs.getInt("id_usuario"),
                    rs.getString("nome_usuario"),
                    rs.getString("senha"),
                    rs.getInt("id_pessoa")
            );
        }
        
        // IMPORTANTE!!!: Sempre fechar a conexão ao fim da função. Caso
        // contrário, alguma hora vai dar pau e consequentemente dor de cabeça
        conn.close();
        return usuario;
    }

}
