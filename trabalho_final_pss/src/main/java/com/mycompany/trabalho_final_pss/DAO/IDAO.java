package com.mycompany.trabalho_final_pss.DAO;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IDAO<T> {

    public T get(int id) throws SQLException;

    public ArrayList<T> getList() throws SQLException;

    public int save(T objeto) throws SQLException;

    public int delete(T objeto) throws SQLException;

    public T update(T objeto) throws SQLException;

}
