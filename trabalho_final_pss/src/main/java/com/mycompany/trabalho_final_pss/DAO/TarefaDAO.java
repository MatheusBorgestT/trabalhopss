package com.mycompany.trabalho_final_pss.DAO;

import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.Tarefa;
import com.mycompany.trabalho_final_pss.Utils.DAOUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;

public class TarefaDAO implements IDAO<Tarefa>{

    @Override
    public Tarefa get(int id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Tarefa> getList() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int save(Tarefa objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " INSERT INTO tarefa(data_agendamento, data_limite_termino, descricao, finalizada, periodicidade, id_autor, id_republica) values (?, ?, ?, ?, ?, ?, ?) ",
                Statement.RETURN_GENERATED_KEYS
        );
        
        ps.setDate(1, java.sql.Date.valueOf(objeto.getDataAgendamento()));
        ps.setDate(2, java.sql.Date.valueOf(objeto.getDataLimiteTermino()));
        ps.setString(3, objeto.getDescricao());
        ps.setBoolean(4, objeto.isFinalizada());
        ps.setInt(5, objeto.getPeriodicidade().getValorInt());
        ps.setInt(6, objeto.getIdAutor());
        ps.setInt(7, objeto.getIdRepublica());
        
        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);
        
        //Pessoas reponsaveis
        ps = conn.prepareStatement(" INSERT INTO tarefa_morador(id_tarefa, id_morador) VALUES (?, ?) ");
        for (Morador m : objeto.getMoradoresResponsaveis()) {
            ps.setInt(1, id);
            ps.setInt(2, m.getIdPessoa());
            ps.executeUpdate();
        }

        conn.close();
        return id;
    }

    @Override
    public int delete(Tarefa objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();

        //Pessoas reponsaveis
        PreparedStatement ps = conn.prepareStatement(
                " DELETE FROM tarefa_morador WHERE id_tarefa = ?"
        );
        ps.setInt(1, objeto.getIdTarefa());
        ps.executeUpdate();

        ps = conn.prepareStatement(
                " DELETE FROM tarefa WHERE id_tarefa = ?",
                Statement.RETURN_GENERATED_KEYS
        );
        ps.setInt(1, objeto.getIdTarefa());
        int linhasAfetadas = ps.executeUpdate();
        int id = DAOUtil.extrairIdGerado(linhasAfetadas, ps);

        conn.close();
        return id;
    }

    @Override
    public Tarefa update(Tarefa objeto) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " UPDATE tarefa SET descricao = ?, data_limite_termino = ? WHERE id_tarefa = ? "
        );

        ps.setString(1, objeto.getDescricao());
        ps.setDate(2, java.sql.Date.valueOf(objeto.getDataLimiteTermino()));
        ps.setInt(3, objeto.getIdTarefa());

        ps.executeUpdate();

        conn.close();

        return objeto;
    }
    
    public ArrayList<Tarefa> getByIdRepublica(int id) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM tarefa WHERE id_republica = ? AND NOT finalizada ");

        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        ArrayList<Tarefa> lista = new ArrayList<>();
        while (rs.next()) {
            lista.add(new Tarefa(
                    rs.getInt("id_tarefa"),
                    rs.getDate("data_agendamento").toLocalDate(),
                    Objects.isNull(rs.getObject("data_limite_termino")) ? null : rs.getDate("data_limite_termino").toLocalDate(),
                    rs.getString("descricao"),
                    rs.getBoolean("finalizada"),
                    rs.getInt("id_autor"),
                    rs.getInt("id_republica"),
                    rs.getInt("periodicidade")
            ));
        }

        conn.close();
        return lista;
    }
    
    public ArrayList<Tarefa> getByDescricao(int idRepublica, String descricao) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM tarefa WHERE LOWER(descricao) LIKE LOWER(?) AND NOT finalizada AND id_republica = ?");

        ps.setString(1, "%" + descricao + "%");
        ps.setInt(2, idRepublica);
        ResultSet rs = ps.executeQuery();
        ArrayList<Tarefa> lista = new ArrayList<>();
        while (rs.next()) {
            lista.add(new Tarefa(
                    rs.getInt("id_tarefa"),
                    rs.getDate("data_agendamento").toLocalDate(),
                    Objects.isNull(rs.getObject("data_limite_termino")) ? null : rs.getDate("data_limite_termino").toLocalDate(),
                    rs.getString("descricao"),
                    rs.getBoolean("finalizada"),
                    rs.getInt("id_autor"),
                    rs.getInt("id_republica"),
                    rs.getInt("periodicidade")
            ));
        }

        conn.close();
        return lista;
    }
    
    public ArrayList<Tarefa> getByPessoa(int idRepublica, String pessoa) throws SQLException {
        Connection conn = DAOUtil.getConnection();
        PreparedStatement ps = conn.prepareStatement(
                " SELECT * FROM tarefa t"
                + " INNER JOIN morador m ON t.id_autor = m.id_pessoa "
                + " INNER JOIN pessoa p ON m.id_pessoa = p.id_pessoa "
                + " WHERE LOWER(p.nome) LIKE LOWER(?) "
                + " AND NOT t.finalizada "
                + " AND t.id_republica = ? "
        );

        ps.setString(1, "%" + pessoa + "%");
        ps.setInt(2, idRepublica);
        ResultSet rs = ps.executeQuery();
        ArrayList<Tarefa> lista = new ArrayList<>();
        while (rs.next()) {
            lista.add(new Tarefa(
                    rs.getInt("id_tarefa"),
                    rs.getDate("data_agendamento").toLocalDate(),
                    Objects.isNull(rs.getObject("data_limite_termino")) ? null : rs.getDate("data_limite_termino").toLocalDate(),
                    rs.getString("descricao"),
                    rs.getBoolean("finalizada"),
                    rs.getInt("id_autor"),
                    rs.getInt("id_republica"),
                    rs.getInt("periodicidade")
            ));
        }

        conn.close();
        return lista;
    }
    
}
