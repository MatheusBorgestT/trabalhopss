package com.mycompany.trabalho_final_pss.DAO;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {

    private final static String dbUrl = "jdbc:sqlite:../banco.db";

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(dbUrl);
    }

}
