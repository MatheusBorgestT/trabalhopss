package com.mycompany.trabalho_final_pss.Utils;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAOUtil {

    private final static String dbUrl = "jdbc:sqlite:../banco.db";

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(dbUrl);
    }
    
    public static int extrairIdGerado(int linhasAfetadas, Statement stmt) throws SQLException {
        if (linhasAfetadas == 0) {
            throw new SQLException("Creating user failed, no rows affected.");
        }

        try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                return (int) generatedKeys.getLong(1);
            }
            else {
                throw new SQLException("Creating user failed, no ID obtained.");
            }
        }
    }

}
