package com.mycompany.trabalho_final_pss.View.Tarefa;

import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ManterTarefaView extends javax.swing.JInternalFrame {

    private static ManterTarefaView instancia;
    
    private ManterTarefaView() {
        initComponents();
    }
    
    public static ManterTarefaView getInstancia() {
        if (instancia == null) {
            instancia = new ManterTarefaView();
        }
        return instancia;
    }
    
    public void setBotaoConfirmarVisible(boolean b) {
        botaoConfirmarCadastroTarefa.setVisible(b);
    }
    
    public void preencherTabelaMoradores(Object[] valores) {
        var model = (DefaultTableModel) tblMoradores.getModel();
        model.setRowCount(0);
        for (Object v : valores) {
            model.addRow(new Object[]{v});
        }
    }
    
    public void preencherTabelaResponsaveis(Object[] valores) {
        var model = (DefaultTableModel) tblResponsaveis.getModel();
        model.setRowCount(0);
        for (Object v : valores) {
            model.addRow(new Object[]{v});
        }
    }
    
    public int getSelectedRowTabelaMoradores() {
        return tblMoradores.getSelectedRow();
    }
    
    public int getSelectedRowTabelaResponsaveis() {
        return tblResponsaveis.getSelectedRow();
    }
    
    public void setTextCampoData(String valor) {
        campoDataAgendamento.setText(valor);
    }
    
    public Map<String, Object> getValoresDosCampos() {
        HashMap<String, Object> valores = new HashMap<>();
        
        valores.put("descricao", campoDescricaoTarefa.getText());
        valores.put("dataAgendamento", campoDataAgendamento.getText());
        valores.put("dataTermino", campoDataTermino.getText());
        
        return valores;
    }
    
    public void setValoresDosCampos(Map<String, Object> valores) {
        campoDescricaoTarefa.setText((String)valores.get("descricao"));
        campoDataAgendamento.setText((String)valores.get("dataAgendamento"));
        campoDataTermino.setText((String)valores.get("dataTermino"));
    }

    public JTable getTblResponsaveis() {
        return tblResponsaveis;
    }

    public JTable getTblMoradores() {
        return tblMoradores;
    }
    
    //--------------------------------------------------------------------------
    // Botões
    //--------------------------------------------------------------------------
    public void adicionarActionListenerBotaoConfirmar(ActionListener e) {
        botaoConfirmarCadastroTarefa.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoAdicionarResponsavel(ActionListener e) {
        botaoAdicionarResponsavel.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoRemoverResponsavel(ActionListener e) {
        botaoRemoverResponsavel.addActionListener(e);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        campoDataAgendamento = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        campoDataTermino = new javax.swing.JFormattedTextField();
        botaoConfirmarCadastroTarefa = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblResponsaveis = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        campoDescricaoTarefa = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblMoradores = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        botaoAdicionarResponsavel = new javax.swing.JButton();
        botaoRemoverResponsavel = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setClosable(true);

        jLabel4.setText("Data de término:");

        botaoConfirmarCadastroTarefa.setText("Confirmar");

        tblResponsaveis.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblResponsaveis);

        jLabel1.setText("Descrição da tarefa:");

        tblMoradores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblMoradores);

        jLabel2.setText("Responsáveis pela tarefa:");

        botaoAdicionarResponsavel.setText("->");

        botaoRemoverResponsavel.setText("<-");

        jLabel3.setText("Data de agendamento:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(campoDescricaoTarefa, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(campoDataAgendamento, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(29, 29, 29)
                                        .addComponent(botaoConfirmarCadastroTarefa, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(campoDataTermino, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botaoAdicionarResponsavel)
                            .addComponent(botaoRemoverResponsavel))
                        .addGap(44, 44, 44)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(campoDescricaoTarefa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(campoDataTermino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(37, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(72, 72, 72)
                                .addComponent(botaoAdicionarResponsavel)
                                .addGap(18, 18, 18)
                                .addComponent(botaoRemoverResponsavel)))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(campoDataAgendamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(botaoConfirmarCadastroTarefa)
                                .addContainerGap())))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoAdicionarResponsavel;
    private javax.swing.JButton botaoConfirmarCadastroTarefa;
    private javax.swing.JButton botaoRemoverResponsavel;
    private javax.swing.JFormattedTextField campoDataAgendamento;
    private javax.swing.JFormattedTextField campoDataTermino;
    private javax.swing.JTextField campoDescricaoTarefa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblMoradores;
    private javax.swing.JTable tblResponsaveis;
    // End of variables declaration//GEN-END:variables
}
