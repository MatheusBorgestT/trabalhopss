package com.mycompany.trabalho_final_pss.View.MenuPrincipal;

import java.awt.Color;
import java.awt.event.ActionListener;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class MenuPrincipalView extends javax.swing.JFrame {

    private static MenuPrincipalView instancia;

    private MenuPrincipalView() {
        initComponents();
    }

    public static MenuPrincipalView getInstancia() {
        if (instancia == null) {
            instancia = new MenuPrincipalView();
        }
        return instancia;
    }

    public void setTodosMenusItensVisiveis(boolean b) {
        for (int i = 0; i < jMenuBar.getComponentCount(); i++) {
            var menu = (JMenu) (jMenuBar.getComponent(i));
            for (int j = 0; j < menu.getMenuComponentCount(); j++) {
                var menuItem = (JMenuItem) (menu.getMenuComponent(j));
                menuItem.setVisible(b);
            }
        }
    }

    public void addToDesktop(JInternalFrame frame) {
        desktop.remove(frame);
        desktop.add(frame);
    }

    // -------------------------------------------------------------------------
    // Tornando Botões Visíveis
    // -------------------------------------------------------------------------
    // UC1 - Manter República
    public void tornarMinhaRepublicaVisivel(boolean b) {
        jMenuRepublica.setVisible(b);
    }

    // UC13 - Buscar Vagas
    public void tornarBuscarVagasVisivel(boolean b) {
        jMenuBuscarVagas.setVisible(b);
    }

    // UC10 - Consultar minhas receitas e despesas
    public void tornarConsultarLancamentoVisivel(boolean b) {
        jMenuConsultarLancamento.setVisible(b);
    }

    // UC11 - Consultar resultado mensal
    public void tornarConsultarResultadoVisivel(boolean b) {
        jMenuConsultarResultado.setVisible(b);
    }

    // UC5 - Convidar moradores
    public void tornarConvidarVisivel(boolean b) {
        jMenuConvidar.setVisible(b);
    }

    // UC1 - Criar republica
    public void tornarCriarRepublicaVisivel(boolean b) {
        jMenuCriarRepublica.setVisible(b);
    }

    // UC15 - Fazer estornos de lancamento
    public void tornarEstornosVisivel(boolean b) {
        jMenuEstornos.setVisible(b);
    }

    // UC7 - Manter perfil
    public void tornarMeuPerfilVisivel(boolean b) {
        jMenuMeuPerfil.setVisible(b);
    }

    // UC2 - Manter moradores
    public void tornarMoradoresRepublicaVisivel(boolean b) {
        jMenuMoradoresRepublica.setVisible(b);
    }

    // UC17 - Aceitar convite
    public void tornarNotificacoesVisivel(boolean b) {
        jMenuNotificacoes.setVisible(b);
    }

    // UC4 - Manter receitas e despesas
    public void tornarReceitasDespesasVisivel(boolean b) {
        jMenuReceitasDespesas.setVisible(b);
    }

    // UC8 - Manter reclamacoes/sugestoes
    public void tornarReclamacoesVisivel(boolean b) {
        jMenuReclamacoes.setVisible(b);
    }

    // UC12 - Registrar conclusao da tarefa
    public void tornarRegistrarConclusaoVisivel(boolean b) {
        jMenuRegistrarConclusao.setVisible(b);
    }

    // UC9 - Registrar pagamento de receitas ou despesas
    public void tornarRegistrarPagamentoVisivel(boolean b) {
        jMenuRegistrarPagamento.setVisible(b);
    }

    // UC6 - Confirmar a solucao de reclamacoes/sugestoes
    public void tornarSolucaoVisivel(boolean b) {
        jMenuSolucao.setVisible(b);
    }

    // UC3 - Manter tarefas
    public void tornarTarefasGeraisVisivel(boolean b) {
        jMenuTarefasGerais.setVisible(b);
    }

    public void tornarEncerrarSessaoVisivel(boolean b) {
        jMenuEncerrarSessao.setVisible(b);
    }

    // -------------------------------------------------------------------------
    // Acionando Botões
    // -------------------------------------------------------------------------
    public void adicionarActionListenerOpcaoMinhaRepublica(ActionListener e) {
        jMenuRepublica.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoBuscarVagas(ActionListener e) {
        jMenuBuscarVagas.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoConsultarLancamento(ActionListener e) {
        jMenuConsultarLancamento.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoConsultarResultado(ActionListener e) {
        jMenuConsultarResultado.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoConvidar(ActionListener e) {
        jMenuConvidar.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoCriarRepublica(ActionListener e) {
        jMenuCriarRepublica.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoEstornos(ActionListener e) {
        jMenuEstornos.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoMeuPerfil(ActionListener e) {
        jMenuMeuPerfil.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoMoradoresRepublica(ActionListener e) {
        jMenuMoradoresRepublica.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoNotificacoes(ActionListener e) {
        jMenuNotificacoes.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoReceitasDespesas(ActionListener e) {
        jMenuReceitasDespesas.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoReclamacoes(ActionListener e) {
        jMenuReclamacoes.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoRegistrarConclusao(ActionListener e) {
        jMenuRegistrarConclusao.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoRegistrarPagamento(ActionListener e) {
        jMenuRegistrarPagamento.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoSolucao(ActionListener e) {
        jMenuSolucao.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoTarefasGerais(ActionListener e) {
        jMenuTarefasGerais.addActionListener(e);
    }

    public void adicionarActionListenerOpcaoEncerrarSessao(ActionListener e) {
        jMenuEncerrarSessao.addActionListener(e);
    }

    // -------------------------------------------------------------------------
    // Rodapé
    // -------------------------------------------------------------------------
    public void setUsuarioRodape(String valor) {
        jLabelRodapeUsuario.setText("Usuário: " + valor);
    }

    public void setTipoUsuarioRodape(String valor) {
        jLabelRodapeTipoUsuario.setText("Tipo de Usuário: " + valor);
    }

    public void setRepublicaRodape(String valor) {
        jLabelRodapeRepublica.setText("República: " + valor);
    }

    public void setQuantidadeMoradoresRepublicaRodape(int valor) {
        if (valor == -1) {
            jLabelRodapeQuantidadeMoradoresRepublica.setText("Moradores na República: ");
        } else {
            jLabelRodapeQuantidadeMoradoresRepublica.setText("Moradores na República: " + String.valueOf(valor));
        }
    }

    public void setQuantidadeNotificacoesRodape(int valor) {
        jButtonRodapeNotificacoes.setText(String.valueOf(valor));

        if (valor == 0) {
            jButtonRodapeNotificacoes.setForeground(Color.BLACK);
        } else {
            jButtonRodapeNotificacoes.setForeground(Color.RED);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        desktop = new javax.swing.JDesktopPane();
        jPanel1 = new javax.swing.JPanel();
        jButtonRodapeNotificacoes = new javax.swing.JButton();
        jLabelRodapeQuantidadeMoradoresRepublica = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabelRodapeRepublica = new javax.swing.JLabel();
        jLabelRodapeTipoUsuario = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jLabelRodapeUsuario = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        jMenuBar = new javax.swing.JMenuBar();
        menuRepublica = new javax.swing.JMenu();
        jMenuRepublica = new javax.swing.JMenuItem();
        jMenuMoradoresRepublica = new javax.swing.JMenuItem();
        jMenuSolucao = new javax.swing.JMenuItem();
        jMenuConvidar = new javax.swing.JMenuItem();
        jMenuBuscarVagas = new javax.swing.JMenuItem();
        jMenuConsultarResultado = new javax.swing.JMenuItem();
        jMenuReclamacoes = new javax.swing.JMenuItem();
        jMenuCriarRepublica = new javax.swing.JMenuItem();
        menuMorador = new javax.swing.JMenu();
        jMenuMeuPerfil = new javax.swing.JMenuItem();
        jMenuNotificacoes = new javax.swing.JMenuItem();
        menuTarefas = new javax.swing.JMenu();
        jMenuTarefasGerais = new javax.swing.JMenuItem();
        jMenuRegistrarConclusao = new javax.swing.JMenuItem();
        menuLancamentos = new javax.swing.JMenu();
        jMenuReceitasDespesas = new javax.swing.JMenuItem();
        jMenuEstornos = new javax.swing.JMenuItem();
        jMenuConsultarLancamento = new javax.swing.JMenuItem();
        jMenuRegistrarPagamento = new javax.swing.JMenuItem();
        menuSair = new javax.swing.JMenu();
        jMenuEncerrarSessao = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout desktopLayout = new javax.swing.GroupLayout(desktop);
        desktop.setLayout(desktopLayout);
        desktopLayout.setHorizontalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 910, Short.MAX_VALUE)
        );
        desktopLayout.setVerticalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 553, Short.MAX_VALUE)
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButtonRodapeNotificacoes.setText("-1");
        jButtonRodapeNotificacoes.setPreferredSize(new java.awt.Dimension(30, 30));

        jLabelRodapeQuantidadeMoradoresRepublica.setText("Moradores na República: -1");

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabelRodapeRepublica.setText("República: Blank");

        jLabelRodapeTipoUsuario.setText("Tipo de Usuário: Blank");

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabelRodapeUsuario.setText("Usuário: Blank");

        jSeparator6.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelRodapeUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelRodapeTipoUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelRodapeRepublica)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelRodapeQuantidadeMoradoresRepublica)
                .addGap(6, 6, 6)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonRodapeNotificacoes, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jSeparator2)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButtonRodapeNotificacoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jLabelRodapeQuantidadeMoradoresRepublica, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabelRodapeRepublica, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator4)
            .addComponent(jLabelRodapeTipoUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator5)
            .addComponent(jLabelRodapeUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator6)
        );

        menuRepublica.setText("República");

        jMenuRepublica.setText("Minha República");
        menuRepublica.add(jMenuRepublica);

        jMenuMoradoresRepublica.setText("Moradores da República");
        menuRepublica.add(jMenuMoradoresRepublica);

        jMenuSolucao.setText("Solução de Reclamações/Sugestões");
        menuRepublica.add(jMenuSolucao);

        jMenuConvidar.setText("Convidar");
        menuRepublica.add(jMenuConvidar);

        jMenuBuscarVagas.setText("Buscar Vagas");
        menuRepublica.add(jMenuBuscarVagas);

        jMenuConsultarResultado.setText("Consultar Resultado Mensal");
        menuRepublica.add(jMenuConsultarResultado);

        jMenuReclamacoes.setText("Reclamações/Sugestões");
        menuRepublica.add(jMenuReclamacoes);

        jMenuCriarRepublica.setText("Criar República");
        menuRepublica.add(jMenuCriarRepublica);

        jMenuBar.add(menuRepublica);

        menuMorador.setText("Morador");

        jMenuMeuPerfil.setText("Meu Perfil");
        menuMorador.add(jMenuMeuPerfil);

        jMenuNotificacoes.setText("Notificações");
        menuMorador.add(jMenuNotificacoes);

        jMenuBar.add(menuMorador);

        menuTarefas.setText("Tarefas");

        jMenuTarefasGerais.setText("Tarefas Gerais");
        menuTarefas.add(jMenuTarefasGerais);

        jMenuRegistrarConclusao.setText("Registrar Conclusão");
        menuTarefas.add(jMenuRegistrarConclusao);

        jMenuBar.add(menuTarefas);

        menuLancamentos.setText("Lançamentos");

        jMenuReceitasDespesas.setText("Receitas e Despesas");
        menuLancamentos.add(jMenuReceitasDespesas);

        jMenuEstornos.setText("Estornos");
        menuLancamentos.add(jMenuEstornos);

        jMenuConsultarLancamento.setText("Consultar");
        menuLancamentos.add(jMenuConsultarLancamento);

        jMenuRegistrarPagamento.setText("Registrar Pagamento");
        menuLancamentos.add(jMenuRegistrarPagamento);

        jMenuBar.add(menuLancamentos);

        menuSair.setText("Sair");

        jMenuEncerrarSessao.setText("Encerrar Sessão");
        menuSair.add(jMenuEncerrarSessao);

        jMenuBar.add(menuSair);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktop)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(desktop)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane desktop;
    private javax.swing.JButton jButtonRodapeNotificacoes;
    private javax.swing.JLabel jLabelRodapeQuantidadeMoradoresRepublica;
    private javax.swing.JLabel jLabelRodapeRepublica;
    private javax.swing.JLabel jLabelRodapeTipoUsuario;
    private javax.swing.JLabel jLabelRodapeUsuario;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenuItem jMenuBuscarVagas;
    private javax.swing.JMenuItem jMenuConsultarLancamento;
    private javax.swing.JMenuItem jMenuConsultarResultado;
    private javax.swing.JMenuItem jMenuConvidar;
    private javax.swing.JMenuItem jMenuCriarRepublica;
    private javax.swing.JMenuItem jMenuEncerrarSessao;
    private javax.swing.JMenuItem jMenuEstornos;
    private javax.swing.JMenuItem jMenuMeuPerfil;
    private javax.swing.JMenuItem jMenuMoradoresRepublica;
    private javax.swing.JMenuItem jMenuNotificacoes;
    private javax.swing.JMenuItem jMenuReceitasDespesas;
    private javax.swing.JMenuItem jMenuReclamacoes;
    private javax.swing.JMenuItem jMenuRegistrarConclusao;
    private javax.swing.JMenuItem jMenuRegistrarPagamento;
    private javax.swing.JMenuItem jMenuRepublica;
    private javax.swing.JMenuItem jMenuSolucao;
    private javax.swing.JMenuItem jMenuTarefasGerais;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JMenu menuLancamentos;
    private javax.swing.JMenu menuMorador;
    private javax.swing.JMenu menuRepublica;
    private javax.swing.JMenu menuSair;
    private javax.swing.JMenu menuTarefas;
    // End of variables declaration//GEN-END:variables
}
