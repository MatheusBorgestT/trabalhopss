
package com.mycompany.trabalho_final_pss.View.ManterRepublica;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFormattedTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ManterRepublicaView extends javax.swing.JInternalFrame {

    private static ManterRepublicaView instancia;
 
    private ManterRepublicaView() {
        initComponents();
    }
    
    public static ManterRepublicaView getInstancia() {
        if (instancia == null) {
            instancia = new ManterRepublicaView();
        }
        return instancia;
    }
    
    public void adicionarActionListenerBotaoConfirmar(ActionListener e) {
        jButtonConfirmar.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoCancelar(ActionListener e) {
        jButtonCancelar.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoExcluir(ActionListener e) {
        jButtonExcluir.addActionListener(e);
    }

    public Map<String, String> getValoresDosCampos() {
        Map<String, String> valores = new HashMap<>();
        
        valores.put("nome", jTextFieldNome.getText());
        valores.put("dataFundacao", jFormattedTextFieldDataFundacao.getText());
        valores.put("despesasMediasMorador", jTextFieldDespesasMediasMorador.getText());
        valores.put("totalVagas", jTextFieldTotalVagas.getText());
        valores.put("vagasOcupadas", jTextFieldVagasOcupadas.getText());
        valores.put("vantagens", jTextAreaVantagens.getText());
        valores.put("codigoEtica", jTextAreaCodigoEtica.getText());
        valores.put("logradouro", jTextFieldLogradouro.getText());
        valores.put("bairro", jTextFieldBairro.getText());
        valores.put("pontoReferencia", jTextFieldPontoReferencia.getText());
        valores.put("latitude", jTextFieldLatitude.getText());
        valores.put("longitude", jTextFieldLongitude.getText());
        valores.put("cep", jFormattedTextFieldCep.getText());
        
        return valores;
    }
    
    public void setValoresDosCampos(Map<String, String> valores) {
        jTextFieldNome.setText(valores.get("nome"));
        jFormattedTextFieldDataFundacao.setText(valores.get("dataFundacao"));
        jTextFieldDespesasMediasMorador.setText(valores.get("despesasMediasMorador"));
        jTextFieldTotalVagas.setText(valores.get("totalVagas"));
        jTextFieldVagasOcupadas.setText(valores.get("vagasOcupadas"));
        jTextAreaVantagens.setText(valores.get("vantagens"));
        jTextAreaCodigoEtica.setText(valores.get("codigoEtica"));
        jTextFieldLogradouro.setText(valores.get("logradouro"));
        jTextFieldBairro.setText(valores.get("bairro"));
        jTextFieldPontoReferencia.setText(valores.get("pontoReferencia"));
        jTextFieldLatitude.setText(valores.get("latitude"));
        jTextFieldLongitude.setText(valores.get("longitude"));
        jFormattedTextFieldCep.setText(valores.get("cep"));
    }
    
    public void setCamposEditable(boolean b) {
        for (Component c: jPanelRepublica.getComponents()) {
            var type = c.getClass();
            if (type == JTextField.class || type == JFormattedTextField.class) {
                c.setEnabled(b);
            }
        }
        for (Component c: jPanelEndereco.getComponents()) {
            var type = c.getClass();
            if (type == JTextField.class || type == JFormattedTextField.class) {
                c.setEnabled(b);
            }
        }
        
        jTextAreaVantagens.setEnabled(b);
        jTextAreaCodigoEtica.setEnabled(b);
    }
    
    public void setBotaoCancelarVisible(boolean b) {
        jButtonCancelar.setVisible(b);
    }
    
    public void setBotaoExcluirVisible(boolean b) {
        jButtonExcluir.setVisible(b);
    }
    
    public void setBotaoConfirmarVisible(boolean b) {
        jButtonConfirmar.setVisible(b);
    }
    
    public void setBotaoConfirmarText(String text) {
        jButtonConfirmar.setText(text);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonConfirmar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelRepublica = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jFormattedTextFieldDataFundacao = new javax.swing.JFormattedTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaVantagens = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaCodigoEtica = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldDespesasMediasMorador = new javax.swing.JTextField();
        jTextFieldTotalVagas = new javax.swing.JTextField();
        jTextFieldVagasOcupadas = new javax.swing.JTextField();
        jPanelEndereco = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jTextFieldLogradouro = new javax.swing.JTextField();
        jTextFieldBairro = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldPontoReferencia = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jFormattedTextFieldCep = new javax.swing.JFormattedTextField();
        jLabel15 = new javax.swing.JLabel();
        jTextFieldLatitude = new javax.swing.JTextField();
        jTextFieldLongitude = new javax.swing.JTextField();

        setClosable(true);

        jButtonConfirmar.setText("Confirmar");

        jButtonExcluir.setText("Excluir");

        jButtonCancelar.setText("Cancelar");

        jPanelRepublica.setBorder(null);
        jPanelRepublica.setToolTipText("");

        jLabel1.setText("Nome:");

        try {
            jFormattedTextFieldDataFundacao.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel2.setText("Data de Fundação:");

        jLabel3.setText("Despesas Médias/Morador (R$):");

        jLabel4.setText("Total de Vagas:");

        jLabel6.setText("Vagas Ocup.:");

        jLabel7.setText("Vantagens:");

        jTextAreaVantagens.setColumns(20);
        jTextAreaVantagens.setRows(5);
        jScrollPane1.setViewportView(jTextAreaVantagens);

        jTextAreaCodigoEtica.setColumns(20);
        jTextAreaCodigoEtica.setRows(5);
        jScrollPane2.setViewportView(jTextAreaCodigoEtica);

        jLabel8.setText("Código de Ética:");

        javax.swing.GroupLayout jPanelRepublicaLayout = new javax.swing.GroupLayout(jPanelRepublica);
        jPanelRepublica.setLayout(jPanelRepublicaLayout);
        jPanelRepublicaLayout.setHorizontalGroup(
            jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRepublicaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanelRepublicaLayout.createSequentialGroup()
                            .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTextFieldNome)
                                .addGroup(jPanelRepublicaLayout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addGap(0, 0, Short.MAX_VALUE)))
                            .addGap(18, 18, 18)
                            .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel2)
                                .addComponent(jFormattedTextFieldDataFundacao, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanelRepublicaLayout.createSequentialGroup()
                            .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelRepublicaLayout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(0, 44, Short.MAX_VALUE))
                                .addComponent(jTextFieldDespesasMediasMorador))
                            .addGap(18, 18, 18)
                            .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4)
                                .addComponent(jTextFieldTotalVagas, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanelRepublicaLayout.createSequentialGroup()
                                    .addComponent(jLabel6)
                                    .addGap(106, 106, 106))
                                .addComponent(jTextFieldVagasOcupadas))))
                    .addGroup(jPanelRepublicaLayout.createSequentialGroup()
                        .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelRepublicaLayout.setVerticalGroup(
            jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRepublicaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jFormattedTextFieldDataFundacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelRepublicaLayout.createSequentialGroup()
                        .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldTotalVagas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldVagasOcupadas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanelRepublicaLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldDespesasMediasMorador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelRepublicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Dados da República", jPanelRepublica);

        jPanelEndereco.setBorder(null);

        jLabel9.setText("Logradouro:");

        jLabel10.setText("Bairro:");

        jLabel11.setText("Ponto de Referência:");

        jLabel12.setText("Latitude:");

        jLabel13.setText("Longitude");

        try {
            jFormattedTextFieldCep.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel15.setText("CEP:");

        javax.swing.GroupLayout jPanelEnderecoLayout = new javax.swing.GroupLayout(jPanelEndereco);
        jPanelEndereco.setLayout(jPanelEnderecoLayout);
        jPanelEnderecoLayout.setHorizontalGroup(
            jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldLogradouro)
                    .addComponent(jTextFieldBairro)
                    .addComponent(jTextFieldPontoReferencia)
                    .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                        .addComponent(jTextFieldLatitude, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addComponent(jTextFieldLongitude, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jFormattedTextFieldCep, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)))
                    .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelEnderecoLayout.setVerticalGroup(
            jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldPontoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelEnderecoLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldLatitude, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelEnderecoLayout.createSequentialGroup()
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(jLabel15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jFormattedTextFieldCep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldLongitude, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Endereço", jPanelEndereco);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonExcluir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonCancelar)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonConfirmar)))
                .addGap(12, 12, 12))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonConfirmar)
                        .addComponent(jButtonCancelar))
                    .addComponent(jButtonExcluir))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonConfirmar;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JFormattedTextField jFormattedTextFieldCep;
    private javax.swing.JFormattedTextField jFormattedTextFieldDataFundacao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanelEndereco;
    private javax.swing.JPanel jPanelRepublica;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextArea jTextAreaCodigoEtica;
    private javax.swing.JTextArea jTextAreaVantagens;
    private javax.swing.JTextField jTextFieldBairro;
    private javax.swing.JTextField jTextFieldDespesasMediasMorador;
    private javax.swing.JTextField jTextFieldLatitude;
    private javax.swing.JTextField jTextFieldLogradouro;
    private javax.swing.JTextField jTextFieldLongitude;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldPontoReferencia;
    private javax.swing.JTextField jTextFieldTotalVagas;
    private javax.swing.JTextField jTextFieldVagasOcupadas;
    // End of variables declaration//GEN-END:variables
}
