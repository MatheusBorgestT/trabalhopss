package com.mycompany.trabalho_final_pss.View.Login;

import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class LoginView extends javax.swing.JFrame {
    
    // Criei as Views como singletons pois não é necessário que a mesma tela
    // esteja aberta mais de uma vez na tela (até onde percebi). Caso algo do
    // tipo apareça, sugiro criarmos Views não-singleton como excessões da regra

    private static LoginView instancia;

    private LoginView() {
        initComponents();
    }

    public static LoginView getInstancia() {
        if (instancia == null) {
            instancia = new LoginView();
        }
        return instancia;
    }

    public void adicionarActionListenerBotaoNovoUsuario(ActionListener e) {
        botaoNovoUsuario.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoAcessar(ActionListener e) {
        botaoAcessar.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoCancelar(ActionListener e) {
        botaoCancelar.addActionListener(e);
    }
    
    public Map<String, String> getValoresDosCampos() {
        // Utilizei da classe map para transferir informações até a service
        // pois o intuito da View e da Presenter é simplesmente passar as
        // informações para o backend e trabalhar com a resposta, então achei
        // interessante manter as models o longe dessas classes se possível
        Map<String, String> dict = new HashMap<>();
        
        dict.put("usuario", campoUsuario.getText());
        dict.put("senha", String.valueOf(campoSenha.getPassword()));
        
        return dict;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        campoUsuario = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        campoSenha = new javax.swing.JPasswordField();
        botaoNovoUsuario = new javax.swing.JButton();
        botaoCancelar = new javax.swing.JButton();
        botaoAcessar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setText("Usuário:");

        jLabel2.setText("Senha:");

        botaoNovoUsuario.setText("Novo Usuário");

        botaoCancelar.setText("Cancelar");

        botaoAcessar.setText("Acessar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(campoUsuario)
                    .addComponent(campoSenha)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(botaoNovoUsuario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 94, Short.MAX_VALUE)
                        .addComponent(botaoAcessar)
                        .addGap(18, 18, 18)
                        .addComponent(botaoCancelar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(campoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(campoSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoNovoUsuario)
                    .addComponent(botaoCancelar)
                    .addComponent(botaoAcessar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoAcessar;
    private javax.swing.JButton botaoCancelar;
    private javax.swing.JButton botaoNovoUsuario;
    private javax.swing.JPasswordField campoSenha;
    private javax.swing.JTextField campoUsuario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
