package com.mycompany.trabalho_final_pss.View.ManterReclamacoesSugestoes;

import com.mycompany.trabalho_final_pss.Model.TipoReclamacaoSugestaoEnum;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.table.DefaultTableModel;

public class ManterReclamacaoSugestaoView extends javax.swing.JInternalFrame {

    private static ManterReclamacaoSugestaoView instancia;

    private ManterReclamacaoSugestaoView() {
        initComponents();
    }

    public static ManterReclamacaoSugestaoView getInstancia() {
        if (instancia == null) {
            instancia = new ManterReclamacaoSugestaoView();
        }
        return instancia;
    }

    public void setAbaInformacoesEnabled(boolean b) {
        jRadioButtonReclamacao.setEnabled(b);
        jRadioButtonSugestao.setEnabled(b);
        jTextAreaDescricao.setEnabled(b);
    }
    
    public void setAbaMoradoresEnabled(boolean b) {
        jTableMoradores.setEnabled(b);
        jTableEnvolvidos.setEnabled(b);
        jButtonAdicionarNaTabela.setEnabled(b);
        jButtonRemoverDaTabela.setEnabled(b);
    }

    public void setBotaoConfirmarVisible(boolean b) {
        jButtonConfirmar.setVisible(b);
    }

    public void preencherTabelaMoradores(Object[] valores) {
        var model = (DefaultTableModel) jTableMoradores.getModel();
        model.setRowCount(0);
        for (Object v : valores) {
            model.addRow(new Object[]{v});
        }
    }

    public void preencherTabelaEnvolvidos(Object[] valores) {
        var model = (DefaultTableModel) jTableEnvolvidos.getModel();
        model.setRowCount(0);
        for (Object v : valores) {
            model.addRow(new Object[]{v});
        }
    }

    public int getSelectedRowTabelaMoradores() {
        return jTableMoradores.getSelectedRow();
    }

    public int getSelectedRowTabelaEnvolvidos() {
        return jTableEnvolvidos.getSelectedRow();
    }

    public void setTextCampoData(String valor) {
        jTextFieldDataRealizada.setText(valor);
    }

    public Map<String, Object> getValoresDosCampos() {
        HashMap<String, Object> valores = new HashMap<>();

        int tipo;
        if (jRadioButtonReclamacao.isSelected() || jRadioButtonSugestao.isSelected()) {
            if (jRadioButtonReclamacao.isSelected()) {
                tipo = TipoReclamacaoSugestaoEnum.RECLAMACAO.getValorInt();
            } else {
                tipo = TipoReclamacaoSugestaoEnum.SUGESTAO.getValorInt();
            }
        } else {
            tipo = -1;
        }

        valores.put("tipo", String.valueOf(tipo));
        valores.put("descricao", jTextAreaDescricao.getText());

        return valores;
    }

    public void setValoresDosCampos(Map<String, Object> valores) {
        switch ((TipoReclamacaoSugestaoEnum) valores.get("tipo")) {
            case RECLAMACAO:
                jRadioButtonReclamacao.setSelected(true);
                break;
            case SUGESTAO:
                jRadioButtonSugestao.setSelected(true);
                break;
        }
        
        jTextFieldDataRealizada.setText((String) valores.get("dataCriacao"));
        jTextAreaDescricao.setText((String) valores.get("descricao"));
    }
    
    public void limparCampos() {
        buttonGroupTipo.clearSelection();
        jTextFieldDataRealizada.setText("");
        jTextAreaDescricao.setText("");
        preencherTabelaMoradores(new Object[]{});
        preencherTabelaEnvolvidos(new Object[]{});
    }

    //--------------------------------------------------------------------------
    // Botões
    //--------------------------------------------------------------------------
    public void adicionarActionListenerBotaoAdicionarNaTabela(ActionListener e) {
        jButtonAdicionarNaTabela.addActionListener(e);
    }

    public void adicionarActionListenerBotaoRemoverDaTabela(ActionListener e) {
        jButtonRemoverDaTabela.addActionListener(e);
    }

    public void adicionarActionListenerBotaoConfirmar(ActionListener e) {
        jButtonConfirmar.addActionListener(e);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupTipo = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaDescricao = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jRadioButtonReclamacao = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jRadioButtonSugestao = new javax.swing.JRadioButton();
        jTextFieldDataRealizada = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableMoradores = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableEnvolvidos = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jButtonRemoverDaTabela = new javax.swing.JButton();
        jButtonAdicionarNaTabela = new javax.swing.JButton();
        jButtonConfirmar = new javax.swing.JButton();

        setClosable(true);

        jTabbedPane1.setPreferredSize(new java.awt.Dimension(522, 285));

        jTextAreaDescricao.setColumns(20);
        jTextAreaDescricao.setRows(5);
        jScrollPane1.setViewportView(jTextAreaDescricao);

        jLabel3.setText("Descrição:");

        buttonGroupTipo.add(jRadioButtonReclamacao);
        jRadioButtonReclamacao.setText("Reclamação");

        jLabel1.setText("Tipo:");

        buttonGroupTipo.add(jRadioButtonSugestao);
        jRadioButtonSugestao.setText("Sugestão");

        jTextFieldDataRealizada.setEnabled(false);

        jLabel2.setText("Data Realizada:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(178, 178, 178))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jRadioButtonReclamacao)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButtonSugestao))
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(0, 178, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jTextFieldDataRealizada)
                                .addContainerGap())))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonReclamacao)
                    .addComponent(jRadioButtonSugestao)
                    .addComponent(jTextFieldDataRealizada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Informações", jPanel1);

        jLabel4.setText("Moradores:");

        jTableMoradores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTableMoradores);
        if (jTableMoradores.getColumnModel().getColumnCount() > 0) {
            jTableMoradores.getColumnModel().getColumn(0).setResizable(false);
        }

        jTableEnvolvidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTableEnvolvidos);
        if (jTableEnvolvidos.getColumnModel().getColumnCount() > 0) {
            jTableEnvolvidos.getColumnModel().getColumn(0).setResizable(false);
        }

        jLabel5.setText("Envolvidos:");

        jButtonRemoverDaTabela.setText("<");

        jButtonAdicionarNaTabela.setText(">");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonRemoverDaTabela, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonAdicionarNaTabela, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addComponent(jButtonAdicionarNaTabela)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonRemoverDaTabela)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Moradores Envolvidos", jPanel2);

        jButtonConfirmar.setText("Confirmar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonConfirmar)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonConfirmar)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupTipo;
    private javax.swing.JButton jButtonAdicionarNaTabela;
    private javax.swing.JButton jButtonConfirmar;
    private javax.swing.JButton jButtonRemoverDaTabela;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JRadioButton jRadioButtonReclamacao;
    private javax.swing.JRadioButton jRadioButtonSugestao;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableEnvolvidos;
    private javax.swing.JTable jTableMoradores;
    private javax.swing.JTextArea jTextAreaDescricao;
    private javax.swing.JTextField jTextFieldDataRealizada;
    // End of variables declaration//GEN-END:variables
}
