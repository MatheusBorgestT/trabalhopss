package com.mycompany.trabalho_final_pss.View.Tarefa;

import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.table.DefaultTableModel;

public class ListagemTarefasView extends javax.swing.JInternalFrame {

    private static ListagemTarefasView instancia;

    private ListagemTarefasView() {
        initComponents();
    }

    public static ListagemTarefasView getInstancia() {
        if (instancia == null) {
            instancia = new ListagemTarefasView();
        }
        return instancia;
    }
    
    public void preencherTabela(List<Object[]> valores) {
        var model = (DefaultTableModel) jTableTarefas.getModel();
        model.setRowCount(0);
        for (Object[] v: valores) {
            model.addRow(v);
        }
    }
    
    public int getSelectedIndexFiltro() {
        return jComboBoxFiltro.getSelectedIndex();
    }
    
    public int getSelectedRowTabela() {
        return jTableTarefas.getSelectedRow();
    }
    
    public String getTextCampoBusca() {
        return campoBuscaTarefa.getText();
    }
    
    public void setSelectedIndexFiltro(int index) {
        jComboBoxFiltro.setSelectedIndex(index);
    }
    
    public void setTextCampoBusca(String valor) {
        campoBuscaTarefa.setText(valor);
    }
    
    // -------------------------------------------------------------------------
    // Visibilidade de botões
    // -------------------------------------------------------------------------
    public void setBotaoExcluirVisible(boolean b) {
        botaoExcluirTarefa.setVisible(b);
    }
    
    // -------------------------------------------------------------------------
    // Acionando Botões
    // -------------------------------------------------------------------------
    public void adicionarActionListenerBotaoBuscar(ActionListener e) {
        botaoBuscarTarefa.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoCadastrarTarefa(ActionListener e) {
        botaoCadastrarNovaTarefa.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoEditarTarefa(ActionListener e) {
        botaoEditarTarefa.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoExcluirTarefa(ActionListener e) {
        botaoExcluirTarefa.addActionListener(e);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBoxFiltro = new javax.swing.JComboBox<>();
        campoBuscaTarefa = new javax.swing.JTextField();
        botaoBuscarTarefa = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableTarefas = new javax.swing.JTable();
        botaoCadastrarNovaTarefa = new javax.swing.JButton();
        botaoExcluirTarefa = new javax.swing.JButton();
        botaoEditarTarefa = new javax.swing.JButton();

        setClosable(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Manter Tarefa");

        jComboBoxFiltro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pessoa", "Descrição" }));

        botaoBuscarTarefa.setText("Buscar");

        jTableTarefas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Tarefa", "Responsáveis", "Data Agendamento", "Data Término", "Realizada"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTableTarefas);

        botaoCadastrarNovaTarefa.setText("Cadastrar nova tarefa");

        botaoExcluirTarefa.setText("Excluir");

        botaoEditarTarefa.setText("Editar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(botaoCadastrarNovaTarefa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botaoExcluirTarefa)
                        .addGap(18, 18, 18)
                        .addComponent(botaoEditarTarefa))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jComboBoxFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(campoBuscaTarefa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botaoBuscarTarefa)))
                .addGap(14, 14, 14))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(campoBuscaTarefa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botaoBuscarTarefa))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoCadastrarNovaTarefa)
                    .addComponent(botaoExcluirTarefa)
                    .addComponent(botaoEditarTarefa))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoBuscarTarefa;
    private javax.swing.JButton botaoCadastrarNovaTarefa;
    private javax.swing.JButton botaoEditarTarefa;
    private javax.swing.JButton botaoExcluirTarefa;
    private javax.swing.JTextField campoBuscaTarefa;
    private javax.swing.JComboBox<String> jComboBoxFiltro;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableTarefas;
    // End of variables declaration//GEN-END:variables
}
