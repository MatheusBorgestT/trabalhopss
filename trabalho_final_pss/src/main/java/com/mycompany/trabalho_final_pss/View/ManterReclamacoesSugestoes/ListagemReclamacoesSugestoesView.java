
package com.mycompany.trabalho_final_pss.View.ManterReclamacoesSugestoes;

import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.table.DefaultTableModel;

public class ListagemReclamacoesSugestoesView extends javax.swing.JInternalFrame {
    
    private static ListagemReclamacoesSugestoesView instancia;

    private ListagemReclamacoesSugestoesView() {
        initComponents();
    }
    
    public static ListagemReclamacoesSugestoesView getInstancia() {
        if (instancia == null) {
            instancia = new ListagemReclamacoesSugestoesView();
        }
        return instancia;
    }
    
    public void preencherTabela(List<Object[]> valores) {
        var model = (DefaultTableModel) jTableReclamacoesSugestoes.getModel();
        model.setRowCount(0);
        for (Object[] v: valores) {
            model.addRow(v);
        }
    }
    
    public int getSelectedIndexFiltro() {
        return jComboBoxFiltro.getSelectedIndex();
    }
    
    public int getSelectedRowTabela() {
        return jTableReclamacoesSugestoes.getSelectedRow();
    }
    
    public String getTextCampoBusca() {
        return jTextFieldBusca.getText();
    }
    
    public void setSelectedIndexFiltro(int index) {
        jComboBoxFiltro.setSelectedIndex(index);
    }
    
    public void setTextCampoBusca(String valor) {
        jTextFieldBusca.setText(valor);
    }
    
    // -------------------------------------------------------------------------
    // Visibilidade de botões
    // -------------------------------------------------------------------------
    public void setBotaoExcluirVisible(boolean b) {
        jButtonExcluir.setVisible(b);
    }
    
    // -------------------------------------------------------------------------
    // Acionando Botões
    // -------------------------------------------------------------------------
    public void adicionarActionListenerBotaoBuscar(ActionListener e) {
        jButtonBuscar.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoCadastrarNova(ActionListener e) {
        jButtonCadastrarNova.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoExcluir(ActionListener e) {
        jButtonExcluir.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoVerInfo(ActionListener e) {
        jButtonVerInfo.addActionListener(e);
    }
    
    public void adicionarActionListenerBotaoEditar(ActionListener e) {
        jButtonEditar.addActionListener(e);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBoxFiltro = new javax.swing.JComboBox<>();
        jTextFieldBusca = new javax.swing.JTextField();
        jButtonBuscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableReclamacoesSugestoes = new javax.swing.JTable();
        jButtonEditar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jButtonVerInfo = new javax.swing.JButton();
        jButtonCadastrarNova = new javax.swing.JButton();

        setClosable(true);

        jComboBoxFiltro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pessoa", "Descrição" }));
        jComboBoxFiltro.setSelectedIndex(-1);

        jButtonBuscar.setText("Buscar");

        jTableReclamacoesSugestoes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Data", "Tipo", "Autor", "Descrição", "Envolvidos", "Finalizada"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTableReclamacoesSugestoes);

        jButtonEditar.setText("Editar");

        jButtonExcluir.setText("Excluir");

        jButtonVerInfo.setText("Ver Informações");

        jButtonCadastrarNova.setText("Cadastrar Nova Sugestão / Reclamação");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBoxFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldBusca)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonBuscar))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButtonCadastrarNova)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 133, Short.MAX_VALUE)
                        .addComponent(jButtonExcluir)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonVerInfo)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonEditar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonBuscar))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonEditar)
                    .addComponent(jButtonExcluir)
                    .addComponent(jButtonVerInfo)
                    .addComponent(jButtonCadastrarNova))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonBuscar;
    private javax.swing.JButton jButtonCadastrarNova;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonVerInfo;
    private javax.swing.JComboBox<String> jComboBoxFiltro;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableReclamacoesSugestoes;
    private javax.swing.JTextField jTextFieldBusca;
    // End of variables declaration//GEN-END:variables
}
