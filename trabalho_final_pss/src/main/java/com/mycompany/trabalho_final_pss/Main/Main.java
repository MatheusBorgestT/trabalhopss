
package com.mycompany.trabalho_final_pss.Main;

import com.mycompany.trabalho_final_pss.Presenter.Login.LoginPresenter;

public class Main {
    
    public static void main(String args[]) {
        LoginPresenter login = LoginPresenter.getInstancia();
        login.abrirView(true);
    }
    
}
