
package com.mycompany.trabalho_final_pss.Presenter.MenuPrincipal;

import com.mycompany.trabalho_final_pss.Presenter.ManterRepublica.CreateRepublicaState;
import com.mycompany.trabalho_final_pss.Presenter.ManterRepublica.ManterRepublicaPresenter;

public class SemTetoState extends MenuPrincipalState {

    public SemTetoState(MenuPrincipalPresenter presenter) {
        super(presenter);
    }

    @Override
    public void ajustarView() {
        presenter.tornarCriarRepublicaVisivel(true);
        presenter.tornarBuscarVagasVisivel(true);
        presenter.tornarEncerrarSessaoVisivel(true);
    }

    @Override
    public void opcaoBuscarVagasPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoConsultarLancamentoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoConsultarResultado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoConvidarPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoEstornosPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoMeuPerfilPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoMinhaRepublicaPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoMoradorRepublicaPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoNotificacoesPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoReceitasDespesasPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoReclamacoesPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoConclusaoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoPagamentoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoSolucaoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoTarefasGeraisPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoCriarRepublicaPressionado() {
        var manterRepublicaPresenter = ManterRepublicaPresenter.getInstancia();
        manterRepublicaPresenter.setState(new CreateRepublicaState());
        manterRepublicaPresenter.abrirView(true);
    }
    
}
