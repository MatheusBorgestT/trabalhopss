package com.mycompany.trabalho_final_pss.Presenter.ManterReclamacoesSugestoes;

import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.ReclamacaoSugestao;
import com.mycompany.trabalho_final_pss.Model.Republica;
import com.mycompany.trabalho_final_pss.Presenter.AbstractPresenter;
import com.mycompany.trabalho_final_pss.Presenter.PresentersBridge;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import com.mycompany.trabalho_final_pss.View.ManterReclamacoesSugestoes.ManterReclamacaoSugestaoView;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ManterReclamacaoSugestaoPresenter extends AbstractPresenter {

    private static ManterReclamacaoSugestaoPresenter instancia;

    private ManterReclamacaoSugestaoView view;

    private ManterReclamacaoSugestaoState state;

    private Republica republica;

    private List<Morador> moradoresNaTabela;

    private List<Morador> envolvidosNaTabela;

    private ManterReclamacaoSugestaoPresenter() {
        initView();
    }

    public static ManterReclamacaoSugestaoPresenter getInstancia() {
        if (instancia == null) {
            instancia = new ManterReclamacaoSugestaoPresenter();
        }
        return instancia;
    }

    @Override
    protected void initView() {
        view = ManterReclamacaoSugestaoView.getInstancia();

        view.adicionarActionListenerBotaoAdicionarNaTabela((ActionListener) e -> {
            botaoAdicionarNaTabelaPressionado();
        });

        view.adicionarActionListenerBotaoRemoverDaTabela((ActionListener) e -> {
            botaoRemoverDaTabelaPressionado();
        });

        view.adicionarActionListenerBotaoConfirmar((ActionListener) e -> {
            botaoConfirmarPressionado();
        });
    }

    @Override
    public void abrirView(boolean b) {
        fecharView(); // caso já esteja aberta com para outra funcionalidade

        limparCampos();
        setRepublica();

        PresentersBridge.adicionarViewAoMenuPrincipal(view);
        view.setVisible(b);
    }

    public void fecharView() {
        view.setVisible(false);
    }

    public void setState(ManterReclamacaoSugestaoState state) {
        this.state = state;
    }

    public void setRepublica() {
        try {
            republica = UsuarioService.getRepublicaOndeResideUsuarioLogado();
            moradoresNaTabela = republica.getMoradores();
            envolvidosNaTabela = new ArrayList<>();
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro ao recuperar dados da república");
            Logger.getLogger(ManterReclamacaoSugestaoPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void preencherViewComReclamacaoSugestao(ReclamacaoSugestao rs) {
        try {
            HashMap<String, Object> valores = new HashMap<>();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

            valores.put("tipo", rs.getTipo());
            valores.put("dataCriacao", dtf.format(rs.getDataCriacao()));
            valores.put("descricao", rs.getDescricao());

            view.setValoresDosCampos(valores);

            moradoresNaTabela = republica.getMoradores();
            envolvidosNaTabela = new ArrayList<>();
            for (Morador envolvido : rs.getMoradoresEnvolvidos()) {
                for (Morador morador : moradoresNaTabela) {
                    if (envolvido.getIdPessoa() == morador.getIdPessoa()) {
                        moradoresNaTabela.remove(morador);
                        break;
                    }
                }
                envolvidosNaTabela.add(envolvido);
            }

            preencherTabelas();
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro no banco de dados");
            Logger.getLogger(ManterReclamacaoSugestaoPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void preencherTabelas() {
        try {
            List<String> nomes = new ArrayList<>();
            for (Morador m : moradoresNaTabela) {
                nomes.add(m.getPessoa().getNome());
            }
            view.preencherTabelaMoradores(nomes.toArray());

            nomes = new ArrayList<>();
            for (Morador m : envolvidosNaTabela) {
                nomes.add(m.getPessoa().getNome());
            }
            view.preencherTabelaEnvolvidos(nomes.toArray());
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro ao preencher as tabelas");
            Logger.getLogger(ManterReclamacaoSugestaoPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Map<String, Object> getValorDosCampos() throws SQLException {
        var valores = view.getValoresDosCampos();
        valores.put("envolvidos", envolvidosNaTabela);
        valores.put("idAutor", UsuarioService.getMoradorLogado().getIdPessoa());
        valores.put("idRepublica", UsuarioService.getRepublicaOndeResideUsuarioLogado().getIdRepublica());
        valores.put("dataCriacao", LocalDate.now());
        return valores;
    }

    public void setCampoDataDiaDeHoje() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        view.setTextCampoData(dtf.format(LocalDate.now()));
    }

    //--------------------------------------------------------------------------
    // Ponte com a View
    //--------------------------------------------------------------------------
    public void setAbaInformacoesEnabled(boolean b) {
        view.setAbaInformacoesEnabled(b);
    }

    public void setAbaMoradoresEnabled(boolean b) {
        view.setAbaMoradoresEnabled(b);
    }

    public void setBotaoConfirmarVisible(boolean b) {
        view.setBotaoConfirmarVisible(b);
    }

    public void limparCampos() {
        view.limparCampos();
    }

    //--------------------------------------------------------------------------
    // Botões
    //--------------------------------------------------------------------------
    public void botaoAdicionarNaTabelaPressionado() {
        int row = view.getSelectedRowTabelaMoradores();
        if (row >= 0) {
            var moradorSelecionado = moradoresNaTabela.get(row);
            moradoresNaTabela.remove(row);
            envolvidosNaTabela.add(moradorSelecionado);
            preencherTabelas();
        } else {
            PopUpUtil.mostrarPopUpErro(view, "Favor selecionar uma linha na tabela de moradores");
        }
    }

    public void botaoRemoverDaTabelaPressionado() {
        int row = view.getSelectedRowTabelaEnvolvidos();
        if (row >= 0) {
            var moradorSelecionado = envolvidosNaTabela.get(row);
            envolvidosNaTabela.remove(row);
            moradoresNaTabela.add(moradorSelecionado);
            preencherTabelas();
        } else {
            PopUpUtil.mostrarPopUpErro(view, "Favor selecionar uma linha na tabela de moradores");
        }
    }

    public void botaoConfirmarPressionado() {
        state.botaoConfirmarPressionado();
    }

}
