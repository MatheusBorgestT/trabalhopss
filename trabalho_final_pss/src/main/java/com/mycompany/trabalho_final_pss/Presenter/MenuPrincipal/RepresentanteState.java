
package com.mycompany.trabalho_final_pss.Presenter.MenuPrincipal;

import com.mycompany.trabalho_final_pss.Model.StatusMoradorEnum;
import com.mycompany.trabalho_final_pss.Presenter.ManterMorador.ManterMoradorPresenter;
import com.mycompany.trabalho_final_pss.Presenter.ManterReclamacoesSugestoes.ListagemReclamacoesSugestoesPresenter;
import com.mycompany.trabalho_final_pss.Presenter.ManterRepublica.ManterRepublicaPresenter;
import com.mycompany.trabalho_final_pss.Presenter.ManterRepublica.ReadRepublicaState;
import static com.mycompany.trabalho_final_pss.Presenter.MenuPrincipal.MenuPrincipalState.presenter;
import com.mycompany.trabalho_final_pss.Presenter.Tarefa.ListagemTarefasPresenter;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import java.sql.SQLException;

public class RepresentanteState extends MenuPrincipalState {

    public RepresentanteState(MenuPrincipalPresenter presenter) {
        super(presenter);
    }

    @Override
    public void ajustarView() {
        presenter.tornarMeuPerfilVisivel(true);
        presenter.tornarNotificacoesVisivel(true);
        presenter.tornarRegistrarConclusaoVisivel(true);
        presenter.tornarConsultarResultadoVisivel(true);
        presenter.tornarReclamacoesVisivel(true);
        presenter.tornarReceitasDespesasVisivel(true);
        presenter.tornarRegistrarPagamentoVisivel(true);
        presenter.tornarMinhaRepublicaVisivel(true);
        presenter.tornarEstornosVisivel(true);
        presenter.tornarMoradoresRepublicaVisivel(true);
        presenter.tornarConsultarLancamentoVisivel(true);
        presenter.tornarTarefasGeraisVisivel(true);
        presenter.tornarConvidarVisivel(true);
        presenter.tornarSolucaoVisivel(true);
        presenter.tornarEncerrarSessaoVisivel(true);
    }

    @Override
    public void opcaoBuscarVagasPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoConsultarLancamentoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoConsultarResultado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoConvidarPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoEstornosPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoMeuPerfilPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoMinhaRepublicaPressionado() {
        try {
            var manterRepublicaPresenter = ManterRepublicaPresenter.getInstancia();
            
            var republica = UsuarioService.getRepublicaOndeResideUsuarioLogado();
            manterRepublicaPresenter.setState(new ReadRepublicaState(republica, StatusMoradorEnum.REPRESENTANTE));
            
            manterRepublicaPresenter.abrirView(true);
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(null, "Erro ao visualizar república:\n" + ex.toString());
        }
    }

    @Override
    public void opcaoMoradorRepublicaPressionado() {
        var manterMoradorPresenter = ManterMoradorPresenter.getInstancia();
        manterMoradorPresenter.abrirView(true);
    }

    @Override
    public void opcaoNotificacoesPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoReceitasDespesasPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoReclamacoesPressionado() {
        var presenter = ListagemReclamacoesSugestoesPresenter.getInstancia();
        presenter.abrirView(true);
    }

    @Override
    public void opcaoConclusaoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoPagamentoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoSolucaoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void opcaoTarefasGeraisPressionado() {
        var presenter = ListagemTarefasPresenter.getInstancia();
        presenter.abrirView(true);
        
        /*var manterTarefaPresenter = ListagemTarefasPresenter.getInstancia();
        
        var manterTarefaView = manterTarefaPresenter.getView();
        view.addToDesktop(manterTarefaView);
        manterTarefaView.setVisible(true);*/
    }

    @Override
    public void opcaoCriarRepublicaPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
