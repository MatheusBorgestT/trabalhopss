
package com.mycompany.trabalho_final_pss.Presenter.MenuPrincipal;

import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuPrincipalPresenterBridge {
    
    public static void atualizarUsuarioLogadoEMenuPrincipal() {
        try {
            UsuarioService.atualizarUsuarioInfo();
            
            var presenter = MenuPrincipalPresenter.getInstancia();
            presenter.preencherRodape();
            presenter.adaptarMenuParaStatusUsuario();
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(null, "Erro ao atualizar o menu principal");
            Logger.getLogger(MenuPrincipalPresenterBridge.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
