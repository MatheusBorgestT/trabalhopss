package com.mycompany.trabalho_final_pss.Presenter.ManterRepublica;

import com.mycompany.trabalho_final_pss.Presenter.AbstractPresenter;
import com.mycompany.trabalho_final_pss.Presenter.PresentersBridge;
import com.mycompany.trabalho_final_pss.View.ManterRepublica.ManterRepublicaView;
import java.awt.event.ActionEvent;
import java.util.Map;

public class ManterRepublicaPresenter extends AbstractPresenter {

    private static ManterRepublicaPresenter instancia;

    private static ManterRepublicaView view;

    private ManterRepublicaState state;

    private ManterRepublicaPresenter() {
        initView();
    }

    public static ManterRepublicaPresenter getInstancia() {
        if (instancia == null) {
            instancia = new ManterRepublicaPresenter();
        }
        return instancia;
    }

    @Override
    protected void initView() {
        view = ManterRepublicaView.getInstancia();

        view.adicionarActionListenerBotaoConfirmar((ActionEvent e) -> {
            botaoConfirmarPressionado();
        });

        view.adicionarActionListenerBotaoCancelar((ActionEvent e) -> {
            botaoCancelarPressionado();
        });

        view.adicionarActionListenerBotaoExcluir((ActionEvent e) -> {
            botaoExcluirPressionado();
        });
    }

    @Override
    public void abrirView(boolean b) {
        PresentersBridge.adicionarViewAoMenuPrincipal(view);
        view.setVisible(b);
    }

    public ManterRepublicaView getView() {
        return view;
    }

    public void setState(ManterRepublicaState state) {
        this.state = state;
    }
    
    public void setValoresDosCampos(Map<String, String> valores) {
        view.setValoresDosCampos(valores);
    }
    
    // -------------------------------------------------------------------------
    // Manipuladores da view
    // -------------------------------------------------------------------------
    public void setBotaoCancelarVisible(boolean b) {
        view.setBotaoCancelarVisible(b);
    }

    public void setBotaoExcluirVisible(boolean b) {
        view.setBotaoExcluirVisible(b);
    }
    
    public void setBotaoConfirmarVisible(boolean b) {
        view.setBotaoConfirmarVisible(b);
    }

    public void setBotaoConfirmarText(String text) {
        view.setBotaoConfirmarText(text);
    }

    public Map<String, String> getValoresNaView() {
        return view.getValoresDosCampos();
    }

    public void setCamposEditableView(boolean b) {
        view.setCamposEditable(b);
    }

    public void setValoresDosCamposView(Map<String, String> valores) {
        view.setValoresDosCampos(valores);
    }

    // -------------------------------------------------------------------------
    // Botões
    // -------------------------------------------------------------------------
    private void botaoConfirmarPressionado() {
        state.botaoConfirmarPressionado();;
    }

    private void botaoCancelarPressionado() {
        state.botaoCancelarPressionado();
    }

    private void botaoExcluirPressionado() {
        state.botaoExcluirPressionado();
    }

}
