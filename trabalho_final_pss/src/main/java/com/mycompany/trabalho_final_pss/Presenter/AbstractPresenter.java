package com.mycompany.trabalho_final_pss.Presenter;

public abstract class AbstractPresenter {

    protected abstract void initView();

    public abstract void abrirView(boolean b);

}
