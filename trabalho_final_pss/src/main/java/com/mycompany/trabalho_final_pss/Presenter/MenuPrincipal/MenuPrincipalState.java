package com.mycompany.trabalho_final_pss.Presenter.MenuPrincipal;

import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;

public abstract class MenuPrincipalState {

    protected static MenuPrincipalPresenter presenter;

    protected MenuPrincipalState(MenuPrincipalPresenter presenter) {
        this.presenter = presenter;
        this.presenter.setTodosMenusItensVisiveis(false);
        ajustarView();
    }

    public abstract void ajustarView();

    public abstract void opcaoBuscarVagasPressionado();

    public abstract void opcaoConsultarLancamentoPressionado();

    public abstract void opcaoConsultarResultado();

    public abstract void opcaoConvidarPressionado();

    public abstract void opcaoEstornosPressionado();

    public abstract void opcaoMeuPerfilPressionado();

    public abstract void opcaoMinhaRepublicaPressionado();

    public abstract void opcaoMoradorRepublicaPressionado();

    public abstract void opcaoNotificacoesPressionado();

    public abstract void opcaoReceitasDespesasPressionado();

    public abstract void opcaoReclamacoesPressionado();

    public abstract void opcaoConclusaoPressionado();

    public abstract void opcaoPagamentoPressionado();

    public abstract void opcaoSolucaoPressionado();

    public abstract void opcaoTarefasGeraisPressionado();

    public abstract void opcaoCriarRepublicaPressionado();

    public void opcaoEncerrarSessaoPressionado() {
        var resultado = PopUpUtil.mostrarPopUpConfirmacao(
                null,
                "Realmente deseja sair da sessão?"
        );

        if (resultado) {
            System.exit(0);
        }
    }

}
