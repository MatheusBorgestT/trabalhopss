
package com.mycompany.trabalho_final_pss.Presenter.ManterReclamacoesSugestoes;

import com.mycompany.trabalho_final_pss.Model.ReclamacaoSugestao;
import com.mycompany.trabalho_final_pss.Presenter.AbstractPresenter;
import com.mycompany.trabalho_final_pss.Presenter.PresentersBridge;
import com.mycompany.trabalho_final_pss.Service.ReclamacaoSugestaoService;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import com.mycompany.trabalho_final_pss.View.ManterReclamacoesSugestoes.ListagemReclamacoesSugestoesView;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListagemReclamacoesSugestoesPresenter extends AbstractPresenter {
    
    private static ListagemReclamacoesSugestoesPresenter instancia;
    
    private static ListagemReclamacoesSugestoesView view;
    
    private List<ReclamacaoSugestao> reclamacoesSugestoesNaTabela;
    
    private ListagemReclamacoesSugestoesPresenter() {
        initView();
    }
    
    public static ListagemReclamacoesSugestoesPresenter getInstancia() {
        if (instancia == null) {
            instancia = new ListagemReclamacoesSugestoesPresenter();
        }
        return instancia;
    }

    @Override
    protected void initView() {
        view = ListagemReclamacoesSugestoesView.getInstancia();
        
        view.adicionarActionListenerBotaoBuscar((ActionListener) e -> {
            botaoBuscarPressionado();
        });
        
        view.adicionarActionListenerBotaoCadastrarNova((ActionListener) e -> {
            botaoCadastrarNovaPressionado();
        });
        
        view.adicionarActionListenerBotaoExcluir((ActionListener) e -> {
            botaoExcluirPressionado();
        });
        
        view.adicionarActionListenerBotaoVerInfo((ActionListener) e -> {
            botaoVerInfoPressionado();
        });
        
        view.adicionarActionListenerBotaoEditar((ActionListener) e -> {
            botaoEditarPressionado();
        });
    }

    @Override
    public void abrirView(boolean b) {
        adaptarViewAoUsuario();
        preencherTabelaSemFiltro();
        PresentersBridge.adicionarViewAoMenuPrincipal(view);
        view.setVisible(b);
    }
    
    public void preencherTabelaSemFiltro() {
        try {
            int idRepublica = UsuarioService.getRepublicaOndeResideUsuarioLogado().getIdRepublica();
            reclamacoesSugestoesNaTabela = ReclamacaoSugestaoService.getReclamacoesSugestoesByIdRepublica(idRepublica);
            
            preencherTabela();
            view.setSelectedIndexFiltro(-1);
            view.setTextCampoBusca("");
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro ao conseguir informações da república\n" + ex.toString());
            Logger.getLogger(ListagemReclamacoesSugestoesPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void preencherTabela() throws SQLException {
        ArrayList<Object[]> listaValores = new ArrayList<>();
        for (ReclamacaoSugestao rs: reclamacoesSugestoesNaTabela) {
            listaValores.add(rs.toObjectArray());
        }
        view.preencherTabela(listaValores);
    }
    
    private void adaptarViewAoUsuario() {
        try {
            var statusMoradorLogado = UsuarioService.getMoradorLogado().getStatusMorador();
            switch (statusMoradorLogado) {
                case REPRESENTANTE:
                    view.setBotaoExcluirVisible(true);
                    break;
                case MORADOR:
                    view.setBotaoExcluirVisible(false);
                    break;
                default:
                    PopUpUtil.mostrarPopUpErro(view, "Status de morador inválido");
                    abrirView(false);
                    break;
            }
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro ao buscar informções de usuário logado");
            Logger.getLogger(ListagemReclamacoesSugestoesPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private ReclamacaoSugestao getReclamacaoSugestaoSelecionada() {
        int selectedRow = view.getSelectedRowTabela();
        if (selectedRow >= 0) {
            return reclamacoesSugestoesNaTabela.get(selectedRow);
        } else {
            PopUpUtil.mostrarPopUpErro(view, "Favor selecionar uma linha da tabela");
            return null;
        }
    }
    
    // -------------------------------------------------------------------------
    // Botões
    // -------------------------------------------------------------------------
    private void botaoBuscarPressionado() {
        int index = view.getSelectedIndexFiltro();
        String busca = view.getTextCampoBusca();
        if (busca.isBlank()) {
            preencherTabelaSemFiltro();
        } else {
            try {
                switch (index) {
                    case -1:
                        PopUpUtil.mostrarPopUpErro(view, "Favor escolher um filtro");
                        break;
                    case 0:
                        reclamacoesSugestoesNaTabela = ReclamacaoSugestaoService.getReclamacoesSugestoesByPessoa(
                                UsuarioService.getRepublicaOndeResideUsuarioLogado().getIdRepublica(), 
                                view.getTextCampoBusca()
                        );
                        preencherTabela();
                        break;
                    case 1:
                        reclamacoesSugestoesNaTabela = ReclamacaoSugestaoService.getReclamacoesSugestoesByDescricao(
                                UsuarioService.getRepublicaOndeResideUsuarioLogado().getIdRepublica(), 
                                view.getTextCampoBusca()
                        );
                        preencherTabela();
                        break;
                    default:
                        PopUpUtil.mostrarPopUpErro(view, "Indicie de filtro inválido");
                        break;
                }
            } catch (SQLException ex) {
                PopUpUtil.mostrarPopUpErro(view, "Erro no banco");
                Logger.getLogger(ListagemReclamacoesSugestoesPresenter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void botaoCadastrarNovaPressionado() {
        var manterReclamacaoSugestaoPresenter = ManterReclamacaoSugestaoPresenter.getInstancia();
        manterReclamacaoSugestaoPresenter.abrirView(true);
        manterReclamacaoSugestaoPresenter.setState(new CreateReclamacaoSugestaoState(manterReclamacaoSugestaoPresenter));
    }
    
    private void botaoExcluirPressionado() {
        var rs = getReclamacaoSugestaoSelecionada();
        if (rs != null) {
            if (PopUpUtil.mostrarPopUpConfirmacao(view, "Realmente deseja realizar a exclusão?")) {
                try {
                    ReclamacaoSugestaoService.excluirReclamacaoSugestao(rs);
                    reclamacoesSugestoesNaTabela.remove(rs);
                    preencherTabela();
                } catch (SQLException ex) {
                    PopUpUtil.mostrarPopUpErro(view, "Erro no banco ao excluir a " + rs.getTipo().getValorString().toLowerCase());
                    Logger.getLogger(ListagemReclamacoesSugestoesPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
            
    private void botaoVerInfoPressionado() {
        var rs = getReclamacaoSugestaoSelecionada();
        if (rs != null) {
            var manterReclamacaoSugestaoPresenter = ManterReclamacaoSugestaoPresenter.getInstancia();
            manterReclamacaoSugestaoPresenter.abrirView(true);
            manterReclamacaoSugestaoPresenter.setState(new ReadReclamacaoSugestaoState(manterReclamacaoSugestaoPresenter, rs));
        }
    }
    
    private void botaoEditarPressionado() {
        var rs = getReclamacaoSugestaoSelecionada();
        if (rs != null) {
            var manterReclamacaoSugestaoPresenter = ManterReclamacaoSugestaoPresenter.getInstancia();
            manterReclamacaoSugestaoPresenter.abrirView(true);
            manterReclamacaoSugestaoPresenter.setState(new UpdateReclamacaoSugestaoState(manterReclamacaoSugestaoPresenter, rs));
        }
    }
    
}
