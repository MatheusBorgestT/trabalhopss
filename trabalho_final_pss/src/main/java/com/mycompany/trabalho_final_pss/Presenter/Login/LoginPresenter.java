package com.mycompany.trabalho_final_pss.Presenter.Login;

import com.mycompany.trabalho_final_pss.Presenter.AbstractPresenter;
import com.mycompany.trabalho_final_pss.Presenter.MenuPrincipal.MenuPrincipalPresenter;
import com.mycompany.trabalho_final_pss.Service.LoginService;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.View.Login.LoginView;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.Map;
import javax.swing.JOptionPane;

public class LoginPresenter extends AbstractPresenter {

    // Como a Presenter é acomplada à uma View, e por regra as Views vão ser
    // singletons, achei lógico que as Presenters também sejam singletons
    private static LoginPresenter instancia;

    private static LoginView view;

    private static LoginService service;

    private LoginPresenter() {
        initView();
        service = new LoginService();
    }

    public static LoginPresenter getInstancia() {
        if (instancia == null) {
            instancia = new LoginPresenter();
        }
        return instancia;
    }

    @Override
    protected void initView() {
        view = LoginView.getInstancia();

        view.adicionarActionListenerBotaoNovoUsuario((ActionEvent e) -> {
            botaoNovoUsuarioPressionado();
        });

        view.adicionarActionListenerBotaoAcessar((ActionEvent e) -> {
            botaoAcessarPressionado();
        });

        view.adicionarActionListenerBotaoCancelar((ActionEvent e) -> {
            botaoCancelarPressionado();
        });
    }

    @Override
    public void abrirView(boolean b) {
        view.setVisible(b);
    }

    private void mostrarMensagemErroNoLogin(String mensagem) {
        JOptionPane.showMessageDialog(
                view,
                "Não foi possível realizar o acesso:\n" + mensagem,
                "Erro",
                JOptionPane.WARNING_MESSAGE
        );
    }
    
    public LoginView getView() {
        return view;
    }

    // -------------------------------------------------------------------------
    // Botões
    // -------------------------------------------------------------------------
    private void botaoNovoUsuarioPressionado() {
        NovoUsuarioPresenter.getInstancia().abrirView(true);
    }

    private void botaoAcessarPressionado() {
        Map<String, String> valoresDosCampos = view.getValoresDosCampos();
        String mensagem;
        // Como o Clayton disse, é aqui que trabalhamos o tratamento de
        // excessões
        try {
            mensagem = service.validarLogin(valoresDosCampos);
        } catch (SQLException ex) {
            mensagem = ex.toString();
        }

        // Fiz com que as funções em níveis mais abaixo retornassem uma string
        // com a mensagem de erro, caso ocorresse. Caso não, retorna uma string
        // nula
        // IMPORTANTE: String nula é diferente de string vazia. Não vacile!
        if (mensagem == null) {
            try {
                UsuarioService.setUsuarioLogado(service.getUsuarioLogado(valoresDosCampos));
            } catch (SQLException ex) {
                mensagem = ex.toString();
            }

            if (mensagem == null) {
                MenuPrincipalPresenter menuPrincipalPresenter = MenuPrincipalPresenter.getInstancia();
                menuPrincipalPresenter.abrirView(true);
                menuPrincipalPresenter.adaptarMenuParaStatusUsuario();
                view.dispose();
                JOptionPane.showMessageDialog(
                        view,
                        "Bem vindo",
                        "Sucesso",
                        JOptionPane.INFORMATION_MESSAGE
                );
            } else {
                mostrarMensagemErroNoLogin(mensagem);
            }
        } else {
            mostrarMensagemErroNoLogin(mensagem);
        }
    }

    private void botaoCancelarPressionado() {
        view.dispose();
    }

}
