package com.mycompany.trabalho_final_pss.Presenter;

import com.mycompany.trabalho_final_pss.Presenter.ManterReclamacoesSugestoes.ListagemReclamacoesSugestoesPresenter;
import com.mycompany.trabalho_final_pss.Presenter.Tarefa.ListagemTarefasPresenter;
import com.mycompany.trabalho_final_pss.View.MenuPrincipal.MenuPrincipalView;
import javax.swing.JInternalFrame;

public class PresentersBridge {

    public static void adicionarViewAoMenuPrincipal(JInternalFrame frame) {
        MenuPrincipalView.getInstancia().addToDesktop(frame);
    }
    
    public static void preencherTabelasSemFiltroListagemReclamacaoSugestao() {
        ListagemReclamacoesSugestoesPresenter.getInstancia().preencherTabelaSemFiltro();
    }
    
    public static void preencherTabelasSemFiltroListagemTarefa() {
        ListagemTarefasPresenter.getInstancia().preencherTabelaSemFiltro();
    }

}
