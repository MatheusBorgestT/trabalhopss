package com.mycompany.trabalho_final_pss.Presenter.ManterRepublica;

import com.mycompany.trabalho_final_pss.Model.Republica;
import com.mycompany.trabalho_final_pss.Model.StatusMoradorEnum;
import com.mycompany.trabalho_final_pss.Presenter.MenuPrincipal.MenuPrincipalPresenterBridge;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import java.sql.SQLException;
import java.util.ArrayList;

public class UpdateRepublicaState extends ManterRepublicaState {

    private Republica republica;

    public UpdateRepublicaState(Republica republica) {
        super();
        this.republica = republica;
        ajustarView();
    }

    @Override
    public void ajustarView() {
        presenter.setBotaoCancelarVisible(true);
        presenter.setBotaoExcluirVisible(false);
        presenter.setBotaoConfirmarText("Salvar");
        presenter.setCamposEditableView(true);
    }

    @Override
    public void botaoConfirmarPressionado() {
        if (PopUpUtil.mostrarPopUpConfirmacao(null, "Realmente deseja realizar as alterações?")) {
            ArrayList<String> erros;
            try {
                erros = service.updateRepublica(republica, presenter.getValoresNaView());
            } catch (SQLException ex) {
                erros = new ArrayList<>();
                erros.add(ex.toString());
            }

            if (erros.isEmpty()) {
                try {
                    MenuPrincipalPresenterBridge.atualizarUsuarioLogadoEMenuPrincipal();
                    
                    var republicaAtt = UsuarioService.getRepublicaOndeResideUsuarioLogado();
                    presenter.setState(new ReadRepublicaState(republicaAtt, StatusMoradorEnum.REPRESENTANTE));
                    
                    PopUpUtil.mostrarPopUpSucesso(null, "República editada com sucesso!");
                } catch (SQLException ex) {
                    PopUpUtil.mostrarPopUpErro(null, "Erro ao recuperar a república atualizada:\n" + ex.toString());
                }
            } else {
                StringBuilder mensagem = new StringBuilder();
                for (String e : erros) {
                    mensagem.append("\n").append(e);
                }
                PopUpUtil.mostrarPopUpErro(null, mensagem.toString());
            }
        }
    }

    @Override
    public void botaoExcluirPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void botaoCancelarPressionado() {
        presenter.setState(new ReadRepublicaState(republica, StatusMoradorEnum.REPRESENTANTE));
    }

}
