package com.mycompany.trabalho_final_pss.Presenter.Tarefa;

import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.Republica;
import com.mycompany.trabalho_final_pss.Model.Tarefa;
import com.mycompany.trabalho_final_pss.Presenter.AbstractPresenter;
import com.mycompany.trabalho_final_pss.Presenter.PresentersBridge;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import com.mycompany.trabalho_final_pss.View.Tarefa.ManterTarefaView;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ManterTarefaPresenter extends AbstractPresenter {

    private static ManterTarefaPresenter instancia;

    private ManterTarefaView view;
    
    private ManterTarefaState state;
    
    private Republica republica;
    
    private List<Morador> moradoresNaTabela;

    private List<Morador> responsaveisNaTabela;

    public ManterTarefaPresenter() {
        initView();
    }

    public static ManterTarefaPresenter getInstancia() {
        if (instancia == null) {
            instancia = new ManterTarefaPresenter();
        }
        return instancia;
    }

    @Override
    protected void initView() {
        view = ManterTarefaView.getInstancia();

        view.adicionarActionListenerBotaoConfirmar((ActionEvent e) -> {
            botaoConfirmarPressionado();
        });

        view.adicionarActionListenerBotaoAdicionarResponsavel((ActionEvent e) -> {
            botaoAdicionarPressionado();
        });

        view.adicionarActionListenerBotaoRemoverResponsavel((ActionEvent e) -> {
            botaoRemoverPressionado();
        });
    }

    @Override
    public void abrirView(boolean b) {
        fecharView();

        setRepublica();

        PresentersBridge.adicionarViewAoMenuPrincipal(view);
        view.setVisible(b);
    }
    
    public void fecharView() {
        view.setVisible(false);
    }

    public void setState(ManterTarefaState state) {
        this.state = state;
    }
    
    public void setRepublica() {
        try {
            republica = UsuarioService.getRepublicaOndeResideUsuarioLogado();
            moradoresNaTabela = republica.getMoradores();
            responsaveisNaTabela = new ArrayList<>();
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro ao recuperar dados da república");
            Logger.getLogger(ManterTarefaPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void preencherViewComTarefa(Tarefa tarefa) {
        try {
            HashMap<String, Object> valores = new HashMap<>();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

            valores.put("descricao", tarefa.getDescricao());
            valores.put("dataAgendamento", dtf.format(tarefa.getDataAgendamento()));
            valores.put("dataTermino", dtf.format(tarefa.getDataLimiteTermino()));

            view.setValoresDosCampos(valores);

            moradoresNaTabela = republica.getMoradores();
            responsaveisNaTabela = new ArrayList<>();
            for (Morador responsavel : tarefa.getMoradoresResponsaveis()) {
                for (Morador morador : moradoresNaTabela) {
                    if (responsavel.getIdPessoa() == morador.getIdPessoa()) {
                        moradoresNaTabela.remove(morador);
                        break;
                    }
                }
                responsaveisNaTabela.add(responsavel);
            }

            preencherTabelas();
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro no banco de dados");
            Logger.getLogger(ManterTarefaPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void preencherTabelas() {
        try {
            List<String> nomes = new ArrayList<>();
            for (Morador m : moradoresNaTabela) {
                nomes.add(m.getPessoa().getNome());
            }
            view.preencherTabelaMoradores(nomes.toArray());

            nomes = new ArrayList<>();
            for (Morador m : responsaveisNaTabela) {
                nomes.add(m.getPessoa().getNome());
            }
            view.preencherTabelaResponsaveis(nomes.toArray());
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro ao preencher as tabelas");
            Logger.getLogger(ManterTarefaPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Map<String, Object> getValorDosCampos() throws SQLException {
        var valores = view.getValoresDosCampos();
        valores.put("responsaveis", responsaveisNaTabela);
        valores.put("idAutor", UsuarioService.getMoradorLogado().getIdPessoa());
        valores.put("idRepublica", UsuarioService.getRepublicaOndeResideUsuarioLogado().getIdRepublica());
        valores.put("dataAgendamento", LocalDate.now());
        return valores;
    }

    public void setCampoDataDiaDeHoje() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        view.setTextCampoData(dtf.format(LocalDate.now()));
    }
    
    //--------------------------------------------------------------------------
    // Ponte com a View
    //--------------------------------------------------------------------------
    public void setBotaoConfirmarVisible(boolean b) {
        view.setBotaoConfirmarVisible(b);
    }

    // -------------------------------------------------------------------------
    // Botões
    // -------------------------------------------------------------------------
    private void botaoConfirmarPressionado() {
        state.botaoConfirmarPressionado();
    }

    private void botaoAdicionarPressionado() {
        int row = view.getSelectedRowTabelaMoradores();
        if (row >= 0) {
            var moradorSelecionado = moradoresNaTabela.get(row);
            moradoresNaTabela.remove(row);
            responsaveisNaTabela.add(moradorSelecionado);
            preencherTabelas();
        } else {
            PopUpUtil.mostrarPopUpErro(view, "Favor selecionar uma linha na tabela de moradores");
        }

    }

    private void botaoRemoverPressionado() {
        int row = view.getSelectedRowTabelaResponsaveis();
        if (row >= 0) {
            var moradorSelecionado = responsaveisNaTabela.get(row);
            responsaveisNaTabela.remove(row);
            moradoresNaTabela.add(moradorSelecionado);
            preencherTabelas();
        } else {
            PopUpUtil.mostrarPopUpErro(view, "Favor selecionar uma linha na tabela de moradores");
        }
        
    }

}
