/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.trabalho_final_pss.Presenter.ManterMorador;

import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Model.Republica;
import com.mycompany.trabalho_final_pss.Presenter.AbstractPresenter;
import com.mycompany.trabalho_final_pss.Repository.UsuarioRepository;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.View.ManterMorador.ManterMoradorView;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Julia Dalcamini
 */
public class ManterMoradorPresenter extends AbstractPresenter{
    
    private static ManterMoradorView view;
    
    private static EditarMoradorPresenter editarMorador;
    
    private static ManterMoradorPresenter instancia;
    
    public static ManterMoradorPresenter getInstancia() {
        if (instancia == null) {
            instancia = new ManterMoradorPresenter();
        }
        return instancia;
    }

    @Override
    protected void initView() {
        view = ManterMoradorView.getInstancia();
        
        view.adicionarActionListenerBotaoConvidar((ActionEvent e) -> {
            botaoConvidarPressionado();
        });
        
        view.adicionarActionListenerBotaoEditar((ActionEvent e) -> {
            botaoEditarPressionado();
        });
        
        view.adicionarActionListenerBotaoRemover((ActionEvent e) -> {
            botaoRemoverPressionado();
        });
        
        view.adicionarActionListenerBotaoHistorico((ActionEvent e) -> {
            botaoHistoricoPressionado();
        });
    }
    
    public void preencherTabelaMoradores() throws SQLException {
        Republica republica = UsuarioService.getRepublicaOndeResideUsuarioLogado();
        var tabela = republica.getInfoTabelaMoradores();
        view.preencheTabelaMoradores(tabela);
    }

    public void setViewVisible(boolean b) {
        view.setVisible(b);
    }

    //-------------------------------------------------------------------------------------------
    // BOTÕES
    //-------------------------------------------------------------------------------------------
    private void botaoConvidarPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void botaoEditarPressionado() {
        editarMorador.abrirView(true);
    }

    private void botaoRemoverPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void botaoHistoricoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abrirView(boolean b) {
        try {
            this.preencherTabelaMoradores();
        } catch (SQLException ex) {
            Logger.getLogger(ManterMoradorPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.setViewVisible(true);
    }
}
