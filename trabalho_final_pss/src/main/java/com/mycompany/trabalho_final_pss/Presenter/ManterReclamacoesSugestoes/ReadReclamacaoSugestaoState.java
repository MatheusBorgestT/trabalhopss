package com.mycompany.trabalho_final_pss.Presenter.ManterReclamacoesSugestoes;

import com.mycompany.trabalho_final_pss.Model.ReclamacaoSugestao;

public class ReadReclamacaoSugestaoState extends ManterReclamacaoSugestaoState {

    private ReclamacaoSugestao reclamacaoSugestao;

    public ReadReclamacaoSugestaoState(ManterReclamacaoSugestaoPresenter presenter, ReclamacaoSugestao reclamacaoSugestao) {
        super(presenter);
        this.reclamacaoSugestao = reclamacaoSugestao;
        ajustarView();
    }

    @Override
    public void ajustarView() {
        presenter.setAbaInformacoesEnabled(false);
        presenter.setAbaMoradoresEnabled(false);
        presenter.setBotaoConfirmarVisible(false);
        presenter.preencherViewComReclamacaoSugestao(reclamacaoSugestao);
    }

    @Override
    public void botaoConfirmarPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
