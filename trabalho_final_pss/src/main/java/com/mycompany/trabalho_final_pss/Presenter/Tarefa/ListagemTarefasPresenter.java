package com.mycompany.trabalho_final_pss.Presenter.Tarefa;

import com.mycompany.trabalho_final_pss.Model.Tarefa;
import com.mycompany.trabalho_final_pss.Presenter.AbstractPresenter;
import com.mycompany.trabalho_final_pss.Presenter.PresentersBridge;
import com.mycompany.trabalho_final_pss.Service.TarefaService;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import com.mycompany.trabalho_final_pss.View.Tarefa.ListagemTarefasView;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListagemTarefasPresenter extends AbstractPresenter{

    private static ListagemTarefasPresenter instancia;
    
    private static ListagemTarefasView view;
    
    private List<Tarefa> tarefasNaTabela;

    public ListagemTarefasPresenter() {
        initView();
    }
    
    public static ListagemTarefasPresenter getInstancia() {
        if (instancia == null) {
            instancia = new ListagemTarefasPresenter();
        }
        return instancia;
    }
    
    @Override
    protected void initView() {
        view = ListagemTarefasView.getInstancia();
        
        view.adicionarActionListenerBotaoBuscar((ActionEvent e) -> {
            botaoBuscarPressionado();
        });
        
        view.adicionarActionListenerBotaoCadastrarTarefa((ActionEvent e) -> {
            botaoCadastrarPressionado();
        });
        
        view.adicionarActionListenerBotaoEditarTarefa((ActionEvent e) -> {
            botaoEditarPressionado();
        });
        
        view.adicionarActionListenerBotaoExcluirTarefa((ActionEvent e) -> {
            botaoExcluirPressionado();
        });
    }

    @Override
    public void abrirView(boolean b) {
        adaptarViewAoUsuario();
        preencherTabelaSemFiltro();
        PresentersBridge.adicionarViewAoMenuPrincipal(view);
        view.setVisible(b);
    }
    
    public void preencherTabelaSemFiltro() {
        try {
            int idRepublica = UsuarioService.getRepublicaOndeResideUsuarioLogado().getIdRepublica();
            tarefasNaTabela = TarefaService.getTarefasByIdRepublica(idRepublica);
            
            preencherTabela();
            view.setSelectedIndexFiltro(-1);
            view.setTextCampoBusca("");
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro ao conseguir informações da república\n" + ex.toString());
            Logger.getLogger(ListagemTarefasPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void preencherTabela() throws SQLException {
        ArrayList<Object[]> listaValores = new ArrayList<>();
        for (Tarefa t: tarefasNaTabela) {
            listaValores.add(t.toObjectArray());
        }
        view.preencherTabela(listaValores);
    }
    
    private void adaptarViewAoUsuario() {
        try {
            var statusMoradorLogado = UsuarioService.getMoradorLogado().getStatusMorador();
            switch (statusMoradorLogado) {
                case REPRESENTANTE:
                    view.setBotaoExcluirVisible(true);
                    break;
                case MORADOR:
                    view.setBotaoExcluirVisible(false);
                    break;
                default:
                    PopUpUtil.mostrarPopUpErro(view, "Status de morador inválido");
                    abrirView(false);
                    break;
            }
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro ao buscar informções de usuário logado");
            Logger.getLogger(ListagemTarefasPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Tarefa getTarefaSelecionada() {
        int selectedRow = view.getSelectedRowTabela();
        if (selectedRow >= 0) {
            return tarefasNaTabela.get(selectedRow);
        } else {
            PopUpUtil.mostrarPopUpErro(view, "Favor selecionar uma linha da tabela");
            return null;
        }
    }
    
    // -------------------------------------------------------------------------
    // Botões
    // -------------------------------------------------------------------------
    private void botaoBuscarPressionado() {
        int index = view.getSelectedIndexFiltro();
        String busca = view.getTextCampoBusca();
        if (busca.isBlank()) {
            preencherTabelaSemFiltro();
        } else {
            try {
                switch (index) {
                    case -1:
                        PopUpUtil.mostrarPopUpErro(view, "Favor escolher um filtro");
                        break;
                    case 0:
                        tarefasNaTabela = TarefaService.getTarefasByPessoa(
                                UsuarioService.getRepublicaOndeResideUsuarioLogado().getIdRepublica(), 
                                view.getTextCampoBusca()
                        );
                        preencherTabela();
                        break;
                    case 1:
                        tarefasNaTabela = TarefaService.getTarefasByDescricao(
                                UsuarioService.getRepublicaOndeResideUsuarioLogado().getIdRepublica(), 
                                view.getTextCampoBusca()
                        );
                        preencherTabela();
                        break;
                    default:
                        PopUpUtil.mostrarPopUpErro(view, "Indicie de filtro inválido");
                        break;
                }
            } catch (SQLException ex) {
                PopUpUtil.mostrarPopUpErro(view, "Erro no banco");
                Logger.getLogger(ListagemTarefasPresenter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void botaoCadastrarPressionado() {
        var manterTarefaPresenter = ManterTarefaPresenter.getInstancia();
        manterTarefaPresenter.abrirView(true);
        manterTarefaPresenter.setState(new CreateTarefaState(manterTarefaPresenter));
    }
    
    private void botaoEditarPressionado() {
        var t = getTarefaSelecionada();
        if (t != null) {
            var manterTarefaPresenter = ManterTarefaPresenter.getInstancia();
            manterTarefaPresenter.abrirView(true);
            manterTarefaPresenter.setState(new UpdateTarefaState(manterTarefaPresenter, t));
        }
    }
    
    private void botaoExcluirPressionado() {
        var t = getTarefaSelecionada();
        if (t != null) {
            if (PopUpUtil.mostrarPopUpConfirmacao(view, "Deseja excluir a tarefa?")) {
                try {
                    TarefaService.excluirTarefa(t);
                    tarefasNaTabela.remove(t);
                    preencherTabela();
                } catch (SQLException ex) {
                    PopUpUtil.mostrarPopUpErro(view, "Erro no banco ao excluir a tarefa.");
                    Logger.getLogger(ListagemTarefasPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}