
package com.mycompany.trabalho_final_pss.Presenter.Login;

import com.mycompany.trabalho_final_pss.Presenter.AbstractPresenter;
import com.mycompany.trabalho_final_pss.Service.LoginService;
import com.mycompany.trabalho_final_pss.View.Login.NovoUsuarioView;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class NovoUsuarioPresenter extends AbstractPresenter {
    
    private static NovoUsuarioPresenter instancia;
    
    private static NovoUsuarioView view;
    
    private static LoginService service;
    
    private NovoUsuarioPresenter() {
        super();
        initView();
        service = new LoginService();
    }

    public static NovoUsuarioPresenter getInstancia() {
        if (instancia == null) {
            instancia = new NovoUsuarioPresenter();
        }
        return instancia;
    }

    @Override
    protected void initView() {
        view = NovoUsuarioView.getInstancia();
        
        view.adicionarActionListenerBotaoCadastrar((ActionEvent e) -> {
            botaoCadastrarPressionado();
        });
        
        view.adicionarActionListenerBotaoCancelar((ActionEvent e) -> {
            botaoCancelarPressionado();
        });
    }

    @Override
    public void abrirView(boolean b) {
        view.setVisible(b);
    }
    
    // -------------------------------------------------------------------------
    // Botões
    // -------------------------------------------------------------------------
    private void botaoCadastrarPressionado() {
        List<String> erros = new ArrayList<>();
        try {
            erros = service.cadastrarUsuario(view.getValoresDosCampos());
        } catch (SQLException ex) {
            erros.add(ex.toString());
        }
        
        if (erros.isEmpty()) {
            JOptionPane.showMessageDialog(
                    view,
                    "Cadastro realizado com sucesso",
                    "Sucesso",
                    JOptionPane.INFORMATION_MESSAGE
            );
            view.dispose();
        } else {
            StringBuilder mensagem = new StringBuilder();
            for (String s: erros) {
                mensagem.append("\n").append(s);
            }
            JOptionPane.showMessageDialog(
                    view,
                    "Erro(s) ao realizar o cadastro:" + mensagem.toString(),
                    "Erro",
                    JOptionPane.WARNING_MESSAGE
            );
        }
    }
    
    private void botaoCancelarPressionado() {
        view.dispose();
    }
    
}
