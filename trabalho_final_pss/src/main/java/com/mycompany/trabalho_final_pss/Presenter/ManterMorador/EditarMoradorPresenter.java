/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.trabalho_final_pss.Presenter.ManterMorador;

import com.mycompany.trabalho_final_pss.Model.InstanciaHistoricoRepublicas;
import com.mycompany.trabalho_final_pss.Model.Morador;
import com.mycompany.trabalho_final_pss.Presenter.AbstractPresenter;
import com.mycompany.trabalho_final_pss.Presenter.PresentersBridge;
import com.mycompany.trabalho_final_pss.Service.ManterMoradorService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import com.mycompany.trabalho_final_pss.View.ManterMorador.IFrmEditarMorador;
import com.mycompany.trabalho_final_pss.View.ManterMorador.ManterMoradorView;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Julia Dalcamini
 */
public class EditarMoradorPresenter extends AbstractPresenter{
    
    protected static IFrmEditarMorador view;
    
    protected static ManterMoradorService service;
    
    protected static EditarMoradorPresenter instancia;
    
    private Morador morador;
    
    private InstanciaHistoricoRepublicas historico;
    
    public static EditarMoradorPresenter getInstancia() {
        if (instancia == null) {
            instancia = new EditarMoradorPresenter();
        }
        return instancia;
    }
    
    private void setValoresCampos() {
        Map<String, String> valores = new HashMap<>();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        valores.put("dataEntrada", String.valueOf(historico.getDataEntrada()));
        valores.put("valorAluguel", String.valueOf(historico.getValorAluguel()));
        
        this.setValoresDosCampos(valores);
    }

    @Override
    protected void initView() {
        view = IFrmEditarMorador.getInstancia();
        
        view.adicionarActionListenerBotaoSalvar((ActionEvent e) -> {
            botaoSalvarPressionado();
        });
    }

    @Override
    public void abrirView(boolean b) {
        PresentersBridge.adicionarViewAoMenuPrincipal(view);
    }
    
    public void setViewVisible(boolean b) {
        view.setVisible(b);
        this.setValoresCampos();
    }
    
    public Map<String, String> getValoresView() {
        return view.getValoresDosCampos();
    }
    
    public void setValoresDosCampos(Map<String, String> valores) {
        view.setValoresDosCampos(valores);
    }
    
    // BOTOES
    
    public void botaoSalvarPressionado() {
        if (PopUpUtil.mostrarPopUpConfirmacao(null, "Realmente deseja realizar as alterações?")) {
            ArrayList<String> erros;
            try {
                erros = service.updateMorador(morador, this.getValoresView());
            } catch (SQLException ex) {
                erros = new ArrayList<>();
                erros.add(ex.toString());
            }

            if (erros.isEmpty()) {
                PopUpUtil.mostrarPopUpSucesso(null, "Morador editado com sucesso!");
                this.setViewVisible(false);
            } else {
                StringBuilder mensagem = new StringBuilder();
                for (String e : erros) {
                    mensagem.append("\n").append(e);
                }
                PopUpUtil.mostrarPopUpErro(null, mensagem.toString());
            }
        }
    }
}
