package com.mycompany.trabalho_final_pss.Presenter.Tarefa;

public abstract class ManterTarefaState {
    
    protected ManterTarefaPresenter presenter;
    
    protected ManterTarefaState(ManterTarefaPresenter presenter) {
        this.presenter = presenter;
    }
    
    public abstract void ajustarView();
    
    public abstract void botaoConfirmarPressionado();
}
