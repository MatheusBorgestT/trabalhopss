package com.mycompany.trabalho_final_pss.Presenter.ManterReclamacoesSugestoes;

import com.mycompany.trabalho_final_pss.Model.TipoEntradaEnum;
import com.mycompany.trabalho_final_pss.Model.TipoReclamacaoSugestaoEnum;
import com.mycompany.trabalho_final_pss.Presenter.PresentersBridge;
import com.mycompany.trabalho_final_pss.Service.ReclamacaoSugestaoService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CreateReclamacaoSugestaoState extends ManterReclamacaoSugestaoState {

    public CreateReclamacaoSugestaoState(ManterReclamacaoSugestaoPresenter presenter) {
        super(presenter);
        ajustarView();
    }

    @Override
    public void ajustarView() {
        presenter.setAbaInformacoesEnabled(true);
        presenter.setAbaMoradoresEnabled(true);
        presenter.setBotaoConfirmarVisible(true);
        presenter.setCampoDataDiaDeHoje();
        presenter.preencherTabelas();
    }

    @Override
    public void botaoConfirmarPressionado() {
        try {
            var valores = presenter.getValorDosCampos();
            var erros = ReclamacaoSugestaoService.cadastrarReclamacaoSugestao(presenter.getValorDosCampos());

            if (erros.isEmpty()) {
                PresentersBridge.preencherTabelasSemFiltroListagemReclamacaoSugestao();
                        
                int intTipo = Integer.parseInt((String) valores.get("tipo"));
                String strTipo = TipoReclamacaoSugestaoEnum.getTipo(intTipo).getValorString();
                
                presenter.fecharView();
                PopUpUtil.mostrarPopUpSucesso(null, strTipo + " cadastrada com sucesso!");
            } else {
                StringBuilder sb = new StringBuilder();
                for (String e : erros) {
                    sb.append("\n").append(e);
                }
                PopUpUtil.mostrarPopUpErro(null, sb.toString());
            }
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(null, "Erro relacionado ao banco de dados");
            Logger.getLogger(CreateReclamacaoSugestaoState.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
