package com.mycompany.trabalho_final_pss.Presenter.MenuPrincipal;

import com.mycompany.trabalho_final_pss.Model.StatusMoradorEnum;
import com.mycompany.trabalho_final_pss.Presenter.AbstractPresenter;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import com.mycompany.trabalho_final_pss.View.MenuPrincipal.MenuPrincipalView;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import javax.swing.JInternalFrame;

public class MenuPrincipalPresenter extends AbstractPresenter {
    
    private static MenuPrincipalState state;
    
    private static MenuPrincipalPresenter instancia;
    
    private static MenuPrincipalView view;

    private MenuPrincipalPresenter() {
        initView();
    }

    public static MenuPrincipalPresenter getInstancia() {
        if (instancia == null) {
            instancia = new MenuPrincipalPresenter();
        }
        return instancia;
    }

    @Override
    protected void initView() {
        view = MenuPrincipalView.getInstancia();
        
        view.adicionarActionListenerOpcaoBuscarVagas((ActionEvent e) -> {
            opcaoBuscarVagasPressionado();
        });
        
        view.adicionarActionListenerOpcaoConsultarLancamento((ActionEvent e) -> {
            opcaoConsultarLancamentoPressionado();
        });
        
        view.adicionarActionListenerOpcaoConsultarResultado((ActionEvent e) -> {
            opcaoConsultarResultado();
        });
        
        view.adicionarActionListenerOpcaoConvidar((ActionEvent e) -> {
            opcaoConvidarPressionado();
        });
        
        view.adicionarActionListenerOpcaoCriarRepublica((ActionEvent e) -> {
            opcaoCriarRepublicaPressionado();
        });
        
        view.adicionarActionListenerOpcaoEstornos((ActionEvent e) -> {
            opcaoEstornosPressionado();
        });
        
        view.adicionarActionListenerOpcaoMeuPerfil((ActionEvent e) -> {
            opcaoMeuPerfilPressionado();
        });
        
        view.adicionarActionListenerOpcaoMinhaRepublica((ActionEvent e) -> {
            opcaoMinhaRepublicaPressionado();
        });
        
        view.adicionarActionListenerOpcaoMoradoresRepublica((ActionEvent e) -> {
            opcaoMoradorRepublicaPressionado();
        });
        
        view.adicionarActionListenerOpcaoNotificacoes((ActionEvent e) -> {
            opcaoNotificacoesPressionado();
        });
        
        view.adicionarActionListenerOpcaoReceitasDespesas((ActionEvent e) -> {
            opcaoReceitasDespesasPressionado();
        });
        
        view.adicionarActionListenerOpcaoReclamacoes((ActionEvent e) -> {
            opcaoReclamacoesPressionado();
        });
        
        view.adicionarActionListenerOpcaoRegistrarConclusao((ActionEvent e) -> {
            opcaoConclusaoPressionado();
        });
        
        view.adicionarActionListenerOpcaoRegistrarPagamento((ActionEvent e) -> {
            opcaoPagamentoPressionado();
        });
        
        view.adicionarActionListenerOpcaoSolucao((ActionEvent e) -> {
            opcaoSolucaoPressionado();
        });
        
        view.adicionarActionListenerOpcaoTarefasGerais((ActionEvent e) -> {
            opcaoTarefasGeraisPressionado();
        });
        
        view.adicionarActionListenerOpcaoEncerrarSessao((ActionEvent e) -> {
            opcaoEncerrarSessaoPressionado();
        });
    }    

    @Override
    public void abrirView(boolean b) {
        view.setVisible(b);
    }
    
    public void setTodosMenusItensVisiveis(boolean b) {
        view.setTodosMenusItensVisiveis(b);
    }
    
    public void adaptarMenuParaStatusUsuario() {
        try {
            var moradorLogado = UsuarioService.getMoradorLogado();
            
            switch (moradorLogado.getStatusMorador()) {
                case SEM_TETO:
                    state = new SemTetoState(this);
                    break;
                case MORADOR:
                    state = new MoradorState(this);
                    break;
                case REPRESENTANTE:
                    state = new RepresentanteState(this);
                    break;
                default:
                    PopUpUtil.mostrarPopUpErro(view, "Status do morador indefinido");
                    view.dispose();
                    break;
            }
            
            preencherRodape();
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, ex.toString());
            view.dispose();
        }
    }
    
    public void preencherRodape() {
        var usuario = UsuarioService.getUsuarioLogado();
        view.setUsuarioRodape(usuario.getNomeUsuario());
        
        try {
            var morador = usuario.getPessoa().getMorador();
            var status = morador.getStatusMorador();
            
            view.setTipoUsuarioRodape(status.getValorString());
            if (status != StatusMoradorEnum.SEM_TETO) {
                var republica = morador.getRepublicaOndeReside();
                view.setRepublicaRodape(republica.getNome());
                view.setQuantidadeMoradoresRepublicaRodape(republica.getNumeroVagasOcupadas());
            } else {
                view.setRepublicaRodape("");
                view.setQuantidadeMoradoresRepublicaRodape(-1);
            }
            
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(view, "Erro ao carregar informações do morador\n" + ex.toString());
        }
        
        // notificações
    }
    
    // -------------------------------------------------------------------------
    // Ponte com a View
    // -------------------------------------------------------------------------
    public void addToDesktop(JInternalFrame frame) {
        view.addToDesktop(frame);
    }
    
    // -------------------------------------------------------------------------
    // Tornando Botões Visíveis
    // -------------------------------------------------------------------------
    // UC1 - Manter República
    public void tornarMinhaRepublicaVisivel(boolean b) {
        view.tornarMinhaRepublicaVisivel(b);
    }
    
    // UC13 - Buscar Vagas
    public void tornarBuscarVagasVisivel(boolean b) {
        view.tornarBuscarVagasVisivel(b);
    }
    
    // UC10 - Consultar minhas receitas e despesas
    public void tornarConsultarLancamentoVisivel(boolean b) {
        view.tornarConsultarLancamentoVisivel(b);
    }
    
    // UC11 - Consultar resultado mensal
    public void tornarConsultarResultadoVisivel(boolean b) {
        view.tornarConsultarResultadoVisivel(b);
    }
    
    // UC5 - Convidar moradores
    public void tornarConvidarVisivel(boolean b) {
        view.tornarConvidarVisivel(b);
    }
    
    // UC1 - Criar republica
    public void tornarCriarRepublicaVisivel(boolean b) {
        view.tornarCriarRepublicaVisivel(b);
    }
    
    // UC15 - Fazer estornos de lancamento
    public void tornarEstornosVisivel(boolean b) {
        view.tornarEstornosVisivel(b);
    }
    
    // UC7 - Manter perfil
    public void tornarMeuPerfilVisivel(boolean b) {
        view.tornarMeuPerfilVisivel(b);
    }
    
    // UC2 - Manter moradores
    public void tornarMoradoresRepublicaVisivel(boolean b) {
        view.tornarMoradoresRepublicaVisivel(b);
    }
    
    // UC17 - Aceitar convite
    public void tornarNotificacoesVisivel(boolean b) {
        view.tornarNotificacoesVisivel(b);
    }
    
    // UC4 - Manter receitas e despesas
    public void tornarReceitasDespesasVisivel(boolean b) {
        view.tornarReceitasDespesasVisivel(b);
    }
    
    // UC8 - Manter reclamacoes/sugestoes
    public void tornarReclamacoesVisivel(boolean b) {
        view.tornarReclamacoesVisivel(b);
    }
    
    // UC12 - Registrar conclusao da tarefa
    public void tornarRegistrarConclusaoVisivel(boolean b) {
        view.tornarRegistrarConclusaoVisivel(b);
    }
    
    // UC9 - Registrar pagamento de receitas ou despesas
    public void tornarRegistrarPagamentoVisivel(boolean b) {
        view.tornarRegistrarPagamentoVisivel(b);
    }
    
    // UC6 - Confirmar a solucao de reclamacoes/sugestoes
    public void tornarSolucaoVisivel(boolean b) {
        view.tornarSolucaoVisivel(b);
    }
    
    // UC3 - Manter tarefas
    public void tornarTarefasGeraisVisivel(boolean b) {
        view.tornarTarefasGeraisVisivel(b);
    }
    
    public void tornarEncerrarSessaoVisivel(boolean b) {
        view.tornarEncerrarSessaoVisivel(b);
    }
        
    // -------------------------------------------------------------------------
    // Botões
    // -------------------------------------------------------------------------
    private void opcaoBuscarVagasPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void opcaoConsultarLancamentoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void opcaoConsultarResultado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void opcaoConvidarPressionado() {
        state.opcaoConvidarPressionado();
    }

    private void opcaoEstornosPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void opcaoMeuPerfilPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void opcaoMinhaRepublicaPressionado() {
        state.opcaoMinhaRepublicaPressionado();
    }

    private void opcaoMoradorRepublicaPressionado() {
        state.opcaoMoradorRepublicaPressionado();
    }

    private void opcaoNotificacoesPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void opcaoReceitasDespesasPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void opcaoReclamacoesPressionado() {
        state.opcaoReclamacoesPressionado();
    }

    private void opcaoConclusaoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void opcaoPagamentoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void opcaoSolucaoPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void opcaoTarefasGeraisPressionado() {
        state.opcaoTarefasGeraisPressionado();
    }

    private void opcaoCriarRepublicaPressionado() {
        state.opcaoCriarRepublicaPressionado();
    }
    
    private void opcaoEncerrarSessaoPressionado() {
        state.opcaoEncerrarSessaoPressionado();
    }
}
