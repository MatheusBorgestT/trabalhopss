package com.mycompany.trabalho_final_pss.Presenter.Tarefa;

import com.mycompany.trabalho_final_pss.Model.Tarefa;
import com.mycompany.trabalho_final_pss.Presenter.PresentersBridge;
import com.mycompany.trabalho_final_pss.Service.TarefaService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UpdateTarefaState extends ManterTarefaState {

    private Tarefa tarefa;
    
    public UpdateTarefaState(ManterTarefaPresenter presenter, Tarefa tarefa) {
        super(presenter);
        this.tarefa = tarefa;
        ajustarView();
    }

    @Override
    public void ajustarView() {
        presenter.setBotaoConfirmarVisible(true);
        presenter.preencherViewComTarefa(tarefa);
    }

    @Override
    public void botaoConfirmarPressionado() {
        try {
            var valores = presenter.getValorDosCampos();
            var erros = TarefaService.atualizarTarefa(tarefa, presenter.getValorDosCampos());

            if (erros.isEmpty()) {
                PresentersBridge.preencherTabelasSemFiltroListagemTarefa();

                presenter.fecharView();
                PopUpUtil.mostrarPopUpSucesso(null, "Tarefa atualizada com sucesso!");
            } else {
                StringBuilder sb = new StringBuilder();
                for (String e : erros) {
                    sb.append("\n").append(e);
                }
                PopUpUtil.mostrarPopUpErro(null, sb.toString());
            }
        } catch (SQLException ex) {
            PopUpUtil.mostrarPopUpErro(null, "Erro relacionado ao banco de dados");
            Logger.getLogger(CreateTarefaState.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
