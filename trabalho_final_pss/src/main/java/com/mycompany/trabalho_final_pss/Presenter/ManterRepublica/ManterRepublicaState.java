
package com.mycompany.trabalho_final_pss.Presenter.ManterRepublica;

import com.mycompany.trabalho_final_pss.Service.RepublicaService;

public abstract class ManterRepublicaState {
    
    protected ManterRepublicaPresenter presenter;
    
    protected RepublicaService service;
    
    protected ManterRepublicaState() {
        this.presenter = ManterRepublicaPresenter.getInstancia();
        this.service = new RepublicaService();
    }
    
    public abstract void ajustarView();
    
    public abstract void botaoConfirmarPressionado();
    
    public abstract void botaoExcluirPressionado();
    
    public abstract void botaoCancelarPressionado();
    
}
