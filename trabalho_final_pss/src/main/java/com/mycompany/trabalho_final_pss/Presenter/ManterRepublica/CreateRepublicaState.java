package com.mycompany.trabalho_final_pss.Presenter.ManterRepublica;

import com.mycompany.trabalho_final_pss.Model.StatusMoradorEnum;
import com.mycompany.trabalho_final_pss.Presenter.MenuPrincipal.MenuPrincipalPresenterBridge;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import java.sql.SQLException;
import java.util.ArrayList;

public class CreateRepublicaState extends ManterRepublicaState {

    public CreateRepublicaState() {
        super();
        ajustarView();
    }

    @Override
    public void ajustarView() {
        presenter.setBotaoCancelarVisible(false);
        presenter.setBotaoExcluirVisible(false);
        presenter.setBotaoConfirmarText("Confirmar");
        presenter.setCamposEditableView(true);
    }

    @Override
    public void botaoConfirmarPressionado() {
        ArrayList<String> erros;
        try {
            erros = service.createRepublica(presenter.getValoresNaView());
        } catch (SQLException ex) {
            erros = new ArrayList<>();
            erros.add(ex.toString());
        }
        
        if (erros.isEmpty()) {
            try {
                MenuPrincipalPresenterBridge.atualizarUsuarioLogadoEMenuPrincipal();
                presenter.setState(new ReadRepublicaState(UsuarioService.getRepublicaOndeResideUsuarioLogado(), StatusMoradorEnum.REPRESENTANTE));
                
                PopUpUtil.mostrarPopUpSucesso(null, "República cadastrada com sucesso!");
            } catch (SQLException ex) {
                PopUpUtil.mostrarPopUpErro(null, ex.toString());
            }
        } else {
            StringBuilder mensagem = new StringBuilder();
            for (String e: erros) {
                mensagem.append("\n").append(e);
            }
            PopUpUtil.mostrarPopUpErro(null, mensagem.toString());
        }
    }

    @Override
    public void botaoExcluirPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void botaoCancelarPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated metreason: actual and formal argument lists differ in lengthhods, choose Tools | Templates.
    }

}
