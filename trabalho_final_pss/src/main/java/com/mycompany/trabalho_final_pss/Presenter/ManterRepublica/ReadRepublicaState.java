package com.mycompany.trabalho_final_pss.Presenter.ManterRepublica;

import com.mycompany.trabalho_final_pss.Model.Republica;
import com.mycompany.trabalho_final_pss.Model.StatusMoradorEnum;
import com.mycompany.trabalho_final_pss.Model.Usuario;
import com.mycompany.trabalho_final_pss.Presenter.MenuPrincipal.MenuPrincipalPresenterBridge;
import com.mycompany.trabalho_final_pss.Service.UsuarioService;
import com.mycompany.trabalho_final_pss.Utils.PopUpUtil;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ReadRepublicaState extends ManterRepublicaState {

    private Republica republica;
    
    private StatusMoradorEnum statusMorador;

    public ReadRepublicaState(Republica republica, StatusMoradorEnum statusMorador) {
        super();
        this.republica = republica;
        this.statusMorador = statusMorador;
        setValoresCampos();
        ajustarView();
    }

    @Override
    public void ajustarView() {
        boolean b = statusMorador == StatusMoradorEnum.REPRESENTANTE;
        
        presenter.setBotaoCancelarVisible(false);
        presenter.setBotaoExcluirVisible(b);
        presenter.setBotaoConfirmarVisible(b);
        presenter.setBotaoConfirmarText("Editar");
        presenter.setCamposEditableView(false);
    }

    @Override
    public void botaoConfirmarPressionado() {
        presenter.setState(new UpdateRepublicaState(republica));
    }

    @Override
    public void botaoExcluirPressionado() {
        boolean confirmacao = PopUpUtil.mostrarPopUpConfirmacao(null, "Deseja excluir a república?");
        if (confirmacao) {
            try {
                service.deleteRepublica(republica);
                presenter.abrirView(false);
                
                MenuPrincipalPresenterBridge.atualizarUsuarioLogadoEMenuPrincipal();
                UsuarioService.atualizarUsuarioInfo();
                
                PopUpUtil.mostrarPopUpSucesso(null, "República excluída com sucesso!");
            } catch (SQLException ex) {
                PopUpUtil.mostrarPopUpErro(null, ex.toString());
            }
        }
    }

    @Override
    public void botaoCancelarPressionado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setValoresCampos() {
        Map<String, String> valores = new HashMap<>();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        valores.put("nome", republica.getNome());
        valores.put("dataFundacao", republica.getDataFundacao().format(dtf));
        valores.put("despesasMediasMorador", String.valueOf(republica.getDespesaMedia()));
        valores.put("totalVagas", String.valueOf(republica.getNumeroTotalVagas()));
        valores.put("vagasOcupadas", String.valueOf(republica.getNumeroVagasOcupadas()));
        valores.put("vantagens", republica.getVantagens());
        valores.put("codigoEtica", republica.getEstatuto());
        valores.put("logradouro", republica.getEndereco());
        valores.put("bairro", republica.getBairro());
        valores.put("pontoReferencia", republica.getPontoReferencia());
        valores.put("latitude", String.valueOf(republica.getLatitude()));
        valores.put("longitude", String.valueOf(republica.getLongitude()));
        valores.put("cep", republica.getCep());
        
        presenter.setValoresDosCampos(valores);
    }

}
