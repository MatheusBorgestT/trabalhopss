
package com.mycompany.trabalho_final_pss.Presenter.ManterReclamacoesSugestoes;

public abstract class ManterReclamacaoSugestaoState {
    
    protected ManterReclamacaoSugestaoPresenter presenter;
    
    protected ManterReclamacaoSugestaoState(ManterReclamacaoSugestaoPresenter presenter) {
        this.presenter = presenter;
    }
    
    public abstract void ajustarView();
    
    public abstract void botaoConfirmarPressionado();
    
}
