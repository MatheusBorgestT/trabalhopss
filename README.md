# Gerenciamento de Repúblicas

Trabalho final da disciplina de Projeto de Sistemas de Software, lecionada pelo professor Clayton Fraga.

[Relatório](https://docs.google.com/document/d/1BZv_KOnTqt8Q6hFiTNKwDqPk6xQOS0fMh1tKLn1YsBw/edit?usp=sharing)

# Grupo

* Ingrid de Souza Santos
* Julia Vicente Dalcamini
* Lucas Emílio Cateringer Mussi
* Matheus Borges Teixeira
* Thiago Gonçalves da Pascoa
